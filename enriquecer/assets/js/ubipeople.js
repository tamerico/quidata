/* nao sei se este arquivo é chamado*/

var apis = [];
var apisSelecionadas = [];
var ready = false;
var usuarioTwitterSelecionado;
var usuarioGoogleSelecionado;
var usuarioFacebookSelecionado;

// Variaveis usadas nas condições
var usuarioGoogleSelecionadoFoto;
var usuarioTwitterSelecionadoFoto;
var usuarioFacebookSelecionadoFoto;
var usuarioGoogleSelecionadoCidade;
var usuarioTwitterSelecionadoCidade;
var usuarioGoogleSelecionadoProfissao;
var usuarioTransparenciaSelecionadoProfissao;
var usuarioGoogleSelecionadoBio;
var usuarioTwitterSelecionadoBio;

// Variaveis para verificação do retorno da requisição

var APItransparencia;
var APIfacebook;
var APItwitter;
var APIgplus;
var APIescolaridade;

/* Variaveis para a tela de resultado*/

// informações pessoais

var foto;
var nome;
var cpf = null;
var email;
var telefone;
var cidade;
var profissao;
var biografia;
var biografiaSecundaria;
var idiomas = [];
var cidadesAnteriores = "";
var escolaridade = [];
var instituicao = [];
var academico = [];
var titulo = [];
var tituloSecundario = [];
var instituicaoSecundario = [];
var cidades = [];
var escolaridadeSecundaria;

// informações profissional

var nomeEmpresa;
var nomeFuncionario;
var cargo;
var salarioLiquido;
var tempoCargo;
var endereco;
var cpfTransparencia = null;
var descricaoCoorporativo = null;
var palavrasChaves = [];

//redes sociais

var urlSociais = [];
var urlEmail = [];
var facebook;
var twitter;
var googlePlus;
var linkedin;
var youtube;
var emailUrl;
//outras informacões

var estadoCivil;
var pensamento;

var escolaridadeIndex = 0;

//verifica se todas as APIS retornaram, com sucesso ou não

function APISelecionada (api) {

    apisSelecionadas.push(api);
    $('#loading-complete-' + api).css('display','block');
    //apisSelecionadas = Array.from(new Set(apisSelecionadas));

}

function maybeGerarRelatorio (tipo) {

    apis.push(tipo);

    console.log('apis = ' + apis + '; tipo = ' + tipo);

    $('#loading-' + tipo).css('display','none');
    $('#loading-maior-' + tipo).css('display','none');
    //$('#content-' + tipo).css('display','block');

    if (apis.length >= 6) {

	console.log('pronto');
	$('#loading-relatorio').css('display','none');
	$('#loading-complete-relatorio').css('display','block');
	$('.wizard > .steps .last a').css('opacity','1').css('background-color', 'green').css('cursor','pointer');
	ready = true;
	//gerar relatorio

    }

}

function inicio() {
    document.getElementById("seguimento-pesquisa").style.display = "none" //oculta;
    document.getElementById("redes-sociais").style.display = "none" //oculta;
    document.getElementById("filtros").style.display = "";

    $("#filtros").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#enriquecer").click();
        }
    });

}

// Pesquisa efetuada na primeira tela
function pesquisa(nomeUsuario, emailUsuario, telefoneUsuario, cpfUsuario) {
    console.log("pesquisa  nomeUsuario, emailUsuario, telefoneUsuario, cpfUsuario");

    $('#pesquisandopor').html('Pesquisando por: ' + nomeUsuario + (emailUsuario ? (' e ' + emailUsuario) : ''));

    limpezaDosCampos();
    limpezaDasDivs();
    limpezaDasVariaveis();
    document.getElementById("seguimento-pesquisa").style.display = 'none'; //oculta
    //document.getElementById("filtros").style.display = 'none'; //oculta
    document.getElementById("nao-encontrado-g").style.display = 'none'; //oculta
    //document.getElementById("nao-encontrado-f").style.display = 'none'; //oculta
    //document.getElementById("nao-encontrado-t").style.display = 'none'; //oculta
    document.getElementById("interface").style.marginTop = "-5%";

    //Pega os campos informados pelo usuário na tela inicial
    nome = nomeUsuario;
    email = emailUsuario;
    telefone = telefoneUsuario;
    cpf = cpfUsuario;

    //$('#example-basic-t-5').css('padding','0');
    //$('#example-basic-t-5').attr('href','');

    //Seta os valores pegados na primeira tela, para navbar contendo os filtros
    /*document.getElementById("nome2").value = nome;
    document.getElementById("email2").value = email;
    document.getElementById("telefone2").value = telefone;
    document.getElementById("cpf2").value = cpf;*/

    //Inicia o loading page
    //NProgress.start();

    //seta as mensagens de procura quando usuário inicia o enriquecimento
    $("#msg-status").html("Procurando redes sociais");
    document.getElementById("redes-sociais").style.display = 'block'; //mostra

    //document.getElementById("redes-sociais").style.display = 'none'; //oculta
    //document.getElementById("pesquisando").style.display = ''; //mostra

    $("#nomeParaPesquisar").html(nome);

    /*function salve() {
        setTimeout(function () {
            $("#msg-status").html("Procurando trabalho e rendimento");
        }, 800);

    }

    setTimeout(function () {
        NProgress.set(0.4);
        $("#msg-status").html("Procurando escolaridade");
        salve();
    }, 800);*/

    // enriquecimento inicia aqui
   // https://graph.facebook.com/search?q=a&type=user&fields=id,name,email,link,picture.width(200).height(200)&access_token=EAAJTQbPOvZAoBAHVBsVrpZCzZAWZBEHNnnzVmxUSdGZC1uhhVYFHVjt31HhNkYNxvlJOZBIXfRdrXyxclOmguEeCVESdUUP7k4wV33RhoLcJ7ZBEHtXoZAbafXCWqKepSzG4HsQrr93Ql7DxrBnLuaZC1z2cxu0cZAZB4lpyk5pgZAHRkwZDZD

    // API responsável pelas informações profissionais do potencial cliente
    urlTransparencia = "https://api.ubicity.com.br/es/legado/transparencia/scraping?name=" + nome;
    $.get(urlTransparencia, function (data, status) {
	maybeGerarRelatorio('transparencia');
        if (data.qt == 1) {
            var destinoTransparencia = "resultado-transparencia";
            var destinoHashTransparencia = "#" + destinoTransparencia;

            $("#transparencia").append(
                $("#box-transparencia").clone(false).prop("id", destinoTransparencia).after("#box-transparencia")
            );
            $(destinoHashTransparencia).css("display", '');

            $(destinoHashTransparencia).find('#transparencia-username').html("");
            $(destinoHashTransparencia).find('#transparencia-cargo').html("");
            $(destinoHashTransparencia).find('#transparencia-empresa').html("");

            $(destinoHashTransparencia).find('#transparencia-selecionado').prop('id', "transparencia-selecionado" + i);

            $(destinoHashTransparencia).find('#transparencia-username').append(data.results.name);
            $(destinoHashTransparencia).find('#transparencia-cargo').append(data.results.dsc_cargo);
            $(destinoHashTransparencia).find('#transparencia-empresa').append(data.results.orgao);


            $(document).on('click', '.transparenciaCheck', function () {
                if ($(this).is(':checked')) {
                    $('.transparenciaCheck').on('change', function () {
                        $('.transparenciaCheck').not(this).prop('checked', false);
                    });

                    nomeFuncionario = data.results.name;
                    salarioLiquido = data.results.gain;
                    nomeEmpresa = data.results.empresa;
                    cargo = data.results.dsc_cargo;
                    tempoCargo = data.results.tempo_no_cargo;
                    endereco = data.results.UF;
                    cpfTransparencia = data.results.cpf;
                    usuarioTransparenciaSelecionadoProfissao = data.results.dsc_cargo;

                } else {
                    nomeFuncionario = null;
                    salarioLiquido = null;
                    cargo = null;
                    nomeEmpresa = null;
                    usuarioTransparenciaSelecionadoProfissao = null;
                    tempoCargo = null;
                    endereco = null;
                    cpfTransparencia = null;
                }
            });

        } else {
            for (var i = 0; i <= data.results.length - 1; i++) {

                var destinoTransparencia = "resultado-transparencia" + i;
                var destinoHashTransparencia = "#" + destinoTransparencia;

                $("#transparencia").append(
                    $("#box-transparencia").clone(false).prop("id", destinoTransparencia).after("#box-transparencia")
                );
                $(destinoHashTransparencia).css("display", '');

                $(destinoHashTransparencia).find('#transparencia-username').html("");
                $(destinoHashTransparencia).find('#transparencia-cargo').html("");
                $(destinoHashTransparencia).find('#transparencia-empresa').html("");

                $(destinoHashTransparencia).find('#transparencia-selecionado').prop('id', "transparencia-selecionado" + i);
                $(destinoHashTransparencia).find('#transparencia-selecionado' + i).prop('value', i);

                $(destinoHashTransparencia).find('#transparencia-username').append(data.results[i].name);
                $(destinoHashTransparencia).find('#transparencia-cargo').append(data.results[i].dsc_cargo);
                $(destinoHashTransparencia).find('#transparencia-empresa').append(data.results[i].orgao);

            }

            $(document).on('click', '.transparenciaCheck', function () {
                if ($(this).is(':checked')) {
                    $('.transparenciaCheck').on('change', function () {
                        $('.transparenciaCheck').not(this).prop('checked', false);
                    });
                    var index = document.getElementById(this.id).value;

                    nomeFuncionario = data.results[index].name;
                    salarioLiquido = data.results[index].gain;
                    nomeEmpresa = data.results[index].empresa;
                    cargo = data.results[index].dsc_cargo;
                    tempoCargo = data.results[index].tempo_no_cargo;
                    endereco = data.results[index].UF;
                    usuarioTransparenciaSelecionadoProfissao = data.results[index].dsc_cargo;
                    cpfTransparencia = data.results[index].cpf;
                    console.log(cpfTransparencia);

                } else {
                    nomeFuncionario = null;
                    salarioLiquido = null;
                    cargo = null;
                    nomeEmpresa = null;
                    usuarioTransparenciaSelecionadoProfissao = null;
                    tempoCargo = null;
                    endereco = null;
                    cpfTransparencia = null;
                }
            });

        }

    }).fail(function (res) {
	APISelecionada('transparencia');
        APItransparencia = true;
	maybeGerarRelatorio('transparencia');
        document.getElementById("nao-encontrado-s").style.display = '';
    });


    // https://dev.api.ubicity.com.br/es/legado/escolaridade?name=Fabio+Mesquita+Buiati (edited)
    urlEscolaridade = "https://api.ubicity.com.br/es/escolaridade?name=" + nome;
    $.get(urlEscolaridade, function (res, status) {

	maybeGerarRelatorio('escolaridade');
        APIescolaridade = true;

	res.dados.forEach(function (data, index) {

	    if (!((data.profissional.length == 0) && (data.educacao.idiomas.length == 0) && (data.educacao.academico.length == 0) && (data.educacao.complementar.length == 0))) {

		var destinoEscolaridade = "box-escolaridade" + index;
		var destinoHashEscolaridade = "#" + destinoEscolaridade;

		if (data.educacao.idiomas.length != 0) {

		    for (var i = 0; i < data.educacao.idiomas.length; i++) {
			if (!(idiomas[index]))
			    idiomas[index] = [];
			idiomas[index][i] = data.educacao.idiomas[i].idioma;
		    }

		}

		if (data.educacao.academico.length != 0) {

		    for (var k = 0; k < data.educacao.academico.length; k++) {
			if (!(academico[index]))
			    academico[index] = [];
			if (!(titulo[index]))
			    titulo[index] = [];
			academico[index][k] = data.educacao.academico[k].instituicao;
			titulo[index][k] = data.educacao.academico[k].titulo;
		    }

		}

		$("#escolaridade").append(
		    $('#box-escolaridade').clone(false).prop('id', destinoEscolaridade).after("#box-escolaridade")
		);

		console.log($(destinoHashEscolaridade));

		$(destinoHashEscolaridade).css("display", '');

		$(destinoHashEscolaridade).find('#escolaridade-selecionado').prop('id', "escolaridade-selecionado" + index);
                $(destinoHashEscolaridade).find('#escolaridade-selecionado' + index).prop('value', index);

		$(destinoHashEscolaridade).find('#escolaridade-username').append(data.resultado);

		if (data.educacao.academico.length != 0) {
		    $(destinoHashEscolaridade).find('#escolaridade-titulo').append(data.educacao.academico[0].titulo);
		    $(destinoHashEscolaridade).find('#escolaridade-instituicao').append(data.educacao.academico[0].instituicao);
		}

	    }

	});

	$(document).on('click', '.escolaridadeCheck', function () {
	    if ($(this).is(':checked')) {

                $('.escolaridadeCheck').on('change', function () {
		    $('.escolaridadeCheck').not(this).prop('checked', false);
                });

		var index = document.getElementById(this.id).value;
		console.log(index);
		escolaridadeIndex = index;

                $('#ubi_idiomas').html(getIdioma(index));

	    } else {
                $('#ubi_escolaridade').html("Não Encontrado");
                $('#ubi_idiomas').html("Não Encontrado");
	    }
        });

    }).fail(function () {
	APISelecionada('escolaridade');
        APIescolaridade = true;
	maybeGerarRelatorio('escolaridade');
        document.getElementById("nao-encontrado-e").style.display = '';
    });

    //API que retorna os perfils do facebook
    ubi_URLFacebook = "https://api.ubicity.com.br/es/legado/redes-sociais/facebook?name=" + nome;
    $.get(ubi_URLFacebook, function (data, status) {
        APIfacebook = true;
	maybeGerarRelatorio('facebook');
        for (i = 0; i <= data.data.length - 1; i++) {

            var destinoFacebook = "resultado-facebook" + i;
            var destinoHashFacebook = "#" + destinoFacebook;

            $("#facebook").append(
                $("#box-facebook").clone(false).prop('id', destinoFacebook).after("#box-facebook")
            );

            //Pega o link do perfil
            perfilFacebook = data.data[i].link;

            $(destinoHashFacebook).find('#perfil-facebook').prop('href', perfilFacebook);
            $(destinoHashFacebook).css("display", '');

            //Limpa os campos html de nome
            $(destinoHashFacebook).find('#facebook-username').html("");


            $(destinoHashFacebook).find('#facebook-selecionado').prop('id', "facebook-selecionado" + i);
            $(destinoHashFacebook).find('#facebook-selecionado' + i).prop('value', i);

            $(destinoHashFacebook).find('#facebook-username').append(data.data[i].name);

	    if (data.data[i].picture) {
		$(destinoHashFacebook).find('#facebook-user-photo').prop('src', data.data[i].picture.data.url);
	    }

        }

        $(document).on('click', '.facebookCheck', function () {
            if ($(this).is(':checked')) {
                $('.facebookCheck').on('change', function () {
                    $('.facebookCheck').not(this).prop('checked', false);
                });
                var index = document.getElementById(this.id).value;

                usuarioFacebookSelecionado = data.data[index].link;
                usuarioFacebookSelecionadoFoto = data.data[index].picture.data.url;
                $('#ubi_URLFacebook').unbind('click');
                $('#ubi_URLFacebook').prop('href', usuarioFacebookSelecionado);
                $('#ubi_URLFacebook').html("Ver Perfil!");

            } else {
                usuarioFacebookSelecionado = "";
                usuarioFacebookSelecionadoFoto = null;
                $('.linkSocial').on('click', function (e) {
                    e.preventDefault();
                });
            }
        });


    }).fail(function () {
	APISelecionada('facebook');
        APIfacebook = true;
	maybeGerarRelatorio('facebook');
        //document.getElementById("nao-encontrado-f").style.display = '';
    });

    //API que retorna os perfils do google
    ubi_URLGooglePlus = "https://api.ubicity.com.br/es/legado/redes-sociais/google-plus?name=" + nome;
    $.get(ubi_URLGooglePlus, function (data, status) {
        cidadesAnteriores = "";
	maybeGerarRelatorio('googlep');
        urlSociais = [];
        APIgplus = true;
        if (data.status == "error") {
            APIgplus = true;
            document.getElementById("nao-encontrado-g").style.display = '';
            document.getElementById("resultadoGoogle0").style.display = 'none';
        }
        for (i = 0; i <= data.data.length - 1; i++) {

            var destinoGoogle = "resultadoGoogle" + i;
            var destinoHashGoogle = "#" + destinoGoogle;


            $("#google-plus").append(
                $("#box-google-plus").clone(false).prop('id', destinoGoogle).after("#box-google-plus")
            );
            $(destinoHashGoogle).css("display", '');

            $(destinoHashGoogle).find('#google-plus-username').html("");
            $(destinoHashGoogle).find('#google-plus-localizacao').html("");

            $(destinoHashGoogle).find('#google-selecionado').prop('id', "google-selecionado" + i);

            $(destinoHashGoogle).find('#google-selecionado' + i).prop('value', i);

            $(destinoHashGoogle).find('#google-plus-username').append(data.data[i].displayName);
            if (data.data[i].hasOwnProperty('placesLived')) {
                $(destinoHashGoogle).find('#google-plus-localizacao').append(data.data[i].placesLived[0].value);
            }
            $(destinoHashGoogle).find('#google-plus-photo').prop('src', data.data[i].image.url.replace("sz=50", "sz=400"));
            $(destinoHashGoogle).find('#perfil-google').prop('href', data.data[i].url);

        }

        $(document).on('click', '.googleCheck', function () {
            if ($(this).is(':checked')) {
                $('.googleCheck').on('change', function () {
                    $('.googleCheck').not(this).prop('checked', false);
                });
                var index = document.getElementById(this.id).value;
                tituloSecundario = [];
                instituicaoSecundario = [];
                console.log(data.data[index]);
                    // for(var k = 0; k <= data.data[index].organizations.length;k++){
                    //     if(data.data[index].organizations[k].type == "school"){
                    //         instituicaoSecundario[k] = data.data[index].organizations[k].name
                    //          console.log("Insituticao " + instituicaoSecundario);
                    //         tituloSecundario[k] = data.data[index].organizations[k].title;
                    //         console.log("Titulo " + tituloSecundario);
                    //     }
                    // }
                usuarioGoogleSelecionado = data.data[index].url;
                usuarioGoogleSelecionadoFoto = data.data[index].image.url;
                usuarioGoogleSelecionadoCidade = data.data[index].placesLived[0].value;
                foto = data.data[index].image.url;
                usuarioGoogleSelecionadoProfissao = data.data[index].occupation;
                usuarioGoogleSelecionadoBio = data.data[index].aboutMe;
                estadoCivil = data.data[index].relationshipStatus;
                pensamento = data.data[index].tagline;
                if (data.data[index].placesLived[0].value != null || data.data[index].placesLived[0].value != undefined) {
                    cidade = data.data[index].placesLived[0].value;
                }
                nome = data.data[index].displayName;
                for (var i = 0; i <= data.data[index].placesLived.length - 1; i++) {
                    if (i == data.data[index].placesLived.length) {
                        cidadesAnteriores += data.data[index].placesLived[i].value;
                    } else {
                        cidadesAnteriores += data.data[index].placesLived[i].value + ", ";
                    }
                }
                if (data.data[index].urls != null && data.data[index].urls != undefined) {
                    for (var j = 0; j <= data.data[index].urls.length - 1; j++) {
                        urlSociais[j] = data.data[index].urls[j].value;
                        urlEmail[j] = data.data[index].urls[j].label;
                    }

                    for (var k = 0; k <= urlSociais.length; k++) {
                        var redesocial = urlSociais[k];
                        var emailUrlSocial = urlEmail[k];
                        if (redesocial != null && redesocial != undefined || emailUrlSocial != null && emailUrlSocial != null) {
                            var verificacao = redesocial.substring(0, 22);
                            var verificacaoEmail = emailUrlSocial.substring(0, 30);
                        }
                        if (verificacao.match(/youtube/)) {
                            youtube = urlSociais[k];
                            $('#ubi_URLYoutube').unbind('click');
                            $('#ubi_URLYoutube').html("Ver perfil!");
                            $('#ubi_URLYoutube').prop('href', youtube);
                        } else if (verificacao.match(/linkedin/)) {
                            linkedin = urlSociais[k];
                            $('#ubi_URLLinkedin').unbind('click');
                            $('#ubi_URLLinkedin').html("Ver perfil!");
                            $('#ubi_URLLinkedin').prop('href', linkedin);
                            console.log(linkedin);
                        } else if (verificacaoEmail.match(/@/)) {
                            emailUrl = verificacaoEmail;
                            console.log(emailUrl);
                        }
                    }
                }
                console.log(urlSociais);
                console.log(urlEmail);
                console.log();
                $('#ubi_URLGooglePlus').unbind('click');
                $('#ubi_URLGooglePlus').html("Ver Perfil!");
                $('#ubi_URLGooglePlus').prop('href', usuarioGoogleSelecionado);

            } else {
                cidadesAnteriores = [];
                usuarioGoogleSelecionado = null;
                usuarioGoogleSelecionadoFoto = null;
                usuarioGoogleSelecionadoCidade = null;
                foto = "";
                usuarioGoogleSelecionadoBio = null;
                biografia = "";
                estadoCivil = "";
                pensamento = "";
                usuarioGoogleSelecionadoProfissao = null;
                nome = "";
                $('.linkSocial').on('click', function (e) {
                    e.preventDefault();
                });
            }
        });

    }).fail(function() {
	APISelecionada('googlep');
	maybeGerarRelatorio('googlep');
	document.getElementById("nao-encontrado-g").style.display = '';
    });

    //API que retorna os perfils do Twitter
    ubi_URLTwitter = "https://api.ubicity.com.br/es/legado/redes-sociais/twitter?name=" + nome;
    $.get(ubi_URLTwitter, function (data, status) {
        APItwitter = true;
	maybeGerarRelatorio('twitter');
        for (i = 0; i <= data.data.length - 1; i++) {

            var destinoTwitter = "resultadoTwitter" + i;
            var destinoHashTwitter = "#" + destinoTwitter;

            $("#twitter").append(
                $("#box-twitter").clone(false).prop('id', destinoTwitter).after("#box-twitter")
            );

            usuarioTwitterSelecionado = "http://www.twitter.com.br/" + data.data[i].screen_name;
            $(destinoHashTwitter).find('#twitter-perfil').prop('href', usuarioTwitterSelecionado);
            $(destinoHashTwitter).css("display", '');

            $(destinoHashTwitter).find('#twitter-username').html("");
            $(destinoHashTwitter).find('#twitter-localizacao').html("");
            $(destinoHashTwitter).find('#twitter-selecionado').prop('id', "twitter-selecionado" + i);
            $(destinoHashTwitter).find('#twitter-selecionado' + i).prop('value', i);

            $(destinoHashTwitter).find('#twitter-username').append(data.data[i].name);
            $(destinoHashTwitter).find('#twitter-localizacao').append(data.data[i].location);
            $(destinoHashTwitter).find('#twitterUserPhoto').prop('src', data.data[i].profile_image_url.replace("_normal", "_400x400"));

        }

        $(document).on('click', '.twitterCheck', function () {
            if ($(this).is(':checked')) {
                $('.twitterCheck').on('change', function () {
                    $('.twitterCheck').not(this).prop('checked', false);
                });

                var index = document.getElementById(this.id).value;
                usuarioTwitterSelecionado = "http://www.twitter.com.br/" + data.data[index].screen_name;
                usuarioTwitterSelecionadoFoto = data.data[index].profile_image_url;
                usuarioTwitterSelecionadoCidade = data.data[index].location;
                usuarioTwitterSelecionadoBio = data.data[index].description;

                $('#ubi_URLTwitter').unbind('click');
                $('#ubi_URLTwitter').html("Ver Perfil!");
                $('#ubi_URLTwitter').prop('href', usuarioTwitterSelecionado);


            } else {
                usuarioTwitterSelecionado = "";
                usuarioTwitterSelecionadoFoto = null;
                usuarioTwitterSelecionadoCidade = null;
                usuarioTwitterSelecionadoBio = null;
                biografiaSecundaria = "";
                $('.linkSocial').on('click', function (e) {
                    e.preventDefault();
                });
            }
        });
    }).fail(function () {
	APISelecionada('twitter');
        APItwitter = true;
	maybeGerarRelatorio('twitter');
        //document.getElementById("nao-encontrado-t").style.display = '';
    });

    if (email) {
	urlEmailCoorporativo = "https://api.ubicity.com.br/es/dominio?email=" + email;
	$.get(urlEmailCoorporativo, function (data, status) {
            // retornaRequisicao();
	    maybeGerarRelatorio('email');
            console.log(data);
            if (data.description != "Unavailable"){
		descricaoCoorporativo = data.description;
            }else{
		descricaoCoorporativo = null;
            }

	    if (data.longitude && data.latitude) {
		$('.map-box iframe').attr('src','https://www.google.com/maps/embed/v1/place?q=' + data.latitude + ',' + data.longitude + '&key=AIzaSyBMf9V4QUuzhWIgqmSqWpcpmxZYMazdbrk');
		console.log('asiodaaaaaaaaa');
	    } else
		$('.map-box').css('display','none');

            if (data.keywords != "Unavailable"){
		for(var i = 0; i <= data.keywords.length -1; i ++){
                    console.log("keyword" + data.keywords[i]);

                    var destinoPalavraChave = "resultado-palavra" + i;
                    var destinoHashPalavraChave = "#" + destinoPalavraChave;

                    $("#palavras").append(
			$("#palavras-chave").clone(false).prop('id', destinoPalavraChave).after("#palavras-chave")
                    );

                    //Pega o link do perfil

                    $(destinoHashPalavraChave).css("display", '');
                    $(destinoHashPalavraChave).find('#palavras-chave').append(data.keywords[i]);
                    $("#resultado-palavra" + i).html(data.keywords[i]);

		}
            }else {
		palavrasChaves = null;
            }

            if (data.social_networks != "Unavailable"){
		for (var k = 0; k <= data.social_networks.facebook.length -1; k ++){

                    var destinoFacebookCoorporativo = "resultado-facebook-coorporativo" + k;
                    var destinoHashFacebookCoorporativo = "#" + destinoFacebookCoorporativo;

                    $("#facebook-coorporativo").append(
			$("#facebook-coorporativo-link").clone(false).prop('id', destinoFacebookCoorporativo).after("#facebook-coorporativo-link")
                    );

                    $("#resultado-facebook-coorporativo" + k).html(data.social_networks.facebook[k] + "<br>");
                    $("#resultado-facebook-coorporativo" + k).prop('href', data.social_networks.facebook[k]);

		}
		for (var j = 0; j <= data.social_networks.twitter.length -1; j ++){

                    var destinoTwitterCoorporativo = "resultado-twitter-coorporativo" + j;
                    var destinoHashTwitterCoorporativo = "#" + destinoTwitterCoorporativo;

                    $("#twitter-coorporativo").append(
			$("#twitter-coorporativo-link").clone(false).prop('id', destinoTwitterCoorporativo).after("#twitter-coorporativo-link")
                    );

                    $("#resultado-twitter-coorporativo" + j).html(data.social_networks.twitter[j] + "<br>");
                    $("#resultado-twitter-coorporativo" + j).prop('href', data.social_networks.twitter[j]);

		}
		for (var k = 0; k <= data.social_networks.facebook.length -1; k ++){

                    var destinoFacebookCoorporativo = "resultado-facebook-coorporativo" + k;
                    var destinoHashFacebookCoorporativo = "#" + destinoFacebookCoorporativo;

                    $("#facebook-coorporativo").append(
			$("#facebook-coorporativo-link").clone(false).prop('id', destinoFacebookCoorporativo).after("#facebook-coorporativo-link")
                    );

                    $("#resultado-facebook-coorporativo" + k).html(data.social_networks.facebook[k] + "<br>");
                    $("#resultado-facebook-coorporativo" + k).prop('href', data.social_networks.facebook[k]);

		}
		for (var l = 0; l <= data.social_networks.linkedin.length -1; l ++){
                    var destinoLinkedinCoorporativo = "resultado-linkedin-coorporativo" + l;
                    var destinoHashLinkedinCoorporativo = "#" + destinoLinkedinCoorporativo;

                    $("#linkedin-coorporativo").append(
			$("#linkedin-coorporativo-link").clone(false).prop('id', destinoLinkedinCoorporativo).after("#linkedin-coorporativo-link")
                    );

                    $("#resultado-linkedin-coorporativo" + l).html(data.social_networks.linkedin[l] + "<br>");
                    $("#resultado-linkedin-coorporativo" + l).prop('href', data.social_networks.linkedin[l]);
		}
	    }
	}).fail(function () {
	    APISelecionada('email');
	    maybeGerarRelatorio('email');
	    $('#map-box').css('display','none');
	});

    } else {

	$('#map-box').css('display','none');
	APISelecionada('email');
	maybeGerarRelatorio('email');

    }

    /*var intervalo = setInterval(function () {
      if (APItransparencia == true || APIfacebook == true || APItwitter == true || APIgplus == true || APIescolaridade == true) {
      retornaRequisicao();
      clearInterval(intervalo);
      }
      }, 1000);*/


} // fim da funcao pesquisa nome
//
// function voltar() {
//     limpezaDosCampos();
//     limpezaDasVariaveis();
//     document.getElementById("seguimento-pesquisa").style.display = 'none';
//     document.getElementById("redes-sociais").style.display = '';
//     document.getElementById("interface").style.marginTop = "-6%";
//
// }

function limpezaDosCampos() {
    // limpa campos texto que começam com ubi_
    x = $("[id^=ubi_]");
    for (var i = 0; i < x.length; i++) {
        $(x[i]).html("Não Encontrado");
        $(x[i]).prop('href', '');
    }
}

function limpezaDasDivs() {
    // Limpa as divs que começam com resultado
    k = $("[id^=resultado]");
    for (var i = 0; i < k.length; i++) {
        $(k[i]).remove();
    }
}

function limpezaDasVariaveis() {
    cidadesAnteriores = "";
    APItransparencia = false;
    APIfacebook = false;
    APItwitter = false;
    APIgplus = false;
    APIescolaridade = false;
}

function getIdioma(index) {
    var printIdioma = "";
    for (var i = 0; i <= idiomas[index].length - 1; i++) {
        if (i == idiomas[index].length - 1) {
            printIdioma += idiomas[index][i];
        } else {
            printIdioma += idiomas[index][i] + ", ";
        }
    }
    return printIdioma;
}

function getEscolaridade() {
    var index = escolaridadeIndex;
    if (index != null && titulo[index]) {
	var printEscolaridade = "";
	for (var i = 0; i <= titulo[index].length - 1; i++) {
            if (i == titulo[index].length - 1) {
		printEscolaridade += titulo[index][i] + " | " + academico[index][i];
            } else {
		printEscolaridade += titulo[index][i] + " | " + academico[index][i] + '<br/><br/> ';
            }
	}
	return printEscolaridade;
    }
}

function getEscolaridadeSecundaria() {
    var printEscolaridadeSecundaria = "";
    for(var i = 0; i <= tituloSecundario.length -1; i ++){
        if (i == tituloSecundario.length - 1){
            printEscolaridadeSecundaria += tituloSecundario[i] + " | " + instituicaoSecundario[i];
        }else{
            printEscolaridadeSecundaria += tituloSecundario[i] + " | " + instituicaoSecundario[i] + '<br/><br/> ';
        }
    }
    return printEscolaridadeSecundaria;
}

// fuction getEscolaridadeSecundaria(){
//     var printEscolaridadeSecundaria = "";
//     for (var)
// }

function gerarResultado() {

    if (ready) {

	var todasAPIs = ['facebook','twitter','googlep','transparencia','escolaridade','email'];
	var faltandoAPIs =
	    Array.from(
		new Set(
		    todasAPIs.filter(x => !(apisSelecionadas.indexOf(x) != -1))
		)
	    );

	var msg;
	if (faltandoAPIs.length == 0)
	    msg = 'Todos perfis foram selecionados';
	else if (faltandoAPIs.length == 1)
	    msg = 'O perfil ' + faltandoAPIs.join(', ') + ' não foi selecionado';
	else if (faltandoAPIs.length == todasAPIs.length)
	    msg = 'Nenhum perfil foi selecionado'
	else
	    msg = 'Os perfis ' + faltandoAPIs.join(', ') + ' não foram selecionados';

	swal({
	    title: "Gerar relatório?",
	    text: msg,
	    type: 'info',
	    buttons: true,
	    buttons: ['Quero selecionar','Gerar relatório']
	}).then(function(isConfirm){

	    if (isConfirm) {

		document.getElementById("redes-sociais").style.display = 'none';
		document.getElementById("seguimento-pesquisa").style.display = '';
		document.getElementById("interface").style.display = 'none';
		document.getElementById("interface").style.marginTop = "-6%";
		$('#ubi_cidades-anteriores').html(cidadesAnteriores);
		$('#ubi_estadoCivil').html(estadoCivil);
		$('#ubi_pensamento').html(pensamento);

		if (getEscolaridade()!= null && getEscolaridade() != undefined && getEscolaridade() != ""){
		    $('#ubi_escolaridade').html(getEscolaridade());
		} else if (getEscolaridadeSecundaria()!= null && getEscolaridadeSecundaria() != undefined && getEscolaridadeSecundaria() != ""){
		    $('#ubi_escolaridade').html(getEscolaridadeSecundaria());
		} else {
		    $('#ubi_escolaridade').html("Não Encontrado");
		}

		if (usuarioGoogleSelecionadoBio == null && usuarioTwitterSelecionadoBio != null) {
		    biografia = usuarioTwitterSelecionadoBio;
		    $('#ubi_biografia').html(biografia);
		} else if (usuarioTwitterSelecionadoBio == null && usuarioGoogleSelecionadoBio != null) {
		    biografia = usuarioGoogleSelecionadoBio;
		    $('#ubi_biografia').html(biografia);
		} else if (usuarioGoogleSelecionadoBio != null && usuarioTwitterSelecionadoBio != null) {
		    biografia = usuarioGoogleSelecionadoBio;
		    $('#ubi_biografia').html(biografia);
		} else {
		    $('#ubi_biografia').html("Não Encontrado");
		}

		if (salarioLiquido == null) {
		    $('#salario').html("Não Encontrado");
		} else {
		    $('#salario').html(salarioLiquido);
		}

		if (nomeEmpresa == null) {
		    $('#nome-empresa').html("Não Encontrado");
		} else {
		    $('#nome-empresa').html(nomeEmpresa);
		}

		if (cargo == null) {
		    $('#cargo').html("Não Encontrado");
		} else {
		    $('#cargo').html(cargo);
		}

		if (endereco == null) {
		    $('#endereco').html("Não Encontrado");
		} else {
		    $('#endereco').html(endereco);
		}

		if (tempoCargo == null) {
		    $('#tempo-no-cargo').html("Não Encontrado");
		} else {
		    $('#tempo-no-cargo').html(tempoCargo)
		}
		if (cpf == "" && cpfTransparencia == null) {
		    $('#ubi_cpf').html("Não Encontrado");
		} else if (cpf != null && cpf != ""){
		    $('#ubi_cpf').html(cpf);
		}else{
		    $('#ubi_cpf').html(cpfTransparencia);
		}

		if (endereco == null) {

		}
		if (usuarioGoogleSelecionadoProfissao == null && usuarioTransparenciaSelecionadoProfissao != null) {
		    profissao = usuarioTransparenciaSelecionadoProfissao;
		    $('#ubi_profissao').html(profissao);
		} else if (usuarioTransparenciaSelecionadoProfissao == null && usuarioGoogleSelecionadoProfissao != null) {
		    profissao = usuarioGoogleSelecionadoProfissao;
		    $('#ubi_profissao').html(profissao);
		} else if (usuarioGoogleSelecionadoProfissao != null && usuarioTransparenciaSelecionadoProfissao != null) {
		    profissao = usuarioGoogleSelecionadoProfissao;
		    $('#ubi_profissao').html(profissao);
		} else {
		    $('#ubi_profissao').html("Não Encontrado");
		}

		if (descricaoCoorporativo == null){
		    $('#descricaoCoorporativo').html("Não Encontrado");
		}else {
		    $('#descricaoCoorporativo').html(descricaoCoorporativo);
		}

		if (usuarioGoogleSelecionadoCidade == null && usuarioTwitterSelecionadoCidade != null) {
		    cidade = usuarioTwitterSelecionadoCidade;
		    $('#ubi_cidade').html(cidade);
		} else if (usuarioTwitterSelecionadoCidade == null && usuarioGoogleSelecionadoCidade != null) {
		    cidade = usuarioGoogleSelecionadoCidade;
		    $('#ubi_cidade').html(cidade);
		} else if (usuarioGoogleSelecionadoCidade != null && usuarioTwitterSelecionadoCidade != null) {
		    cidade = usuarioGoogleSelecionadoCidade;
		    $('#ubi_cidade').html(cidade);
		} else {
		    $('#ubi_cidade').html("Não Encontrado");
		}

		if (usuarioGoogleSelecionadoFoto == null && usuarioTwitterSelecionadoFoto != null) {
		    foto = usuarioTwitterSelecionadoFoto;
		    $('#ubi_fotoUsuario').prop('src', foto.replace("_normal", "_400x400"));
		} else if (usuarioTwitterSelecionadoFoto == null && usuarioGoogleSelecionadoFoto != null) {
		    foto = usuarioGoogleSelecionadoFoto;
		    $('#ubi_fotoUsuario').prop('src', foto.replace('sz=50', 'sz=200'));
		} else if (usuarioTwitterSelecionadoFoto == null && usuarioGoogleSelecionadoFoto == null && usuarioFacebookSelecionadoFoto != null) {
		    foto = usuarioFacebookSelecionadoFoto;
		    $('#ubi_fotoUsuario').prop('src', foto);
		} else if (usuarioGoogleSelecionadoFoto != null && usuarioTwitterSelecionadoFoto != null && usuarioFacebookSelecionadoFoto != null) {
		    foto = usuarioGoogleSelecionadoFoto;
		    $('#ubi_fotoUsuario').prop('src', foto.replace('sz=50', 'sz=200'));
		} else if (usuarioFacebookSelecionadoFoto != null && usuarioTwitterSelecionadoFoto != null) {
		    foto = usuarioTwitterSelecionadoFoto;
		    $('#ubi_fotoUsuario').prop('src', foto.replace("_normal", "_400x400"));
		} else if (usuarioTwitterSelecionadoFoto != null && usuarioGoogleSelecionadoFoto != null) {
		    foto = usuarioGoogleSelecionadoFoto;
		    $('#ubi_fotoUsuario').prop('src', foto.replace('sz=50', 'sz=200'));
		} else {
		    $('#ubi_fotoUsuario').prop('src', 'enriquecer/assets/images/user.jpg');
		}

		if (telefone == "" || telefone == null || telefone == undefined) {
		    $('#ubi_telefone').html("Não Encontrado");
		} else {
		    $('#ubi_telefone').html(telefone);
		}

		if (email == "" || email == "null" || email == "undefined") {
		    if (emailUrl == "" || emailUrl == "null" || emailUrl == "undefined") {
			$('#ubi_email').html("Não Encontrado");
		    } else {
			$('#ubi_email').html(emailUrl);
		    }
		} else {
		    $('#ubi_email').html(email);
		}

		if (nome == null) {

		} else {
		    $('#ubi_username').html(nome);
		}

	    }

	});

    }
}

/*function retornaRequisicao() {
    NProgress.done();
    document.getElementById("pesquisando").style.display = 'none'; //oculta
    document.getElementById("redes-sociais").style.display = ''; //mostra
}*/

$('.linkSocial').on('click', function (e) {
    e.preventDefault();
});

var url = new URL(window.location.href);
var nameVal = url.searchParams.get("name");
var emailVal = url.searchParams.get("email");

var selected = [false,false,false];
var cadastro      = null;
var escolaridade  = null;
var transparencia = null;
var dominio       = null;

setTimeout(function () {
    if (nameVal && nameVal != '') {
	pesquisa(nameVal, emailVal, '','');
	$("#example-basic").steps({
	    headerTag: "h3",
	    bodyTag: "section",
	    transitionEffect: "slide",
	    autoFocus: false,
	    enableAllSteps: true,
	    onStepChanging: function (e, currentIndex, newIndex) {
		if (newIndex > currentIndex) {
		    return selected[newIndex - 1];
		} else {
		    return true;
		}
	    },
	    transitionEffectSpeed: 150
	});
	$('#example-basic').wizard({'nextSelector': '.next', 'previousSelector': '.prev'})
    }
}, 1000);

function selecionarCadastro (i) {

    // se o cadastro escolhido for o mesmo, nada muda
    // senão

    cadastro = null;  // escolher cadastro

    selected = [true,false,false];

}

function selecionarEscolaridade (i) {

    // se o cadastro escolhido for o mesmo, nada muda
    // senão

    escolaridade = null;  // escolher cadastro

    selected = [true,true,false];

}

function selecionarTransparencia (i) {

    // se o cadastro escolhido for o mesmo, nada muda
    // senão

    transparencia = null;  // escolher cadastro

    selected = [true,true,true];

}

function goPrev () {

    $('#example-basic').steps('previous');

}

function goNext () {

    $('#example-basic').steps('next');

}
