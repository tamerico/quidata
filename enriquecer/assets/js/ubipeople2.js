/* tamer@ubicity.com.br
março 2018


este codigo devera ser utilizado como referencia para o novo simb
porem os daddos virao ja procesados do json
*/

var testeInternoSimbUbiplaces= false;
var debug = true;
console.log("SIMB é um produto da ubicity.com.br. 2018");
var telefone_pesquisa;

var url = new URL(window.location.href);
//var nameVal = url.searchParams.get("name");
if (debug) console.log("coletando email do form");

var emailVal = url.searchParams.get("email");
if (emailVal)
  if (debug) console.log("pegou email...");
else {
  if (debug) console.log("naopegou email...");
}
var selected = [false,false,false];

var cadastroSelected      = null;
var escolaridadeSelected  = null;
var transparenciaSelected = null;

var valoresDeCadastro      = [];
var valoresDeEscolaridade  = [];
var valoresDeTransparencia = [];
var dominio = null;

var transparenciaRendered = false;
var escolaridadeRendered = false;

/* funcao final que gera o resultado decorrente das selecoes feitas manualmente para montar o perfil do usuario*/
function gerarResultado() {

    var perfilCriado = {}; /* inserir nesta variavel todos os campos para o perfil. sera gravado em banco para futuras consultas*/

    if (debug) console.log(cadastroSelected);
    if (debug) console.log(escolaridadeSelected);
    if (debug) console.log(transparenciaSelected);

    $('#nextprev').css('display','none');
    $('#redes-sociais').css('display','none');
    $('#seguimento-pesquisa').css('display','');
    $('#interface').css('display', 'none');

    if (cadastroSelected
    	&& cadastroSelected.images
    	&& cadastroSelected.images[0]) {
    	$('#ubi-foto').attr('src',cadastroSelected.images[0].url);

      perfilCriado['foto'] = cadastroSelected.images[0].url;
    }



    if (transparenciaSelected
    	&& transparenciaSelected.name) {
    	   $('#ubi-nome').html(transparenciaSelected.name);
      perfilCriado['nome'] = transparenciaSelected.name;
    } else if (escolaridadeSelected
	       && escolaridadeSelected.source_name) {
	          $('#ubi-nome').html(escolaridadeSelected.source_name);
            perfilCriado['nome'] = escolaridadeSelected.source_name;
    } else if (cadastroSelected
	       && cadastroSelected.names
	       && cadastroSelected.names[0]) {
	           $('#ubi-nome').html(cadastroSelected.names[0].display);
             perfilCriado['nome'] = cadastroSelected.names[0].display;
    }


    if (cadastroSelected
    	&& cadastroSelected.jobs
    	&& cadastroSelected.jobs[0]) {
	         $('#ubi-profissao').html(cadastroSelected.jobs[0].display);
           perfilCriado['profissao'] = cadastroSelected.jobs[0].display;
    } else if (escolaridadeSelected
	       && escolaridadeSelected.profissional
	       && escolaridadeSelected.profissional[0]) {
	           $('#ubi-profissao').html(escolaridadeSelected.profissional[0].descricao);
             perfilCriado['profissao'] = escolaridadeSelected.profissional[0].descricao;
    }

    var ubiEmails = [emailVal];
    if (cadastroSelected
	     && cadastroSelected.emails) {
	        ubiEmails = ubiEmails.concat(cadastroSelected.emails.map(x => x.address));

    }
    if (ubiEmails.length > 0) {
      cont = 0;
	       ubiEmails.forEach((email, i) => {

	          var el = $('<div class="barra-item barra-divisor" id="ubi-emails-' + i + '"><div class="principal"></div></div>');

      	    $('#ubi-emails').append(el);
      	    $('#ubi-emails-' + i + ' .principal').html(email);
            if (cont==0)
              perfilCriado['email'] = email;
            else {
              perfilCriado['email'+cont] = email;
            }
            cont ++;

	     });
    } else {

    	var el = $('<div class="barra-item barra-divisor" id="ubi-emails-err"><div class="principal">Não encontrado</div></div>');

    	$('#ubi-emails').append(el);
      perfilCriado['email'] = "email nao encontrado";

    }



    if (cadastroSelected
	     && cadastroSelected.languages) {

         if (debug) console.log("cadastroSelected.languages");
         if (debug) console.log(cadastroSelected.languages);

         //perfilCriado['idiomas'] = cadastroSelected.languages;

         // coletar dados de  cadastroSelected.languages[language]
         idiomasDicionario = { "pt": "Português",  "en": "Inglês", "es": "Espanhol", "ge": "Alemão", "fr": "Francês" }
         linguagensExibir = "";
         linguagens ="";
         for (x in cadastroSelected.languages){
           if (debug) console.log("linguagem: " + cadastroSelected.languages[x].language);

           if (cadastroSelected.languages[x].language in idiomasDicionario)
           {  //if (debug) console.log("ifou: " + idiomasDicionario[cadastroSelected.languages[x].language]);
              linguagensExibir = linguagensExibir +  idiomasDicionario[cadastroSelected.languages[x].language] + "<br>";
              linguagens = linguagens +  idiomasDicionario[cadastroSelected.languages[x].language] + ",";
            }
            else {
              //if (debug) console.log("elsou: " + cadastroSelected.languages[x].language);
              linguagensExibir = linguagensExibir +   cadastroSelected.languages[x].language + "<br>";
              linguagens = linguagens +   cadastroSelected.languages[x].language + ",";
            }

          }

          $('#ubi-idiomas .principal').html( linguagensExibir);

          perfilCriado['idiomas'] = linguagens.slice(0,-1);
    	     // $('#ubi-idiomas .principal').html(
           //          cadastroSelected.languages.map(x => x.display) // display é uma chave
           //                                    .reduce((a,b) => a + ', ' + b)
           //        );
    }



    if (cadastroSelected
	       && cadastroSelected.addresses) {
              perfilCriado['enderecos'] = cadastroSelected.addresses;
              	cadastroSelected.addresses.forEach((end, i) => {
              	    var el = $('<div class="barra-item barra-divisor" id="ubi-enderecos-' + i + '"><div class="principal"></div></div>');
              	    $('#ubi-enderecos').append(el);
              	    if (end.display) {
              		        $('#ubi-enderecos-' + i + ' .principal').html(end.display);
              	    }
              	});

    } else {
    	var el = $('<div class="barra-item barra-divisor" id="ubi-enderecos-err"><div class="principal">Não encontrado</div></div>');
    	$('#ubi-enderecos').append(el);
    }

    var ubiJobs = [];
    if (cadastroSelected
	&& cadastroSelected.jobs) {
	ubiJobs = cadastroSelected.jobs;
    }
    if (escolaridadeSelected
	     && escolaridadeSelected.profissional) {
	         ubiJobs = ubiJobs.concat(escolaridadeSelected.profissional);
    }
    if (ubiJobs.length > 0) {
      	ubiJobs.forEach((job, i) => {

      	    var el = $('<div class="barra-item barra-divisor" id="ubi-jobs-' + i + '"><div class="principal"></div><div class="complementar complementar-1"></div><div class="complementar complementar-2"></div></div>');

      	    $('#ubi-jobs').append(el);
      	    if (job.display) {
              if (debug) console.log("mostrando dados do trabalho")
          		$('#ubi-jobs-' + i + ' .principal').html(job.display);
          		$('#ubi-jobs-' + i + ' .complementar-1').html(job.organization);
          		$('#ubi-jobs-' + i + ' .complementar-2').html(parseRange(job.date_range));
      	    } else {
          		$('#ubi-jobs-' + i + ' .principal').html(job.descricao);
          		$('#ubi-jobs-' + i + ' .complementar-1').html(job.local);
          		$('#ubi-jobs-' + i + ' .complementar-2').html(job.data);
      	    }

      	});

    } else {

    	var el = $('<div class="barra-item barra-divisor" id="ubi-jobs-err"><div class="principal">Não encontrado</div></div>');
    	$('#ubi-jobs').append(el);

    }
    perfilCriado['jobs'] = ubiJobs;


    var ubiEscolaridade = [];
    if (cadastroSelected
    	&& cadastroSelected.educations) {
    	   ubiEscolaridade = cadastroSelected.educations;
    }

    if (escolaridadeSelected
    	&& escolaridadeSelected.educacao
    	&& escolaridadeSelected.educacao.academico) {
    	   ubiEscolaridade = ubiEscolaridade.concat(escolaridadeSelected.educacao.academico);
    }

    if (ubiEscolaridade.length > 0) {
    	ubiEscolaridade.forEach((esc, i) => {

    	    var el = $('<div class="barra-item barra-divisor" id="ubi-escolaridade-' + i + '"><div class="principal"></div><div class="complementar complementar-1"></div><div class="complementar complementar-2"></div></div>');

    	    $('#ubi-escolaridade').append(el);
    	    if (esc.degree) {
        		$('#ubi-escolaridade-' + i + ' .principal').html(esc.degree);
        		$('#ubi-escolaridade-' + i + ' .complementar-1').html(esc.school);
        		$('#ubi-escolaridade-' + i + ' .complementar-2').html(parseRange(esc.date_range));
    	    } else {
        		$('#ubi-escolaridade-' + i + ' .principal').html(esc.titulo);
        		$('#ubi-escolaridade-' + i + ' .complementar-1').html(esc.instituicao);
        		$('#ubi-escolaridade-' + i + ' .complementar-2').html(esc.data);
    	    }

    	});

    } else {

    	var el = $('<div class="barra-item barra-divisor" id="ubi-escolaridade-err"><div class="principal">Não encontrado</div></div>');
    	$('#ubi-escolaridade').append(el);

    }
    perfilCriado['escolaridade'] = ubiEscolaridade;


    if (cadastroSelected
	     && cadastroSelected.gender) {
	     $('#ubi-sexo').html(parseSexo(cadastroSelected.gender.content));
       perfilCriado['sexo'] = cadastroSelected.gender;
    }

    if (cadastroSelected
	     && cadastroSelected.dob) {
	        $('#ubi-idade').html(parseData(cadastroSelected.dob.date_range.start) + ' (' + parseIdade(cadastroSelected.dob.display) + ' anos)');

          perfilCriado['data-nascimento'] = parseData(cadastroSelected.dob.date_range.start);
          perfilCriado['idade'] =  parseIdade(cadastroSelected.dob.display);
    }

    if (transparenciaSelected
	     && transparenciaSelected.cpf) {
	        $('#ubi-cpf').html(transparenciaSelected.cpf);
          perfilCriado['cpf'] = transparenciaSelected.cpf;
    }

    if (cadastroSelected
	     && cadastroSelected.phones) {
         // tamer
         telefonesExibir = "";
         telefones = "";
         for (x in cadastroSelected.phones)
          {

                telefonesExibir = telefonesExibir  + cadastroSelected.phones[x].display_international + "<br>";
                telefones = telefones  + cadastroSelected.phones[x].display_international + ",";
                if (telefone_pesquisa){
                  telefone_pesquisaLimpo = telefone_pesquisa.replace(/ /g,"");
                  //telefone_pesquisaLimpo = telefone_pesquisaLimpo.replace(/-/g,"");
                  telefone_pesquisaLimpo = telefone_pesquisaLimpo.replace(/[()-+*]/g,"");
                  //telefone_pesquisaLimpo = telefone_pesquisaLimpo.replace(")","");
                  telefoneCadastro = cadastroSelected.phones[x].display_international.replace(/ /g,"");
                  //telefoneCadastro = telefoneCadastro.replace(/-/g,"");
                  telefoneCadastro = telefoneCadastro.replace(/[()-]/g,"");
                  //telefoneCadastro = telefoneCadastro.replace(")","");

                  /*
                    telefone_validado significa que a pesquisa incluiu o parametro telefone e o resultado obtido contem o mesmo telefone.
                  */
                  console.log(telefone_pesquisaLimpo + "=" + telefoneCadastro + " ?" + cadastroSelected.phones[x].display_international + " ? "+ telefoneCadastro.indexOf(telefone_pesquisaLimpo));
                  if (telefoneCadastro.indexOf(telefone_pesquisaLimpo) !=-1 )// se o telefone utilizado na pesquisa for encontrado na pesquisa entao ele é considerado validado
                    perfilCriado['telefone_validado'] = cadastroSelected.phones[x].display_international;
                }

            }

          $('#ubi-telefones').html( telefonesExibir);
	        //$('#ubi-telefones').html(cadastroSelected.phones.map(x => x.display_international).reduce((a,b) => a + ', ' + b));
          perfilCriado['telefones'] = telefones;

    }

    if (transparenciaSelected
	     && transparenciaSelected.empresa) {
	        $('#ubi-empresa').html(transparenciaSelected.empresa);
          perfilCriado['empresa'] = transparenciaSelected.empresa;
    }

    if (transparenciaSelected
	     && transparenciaSelected.dsc_cargo) {
	        $('#ubi-cargo').html(transparenciaSelected.dsc_cargo);
          perfilCriado['cargo'] = transparenciaSelected.dsc_cargo;
    }

    if (transparenciaSelected
	     && transparenciaSelected.gain) {
	        $('#ubi-salario').html(transparenciaSelected.gain);
          perfilCriado['salario'] = transparenciaSelected.gain;
    }

    if (transparenciaSelected
	     && transparenciaSelected.tempo_no_cargo) {
	     $('#ubi-tempo').html(transparenciaSelected.tempo_no_cargo);
       perfilCriado['tempo_no_cargo'] = transparenciaSelected.tempo_no_cargo;
    }


    // dominio
    if (dominio && dominio.description) {
        if (debug) console.log("pegou dados do dominio para descricao")
	       $('#ubi-descricao').html(dominio.description);
         perfilCriado['dominio_site'] = dominio;
         perfilCriado['dominio_descricao'] = dominio.description;
    }
    else {
      if (debug) console.log("nao pegou dados do dominio");
      if (debug) console.log(dominio);
      if (debug) console.log("nao pegou mesmo");

    }



    if (dominio
    	&& dominio.social_networks
    	&& dominio.social_networks.facebook
    	&& (dominio.social_networks.facebook.length > 0)) {
        	$('#ubi-facebook-url').attr('href',dominio.social_networks.facebook.reduce((a,b) => a + ', ' + b));
        	$('#ubi-linkedin-url').text(dominio.social_networks.linkedin.reduce((a,b) => a + ', ' + b));
        	$('#ubi-facebook-url').attr('target','_blank');
    }

    if (dominio
    	&& dominio.social_networks
    	&& dominio.social_networks.twitter
    	&& (dominio.social_networks.twitter.length > 0)) {
        	$('#ubi-twitter-url').attr('href',dominio.social_networks.twitter.reduce((a,b) => a + ', ' + b));
        	$('#ubi-linkedin-url').text(dominio.social_networks.linkedin.reduce((a,b) => a + ', ' + b));
        	$('#ubi-twitter-url').attr('target','_blank');
    }

    if (dominio
    	&& dominio.social_networks
    	&& dominio.social_networks.linkedin
    	&& (dominio.social_networks.linkedin.length > 0)) {
      	$('#ubi-linkedin-url').attr('href',dominio.social_networks.linkedin.reduce((a,b) => a + ', ' + b));
      	$('#ubi-linkedin-url').text(dominio.social_networks.linkedin.reduce((a,b) => a + ', ' + b));
      	$('#ubi-linkedin-url').attr('target','_blank');
    }

    if (dominio
    	&& dominio.longitude
    	&& dominio.latitude) {
	         $('.map-box iframe').attr('src','https://www.google.com/maps/embed/v1/place?q=' + dominio.latitude + ',' + dominio.longitude + '&key=AIzaSyBMf9V4QUuzhWIgqmSqWpcpmxZYMazdbrk');
    }

    if (cadastroSelected
	       && cadastroSelected.user_ids) {

              	cadastroSelected.user_ids.forEach(id => {

                	    var x      = id.content.split('@');
                	    var domain = x[1];
                	    var user   = x[0];

                	    if (domain == 'facebook') {
                          $('#ubi-facebook').attr('href','http://facebook.com/profile.php?id=' + user);
                		      $('#ubi-facebook').text("ver perfil");
                          perfilCriado['social_facebook'] = 'http://facebook.com/profile.php?id=' + user;

                	    } else if (domain == 'twitter') {

                        		$('#ubi-twitter').attr('href','https://twitter.com/intent/user?user_id=' + user);
                        		$('#ubi-twitter').text("ver perfil");
                            perfilCriado['social_twitter'] = 'https://twitter.com/intent/user?user_id=' + user;

                	    } else if (domain == 'google') {

                        		$('#ubi-google').attr('href','https://plus.google.com/' + user);
                        		$('#ubi-google').text("ver perfil");
                            perfilCriado['social_google'] = 'https://plus.google.com/' + user;

                	    } else if (domain == 'linkedin') {

                        		$('#ubi-linkedin').attr('href','http://www.linkedin.com/profile/view?id=' + user);
                        		$('#ubi-linkedin').text("ver perfil");
                            perfilCriado['social_linkedin'] = 'http://www.linkedin.com/profile/view?id=' + user;

                	    } else if (domain == 'youtube') {

                		        $('#ubi-youtube').text("ver perfil (falhou)");

                	    }

              	});

    }

    $('.barra').each((i,el) => {

	var h = $(el).height();
	if (debug) console.log(el);
	if (debug) console.log(h);
	if (h > 190) {

	    $(el).addClass('barra-collapse');
	    $(el).addClass('barra-collapse-' + i);
	    $(el).addClass('closed');
	    $(el).after('<div class="barra-seta" onclick="barraCollapse(' + i + ')">Mais</div>');

	}

    })


    //bla
        if (debug) console.log("perfil json: " + perfilCriado);
        if (debug) console.log( perfilCriado);

        if (debug) console.log("gravar perfil no banco");
        gravarPerfil(perfilCriado);/* chama api para gravar no banco*/

} // fim do gerarResultado

function barraCollapse (index) {

        var el = $('.barra-collapse-' + index);

        if (el.hasClass('closed')) {

    	el.removeClass('closed');
    	el.next().text('Menos');

        } else {

    	el.addClass('closed');
    	el.next().text('Mais');

        }

        if (debug) console.log('barracollapse');
        if (debug) console.log(el);

}


/* coleta dos dados das outras abas para montar o perfil do usuario*/
function gerarRelatorio () {

    if (selected[0] && selected[1] && selected[2]) {
	     gerarResultado();
    }

}



/*
  Esta funcao  é inserida automaticamente em cada registro encontrado no resultado do res.info
  Quando um box de informacao de um resultado é clicado ele ativa esta funcao que vai executar a filtragem das informacoes para o proximo passo , escolaridade e transparencia
*/
function selecionarCadastro (i) {

    if  (!cadastroSelected ||
	 (cadastroSelected != valoresDeCadastro[i])) {

      	cadastroSelected = valoresDeCadastro[i];  //escolher cadastro
        if (debug) console.log ("cadastroSelected");
        if (debug) console.log (cadastroSelected);


      	selected = [true,true,true];

      	$('.wizard > .steps .last a').css('opacity','1').css('background-color', 'green').css('cursor','pointer');

      	$('.selecionar')
      	    .removeClass('active');
      	$('.selecionar')
      	    .html('Selecionar<i class="fa fa-check" id="selected-transparencia" style="float: right;margin: 5px;visibility: hidden;" aria-hidden="true"></i>');

      	$('#dadoscadastrais-card-' + i + ' .selecionar')
      	    .html('Selecionado<i class="fa fa-check" id="selected-transparencia" style="float: right;margin: 5px;" aria-hidden="true"></i>');
      	$('#dadoscadastrais-card-' + i + ' .selecionar')
      	    .addClass('active');

      	$('#selected-cadastro').css('display','block');
      	$('#selected-escolaridade').css('display','none');
      	$('#selected-transparencia').css('display','none');



      	valoresDeEscolaridade
      	    .filter(x => cadastroSelected.names.map(x => x.display).indexOf(x.source_name) !=- -1);
      	valoresDeTransparencia
      	    .filter(x => cadastroSelected.names.map(x => x.display).indexOf(x.source_name) !=- -1);


        if (debug) console.log("valoresDeEscolaridade");
      	if (debug) console.log(valoresDeEscolaridade);
        if (debug) console.log("valoresDeTransparencia");
      	if (debug) console.log(valoresDeTransparencia);

      	if (!escolaridadeRendered) {
      	    renderEscolaridade()
      	}

      	if (!transparenciaRendered) {
      	    renderTransparencia()
      	}

    } else {

        // reorganiza a apresentacao do front

      	cadastroSelected = null;
      	selected = [false,false,false];

      	$('.wizard > .steps .last a').css('opacity','1').css('background-color', '#5d9c85').css('cursor','pointer');

      	$('.selecionar')
      	    .removeClass('active');
      	$('.selecionar')
      	    .html('Selecionar');

      	$('#selected-cadastro').css('display','none');
      	$('#selected-escolaridade').css('display','none');
      	$('#selected-transparencia').css('display','none');

    }

}

function selecionarEscolaridade (i) {

    if  (!escolaridadeSelected ||
	 (escolaridadeSelected != valoresDeEscolaridade[i]))
   {

      	escolaridadeSelected = valoresDeEscolaridade[i];

      	selected = [true,true,true];

      	$('.wizard > .steps .last a').css('opacity','1').css('background-color', 'green').css('cursor','pointer');

      	$('#transparencia-section .selecionar')
      	    .removeClass('active');
      	$('#transparencia-section .selecionar')
      	    .html('Selecionar');

      	$('#escolaridade-section .selecionar')
      	    .removeClass('active');
      	$('#escolaridade-section .selecionar')
      	    .html('Selecionar<i class="fa fa-check" id="selected-escolaridade" style="float: right;margin: 5px;visibility: hidden;" aria-hidden="true"></i>');

      	$('#escolaridade-card-' + i + ' .selecionar')
      	    .html('Selecionado<i class="fa fa-check" id="selected-escolaridade" style="float: right;margin: 5px;" aria-hidden="true"></i>');
      	$('#escolaridade-card-' + i + ' .selecionar')
      	    .addClass('active');

      	$('#selected-escolaridade').css('display','block');
      	$('#selected-transparencia').css('display','none');

    } else
    {

      	escolaridadeSelected = null;

      	selected = [true,true,true];

      	$('.wizard > .steps .last a').css('opacity','1').css('background-color', 'green').css('cursor','pointer');

      	$('#transparencia-section .selecionar')
      	    .removeClass('active');
      	$('#transparencia-section .selecionar')
      	    .html('Selecionar');

      	$('#escolaridade-section .selecionar')
      	    .removeClass('active');
      	$('#escolaridade-section .selecionar')
      	    .html('Selecionar');

      	$('#selected-escolaridade').css('display','none');
      	$('#selected-transparencia').css('display','none');

    }

    escolaridadeRendered = true;

}

function selecionarTransparencia (i) {

    if  (!transparenciaSelected ||
	 (transparenciaSelected != valoresDeTransparencia[i])) {

	transparenciaSelected = valoresDeTransparencia[i];

	selected = [true,true,true];

	$('.wizard > .steps .last a').css('opacity','1').css('background-color', 'green').css('cursor','pointer');

	$('#transparencia-section .selecionar')
	    .removeClass('active');
	$('#transparencia-section .selecionar')
	    .html('Selecionar<i class="fa fa-check" id="selected-transparencia" style="float: right;margin: 5px;visibility: hidden;" aria-hidden="true"></i>');

	$('#transparencia-card-' + i + ' .selecionar')
	    .html('Selecionado<i class="fa fa-check" id="selected-transparencia" style="float: right;margin: 5px;" aria-hidden="true"></i>');
	$('#transparencia-card-' + i + ' .selecionar')
	    .addClass('active');

	$('#selected-transparencia').css('display','block');

    } else {

	transparenciaSelected = null;

	selected = [true,true,true];

	$('.wizard > .steps .last a').css('opacity','1').css('background-color', 'green').css('cursor','pointer');

	$('#transparencia-section .selecionar')
	    .removeClass('active');
	$('#transparencia-section .selecionar')
	    .html('Selecionar');

	$('#selected-transparencia').css('display','none');

    }

    transparenciaRendered = true;

}

function goPrev () {

    $('#example-basic').steps('previous');

}

function goNext () {

    $('#example-basic').steps('next');

}

function renderTransparencia () {

    if (valoresDeTransparencia.length > 0) {

        	for (i = 0; i < valoresDeTransparencia.length; i++) {

        	    // mostrar apenas os filtrados

        	    var person = valoresDeTransparencia[i];

        	    var section = 'transparencia-section';
        	    var model   = 'transparencia-card';
        	    var cloneId = model + '-' + i;

        	    $('#' + section).append(
        		$('#' + model)
        		    .clone(false)
        		    .prop('id', cloneId)
        		    .css('display','block')
        	    );

        	    $('#' + cloneId + ' .selecionar')
        		.attr('onclick', 'selecionarTransparencia(' + i + ')');

        	    if (person.name) {
        		$('#' + cloneId + ' .transparencia-nome')
        		    .html('<b>Nome:</b> ' + person.name);
        	    }
        	    if (person.gain) {
        		$('#' + cloneId + ' .transparencia-gain')
        		    .html('<b>Salário:</b> R$' + person.gain);
        	    }
        	    if (person.cpf) {
        		$('#' + cloneId + ' .transparencia-cpf')
        		    .html('<b>CPF:</b> ' + person.cpf);
        	    }
        	    if (person.empresa) {
        		$('#' + cloneId + ' .transparencia-empresa')
        		    .html('<b>Empresa:</b> ' + person.empresa);
        	    }

        	}

    } else {

	$('.nao-encontrado-transparencia').css('display', 'block');

    }

}

function renderEscolaridade () {

    if (valoresDeEscolaridade.length > 0) {

	for (i = 0; i < valoresDeEscolaridade.length; i++) {

	    // mostrar apenas os filtrados

	    var person = valoresDeEscolaridade[i];

	    var section = 'escolaridade-section';
	    var model   = 'escolaridade-card';
	    var cloneId = model + '-' + i;

	    $('#' + section).append(
		$('#' + model)
		    .clone(false)
		    .prop('id', cloneId)
		    .css('display','block')
	    );

	    $('#' + cloneId + ' .selecionar')
		.attr('onclick', 'selecionarEscolaridade(' + i + ')');

	    if (person.resultado) {
		$('#' + cloneId + ' .escolaridade-nome')
		    .html('<b>Nome:</b> ' + person.resultado);
	    }
	    if (person.profissional && person.profissional[0] && person.profissional[0].descricao) {
		$('#' + cloneId + ' .escolaridade-descricao')
		    .html('<b>Descrição:</b> ' + person.profissional[0].descricao);
	    }
	    if (person.profissional && person.profissional[0] && person.profissional[0].data) {
		$('#' + cloneId + ' .escolaridade-data')
		    .html('<b>Data:</b> ' + person.profissional[0].data);
	    }
	    if (person.profissional && person.profissional[0] && person.profissional[0].local) {
		$('#' + cloneId + ' .escolaridade-local')
		    .html('<b>Local:</b> ' + person.profissional[0].local);
	    }

	}

    } else {

	$('.nao-encontrado-escolaridade').css('display', 'block');

    }

}

/* aba principal do retorno de res.info
    esta funcao desenha os box para CADA possivel usuario para ser selecionado

*/
function renderCadastro() {
    var contadorLog = 0;
    for (i = 0; i < valoresDeCadastro.length; i++)
    {

        	contadorLog ++;
        	var person = valoresDeCadastro[i];

        	var section = 'dadoscadastrais-section';
        	var model   = 'dadoscadastrais-card';
        	var cloneId = model + '-' + i;

          // adiciona um novo campo para exibir os dados
        	$('#' + section).append(
        	    $('#' + model)
        		.clone(false)
        		.prop('id', cloneId)
        		.css('display','block')

        	);

          // adiciona evento de clique para ativar funcao.
          // como é dinamico nao existe isso no html
        	$('#' + cloneId + ' .selecionar')
        	    .attr('onclick', 'selecionarCadastro(' + i + ')');

        	if (person.images && person.images[0]) {
        	    $('#' + cloneId + ' .dadoscadastrais-img')
        		.attr('src',person.images[0].url);
        	} else {
        	    $('#' + cloneId + ' .dadoscadastrais-img')
        		.attr('src','/enriquecer/assets/images/user.jpg');
        	}
        	if (person.names && person.names[0]) {
        	    $('#' + cloneId + ' .dadoscadastrais-nome')
        		.html(person.names[0].display);
        	}
          /* email bla*/
        	if (person.emails && person.emails[0]) {
        	    if (!(person.emails[0].address == 'full.email.available@business.subscription')) {
        		$('#' + cloneId + ' .dadoscadastrais-email')
        		    .html(person.emails[0].address);
        	    }else {
                console.log("email"+ person.emails[0].address);
              }
        	}
          /* telefone bla*/
          if (person.phones && person.phones[0]) {
              // verificar quando usar uma api mais barata, pois quanto é email ele mostrar tal frase mas pra telefone ainda nao vi
        	    if (!(person.phones[0].display == 'full.phone.available@business.subscription')) {
        		$('#' + cloneId + ' .dadoscadastrais-telefone')
        		    .html('<b>Tel(s):</b> ' + person.phones[0].display);
        	    }
        	}
        	if (person.addresses && person.addresses[0]) {
        	    $('#' + cloneId + ' .dadoscadastrais-address')
        		.html('<b>Endereço:</b> ' + person.addresses[0].display);
        	}
        	if (person.dob) {
        	    $('#' + cloneId + ' .dadoscadastrais-dob')
        		.html('<b>Idade:</b> ' + person.dob.display);
        	}
        	if (person.jobs && person.jobs[0]) {
        	    $('#' + cloneId + ' .dadoscadastrais-job')
        		.html('<b>Emprego:</b> ' + person.jobs[0].display);
        	}
        	if (person.educations && person.educations[0]) {
        	    $('#' + cloneId + ' .dadoscadastrais-educacao')
        		.html('<b>Educação:</b> ' + person.educations[0].display);
        	}

    }
    if (debug) console.log("valoresDeCadastro: "+ contadorLog)

    if (valoresDeCadastro.length == 1) {

            	setTimeout(function () {
            	    selecionarCadastro(0);
            	    //goNext();
            	}, 500)

    }

}

/* classe acessoria para obter parametros da url
  fonte: copiei do painel ubiplaces.
  https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js */
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


// realiza a pesquisa do ususario, com ou sem email
function pesquisa(emailUsuario) {

    var parametrosDePesquisa = ""; // esta variavel serve para exibir os parametros de pesquisa

    if (debug) console.log("ubipeople2.js funçao pesquisa( params de pesquisa)");

    // esta api é para usuarios do sistema
    if (emailUsuario)
    {
        api_pesquisa = 'https://api.ubicity.com.br/ub/pesquisa?large=true&email=' + emailUsuario;
        parametrosDePesquisa = emailUsuario;
    }

    var emailSimbUbiplaces = readCookie('useremail');
    if ( (emailSimbUbiplaces=="tamer@ubicity.com.br") ||
          (emailSimbUbiplaces=="edicarlosconsultor@gmail.com") || // edi carlos
          (emailSimbUbiplaces=="joaorobsonmartins@gmail.com") || // joao
          (emailSimbUbiplaces=="fabio@ubicity.com.br") ){

      if (debug) console.log('teste interno UbiCity');
      testeInternoSimbUbiplaces = true;
      // esta api para usuarios beta
      api_pesquisa = 'https://api.ubicity.com.br/ub/pesquisa?large=true';

      // adiciona parametros nome e telefone e cpf
      var email_pesquisa = emailUsuario;
      if (email_pesquisa)
      {
          api_pesquisa += '&email=' + email_pesquisa;
          if (!emailUsuario) // esse if é para evitar dubiplicadade do email na variavel
            parametrosDePesquisa += ", "+ email_pesquisa;
      }else{console.log("nao pegou param email");}


      var nome_pesquisa = getUrlParameter("nome");
      if (nome_pesquisa)
      {
          api_pesquisa += '&nome=' + nome_pesquisa;
          parametrosDePesquisa += ", "+ nome_pesquisa;
      }else{console.log("nao pegou param nome");}

      telefone_pesquisa = getUrlParameter("telefone");
      if (telefone_pesquisa)
      {
        api_pesquisa += '&telefone=' + telefone_pesquisa;
        parametrosDePesquisa += ", "+ telefone_pesquisa;
      }else{console.log("nao pegou telefone");}

      cpf_pesquisa = getUrlParameter("cpf");
      if (cpf_pesquisa)
      {
        api_pesquisa += '&cpf=' + cpf_pesquisa;
        parametrosDePesquisa += ", "+ cpf_pesquisa;
      }else{console.log("nao pegou cpf");}

    }
    else{
     testeInternoSimbUbiplaces = false;
     console.log('pesquisa com usuario do sistema. nao é teste beta.');
   }

     if (debug) console.log("parametro de pesquisa para a api: " + api_pesquisa );

    $('#pesquisandopor').html('Pesquisando por: ' + parametrosDePesquisa );
    document.getElementById("redes-sociais").style.display = 'block';


    	$.ajax({
                //url: 'https://api.ubicity.com.br/ub/pesquisa?nome=' + nomeUsuario + '&email=' + emailUsuario,
              url: api_pesquisa,
              type: "GET",
        	    beforeSend: function (xhr) {
                		xhr.setRequestHeader
                		("Authorization", "Bearer " + readCookie('token'));
        	    },
                cache: false,
                success: function(res) {

                  		if (res.status && (res.status == 'error')) {
                          if (debug) console.log("chamada a api pesquisa retornou error")

                  		    $('.nao-encontrado').css('display','block');
                  		    $('.loading').css('display','none');
                  		    $('.loading-maior').css('display','none');

                  		} else {

                          if (debug) console.log("ok. retornou resposta da api pesquisa chave: info");
                  		    incrementarContador();//faz o debito na conta do usuario

                  		    valoresDeCadastro = res.info;

                  		    if (res.dominio &&
                                (!(res.dominio.status) || (res.dominio.status != 'error')))
                          {
                              if (debug) console.log("pegou dados do dominio");
                              if (debug) console.log(res.dominio);
                              dominio = res.dominio;

                          }
                          else {
                              if (debug) console.log("nao pegou dados do dominio em res.dominio");
                              if (debug) console.log(res);

                          }


                  		    for (var key in res)
                          {
                        			if (res.hasOwnProperty(key) && key != 'info' && key != 'dominio')
                              {

                                      // verifica api escolaridade
                            			    var esc;
                                      if (debug) console.log("avaliando conteudo recebido da api escolaridade: res[key].escolaridade");
                                      if (debug) console.log(res[key].escolaridade);
                                      // comentei abaixo pois nao temos a api escolaridade funcionando corretamtne
                            			    // if ( !(res[key].escolaridade.status)
                                      //       || (res[key].escolaridade.status != 'error')
                                      //       || (res[key].escolaridade != '0')  ) {
                                      //       if (debug) console.log("entrou no if do escolaridade com dados ok");
                                      //   				esc = res[key].escolaridade.dados
                                      //   				    .filter(function(dado) {
                                      //   					return !(!(dado));
                                      //   				    }).map(function(dado) {
                                      //   					dado.source_name = key;
                                      //   					return dado;
                                      //   				    });
                                      //
                            			    // } else {
                                      //     if (debug) console.log("entrou no else do escolaridade");
                            				  //           esc = [];
                                      //
                            			    // }
                                      esc = [];


                                      // obtem o resultado de renda para quantidade = 1
                            			    if ((!(res[key].renda.status)	 || (res[key].renda.status != 'error'))
                            				      && res[key].renda.qt == 1) {
                                              if (debug) console.log("renda results tem qt =1 ");
                                      				res[key].renda.results = [res[key].renda.results];
                                              rendaResults = res[key].renda.results;
                                              console.log("rendaResults");
                                              console.log(rendaResults);

                            			    }


                                      // verifica api transparencia
                            			    var trans;
                                      if (debug) console.log("renda status: ");
                                      if (debug) console.log(res[key].renda.status);
                                      if (debug) console.log("renda results: ");
                                      if (debug) console.log(res[key].renda.results);
                            			    if (!(res[key].renda.status)	|| (res[key].renda.status != 'error')) {
                                  				trans = res[key].renda.results
                                                				    .filter(function(dado) {
                                                					           return !(!(dado));

                                                				    }).map(function(dado) {
                                                    					dado.source_name = key;
                                                              console.log("blablebli>" + dado.source_name);
                                                              dado.source_name = dado.source_name.replace('-',' '); // tamer
                                                              console.log("laleli>" + dado.source_name);
                                                    					return dado;
                                                				    });

                                            trans = [];

                            			    } else {
                                            if (debug) console.log("erro na obtencao de renda");
                            				         trans = [];

                            			    }


                            			    valoresDeEscolaridade
                            				= valoresDeEscolaridade.concat(esc);

                            			    valoresDeTransparencia
                            				= valoresDeTransparencia.concat(trans);

                        			}
                  		    }

                  		    $('.loading').css('display','none');
                  		    $('.loading-maior').css('display','none');

                  		    renderCadastro();

                  		    if (debug) console.log(res);
                  		    if (debug) console.log(valoresDeCadastro);
                  		    if (debug) console.log(valoresDeEscolaridade);
                  		    if (debug) console.log(valoresDeTransparencia);

                  		}
                              },

                error: function() {

                  		$('.nao-encontrado').css('display','block');
                  		$('.loading').css('display','none');
                  		$('.loading-maior').css('display','none');

                }

    	});//ajax


}




/*
**********************************************
atencoa: Esta funcao esta imcompleta

grava o perfil que o cliente montou.
associa com o usuario que criou
inicialmente associar todo cliente montado por tamer ou fabio  para o usuario tamer ou fabio, para satisfazer o mvp do UbiPlaces


*******************************************

*/
function gravarPerfil(perfil){



      $.ajax({
                url: "https://api.ubicity.com.br/ub/pesquisa", //api_gravar_perfil, //qualquer url protegida
                type: 'POST',
                beforeSend: function (xhr) {
                  		xhr.setRequestHeader
                  		("Authorization", "Bearer " + readCookie('token'));
          	    },
                cache: false,
                data:  JSON.stringify ([perfil]) //JSON.stringify ([_id])// envia um array de ids se necessario  ISTO FOI UM DESAFIO!!
                ,
                success: function (response) { //if (debug) console.log("usuario autenticado");

                          if (debug) console.log("resultado gravar perfil :");
                          if (debug) console.log(response);

                },
                error: function (response)
                      {
                          if (debug) console.log("deu erro. avaliar se erro é de token expirado");if (debug) console.log(response);
                          if (debug) console.log("e o erro é : "+ response.status);  // 401 statusText:"Unauthorized"
                          // if (debug) console.log(">>>>");
                          // if (debug) console.log(this.type);   O THIS mostra os componentes de parametros do jquery: data, stype, url, etc
                          // if (debug) console.log(_id);
                          // if (debug) console.log("<<<<");


                      }
            });



} // fim do metodo


/* descrever esta funcao

    Realiza a pesquisa dos parametros no serviço de pesquisa

*/
setTimeout(function ()
{
    if (debug) console.log("chamou a funcao setTimeout steps que chama a funcao Pesquisa");

    var emailSimbUbiplaces = readCookie('useremail');
    if ( (emailVal && emailVal != '')  ||
          ( (emailSimbUbiplaces=="tamer@ubicity.com.br") ||
          (emailSimbUbiplaces=="edicarlosconsultor@gmail.com") || // edi carlos
          (emailSimbUbiplaces=="joaorobsonmartins@gmail.com") || // edi carlos
          (emailSimbUbiplaces=="fabio@ubicity.com.br") )
    ){
      if (emailSimbUbiplaces)
      {
        if (debug) console.log("pesquisa em teste beta com usuario: "+emailSimbUbiplaces);
        pesquisa("");
       }
      else {
        if (debug) console.log("pesquisa com parametro email: "+emailVal);
        pesquisa(emailVal);
      }

      if (debug) console.log("email inserido ok OU teste beta ok: realizando a pesquisa");





          	$("#example-basic").steps({
          	    headerTag: "h3",
          	    bodyTag: "section",
          	    transitionEffect: "slide",
          	    autoFocus: false,
          	    enableAllSteps: true,
                onStepChanging: function (e, currentIndex, newIndex) {
                          		if (newIndex > currentIndex) {
                          		    return selected[newIndex - 1];
                          		} else {
                          		    return true;
                          		}
                	    },
                	    transitionEffectSpeed: 150
          	});

    }else {
      if (debug) console.log("algo deu ruim na submisao do email para pesquisa ubipeople2.js");
    }

}, 1000);

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function parseIdade (x) {

    return x.slice(0,-10);


}

function parseData (x) {

    yyyy = x.slice(0,4);
    mm = x.slice(5,7);
    dd = x.slice(8,10);

    return dd + '/' + mm + '/' + yyyy;

}

function parseRange (x) {
    if (debug) console.log("debug data:" + x)
    if (debug) console.log(x)

    if (x)
      if (x.end)  {

                      	return parseData(x.start)
                      	    + ' - '
                      	    + parseData(x.end);

      } else {

                      	return parseData(x.start)
                      	    + ' - Atual';

      }

}

function parseSexo (x) {

    if (x == 'male') {

	return 'Masculino';

    } else {

	return 'Feminino';

    }

}

/*
    Expoe no front um incremento no uso da franquia do usuario

*/
function incrementarContador() {

    var val = $('.saldo-enriquecimento').html();
    if (val !== '') {
	var x = val.slice(22).split(' de ');
	var consumo = x[0];
	var franquia = x[1];
	if (debug) console.log(val)
	if (debug) console.log(x)
	if (debug) console.log(consumo)
	$('.saldo-enriquecimento').html('Consultas realizadas: ' + (Number(consumo) + 1) + ' de ' + franquia);
    }

}
