var usuarioTwitterSelecionado;
var usuarioTwitterSelecionadoFoto;
var usuarioGoogleSelecionado;
var usuarioGoogleSelecionadoFoto;
var usuarioFacebookSelecionado;
var usuarioFacebookSelecionadoFoto;

/* Variaveis para a tela de resultado*/

// informações pessoais

var foto;
var nome;
var cpf;
var email;
var telefone;
var cidade;
var profissao;
var biografia;
var biografiaSecundaria;
var idiomas = [];
var cidadesAnteriores = [];
var escolaridade = [];
var instituicao = [];
var academico = [];
var titulo = [];
var escolaridadeResumo;
var cidades = []
var cidadesAnterioresATTR = "";

// informações profissional

var nomeEmpresa;
var nomeFuncionario;
var cargo;
var salarioLiquido;
var tempoCargo;
var endereco;


//redes sociais

var facebook;
var twitter;
var googlePlus;
var linkedin;
var youtube;

//outras informacões

var estadoCivil;
var pensamento;

//verifica se todas as APIS retornaram, com sucesso ou não


function inicio() {
    document.getElementById("seguimento-pesquisa").style.display = "none" //oculta;
    document.getElementById("redes-sociais").style.display = "none" //oculta;
    document.getElementById("filtros").style.display = "";


    $("#filtros").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#enriquecer").click();
        }
    });

    $("#navbar-filtros").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#enriquecer2").click();
        }
    });

}

// Pesquisa efetuada na primeira tela

function pesquisa() {
    console.log("index.js  pesquisa()");
    resetGeral();
    document.getElementById("seguimento-pesquisa").style.display = 'none'; //oculta
    document.getElementById("filtros").style.display = 'none'; //oculta
    document.getElementById("nao-encontrado-g").style.display = 'none'; //oculta
    document.getElementById("nao-encontrado-f").style.display = 'none'; //oculta
    document.getElementById("nao-encontrado-t").style.display = 'none'; //oculta
    document.getElementById("interface").style.marginTop = "-5%";
    document.getElementById("navbar-filtros").style.display = '';

    // limpa campos texto que começam com ubi_
    x = $("[id^=ubi_]");
    for (var i = 0; i < x.length; i++) {
        $(x[i]).html("");
    }
    // limpa as imagens  que começam com ubi_foto
    xFoto = $("[id^=ubi_foto]");
    for (var i = 0; i < xFoto.length; i++) {
        console.log("limpando fotos dos elementos:" + i);
        $(xFoto[i]).prop("src", "");
    }
    // Limpa as divs que começam com resultado
    k = $("[id^=resultado]");
    for (var i = 0; i < k.length; i++) {
        $(k[i]).remove();
    }

    //Pega os campos informados pelo usuário na tela inicial
    cpf = document.getElementById("cpf").value;
    telefone = document.getElementById("telefone").value;
    email = document.getElementById("email").value;
    nome = document.getElementById("nome").value;

    //Seta os valores pegados na primeira tela, para navbar contendo os filtros
    document.getElementById("nome2").value = nome;
    document.getElementById("email2").value = email;
    document.getElementById("telefone2").value = telefone;
    document.getElementById("cpf2").value = cpf;

    //Inicia o loading page
    NProgress.start();

    //seta as mensagens de procura quando usuário inicia o enriquecimento
    $("#msg-status").html("Procurando redes sociais");


    document.getElementById("redes-sociais").style.display = 'none'; //oculta
    document.getElementById("pesquisando").style.display = ''; //mostra

    $("#nomeParaPesquisar").html(nome);

    function salve() {
        setTimeout(function () {
            $("#msg-status").html("Procurando trabalho e rendimento");
        }, 800);

    }

    setTimeout(function () {
        NProgress.set(0.4);
        $("#msg-status").html("Procurando escolaridade");
        salve();
    }, 800);

    // enriquecimento inicia aqui

    // API responsável pelas informações profissionais do potencial cliente
    urlTransparencia = "https://dev.api.ubicity.com.br/es/legado/transparencia/scraping?name=" + nome;
    $.get(urlTransparencia, function (data, status) {
        for (var i = 0; i <= data.qt - 1; i++) {

            var destinoTransparencia = "resultado-transparencia" + i;
            var destinoHashTransparencia = "#" + destinoTransparencia;

            $("#transparencia").append(
                $("#box-transparencia").clone(false).prop("id", destinoTransparencia).after("#box-transparencia")
            );
            $(destinoHashTransparencia).find('#transparencia-username').html("");
            $(destinoHashTransparencia).find('#transparencia-cargo').html("");
            $(destinoHashTransparencia).find('#transparencia-empresa').html("");

            $(destinoHashTransparencia).find('#transparencia-selecionado').prop('id', "transparencia-selecionado" + i);


            $(destinoHashTransparencia).find('#transparencia-username').append(data.results[i].name);
            $(destinoHashTransparencia).find('#transparencia-cargo').append(data.results[i].basic_data);
            $(destinoHashTransparencia).find('#transparencia-empresa').append(data.results[i].entity);

            $(destinoHashTransparencia).find('#transparencia-selecionado' + i).attr('name', data.results[i].name);
            $(destinoHashTransparencia).find('#transparencia-selecionado' + i).prop('value', data.results[i].basic_data);
            $(destinoHashTransparencia).find('#transparencia-selecionado' + i).prop('transparenciaEmp', data.results[i].entity);


        }

        $(document).on('click', '.transparenciaCheck', function () {
            if ($(this).is(':checked')) {
                $('.transparenciaCheck').on('change', function () {
                    $('.transparenciaCheck').not(this).prop('checked', false);
                });
                nomeFuncionario = document.getElementById(this.id).name;
                cargo = document.getElementById(this.id).value;
                nomeEmpresa = document.getElementById(this.id).transparenciaEmp;

                console.log(nomeFuncionario, cargo);
                console.log(nomeEmpresa);

            } else {
                nomeFuncionario = "";
                cargo = "";
                nomeEmpresa = "";
            }
        });

    }).fail(function () {
        document.getElementById("nao-encontrado-s").style.display = '';
    });


// https://dev.api.ubicity.com.br/es/legado/escolaridade?name=Fabio+Mesquita+Buiati (edited)
    urlEscolaridade = "https://dev.api.ubicity.com.br/es/escolaridade?nome=" + nome;
    $.get(urlEscolaridade, function (data, status) {
        idiomas = [];
        instituicao = [];
        for (var i = 0; i < data.educacao.idiomas.length; i++) {
            idiomas[i] = data.educacao.idiomas[i].idioma;
        }

        for (var k = 0; k < data.educacao.academico.length; k++) {
            academico[k] = data.educacao.academico[k].instituicao;
            titulo[k] = data.educacao.academico[k].titulo;
        }

        console.log(academico);
        console.log(titulo);

    }).fail(function () {

    });
    //API que retorna os perfils do facebook
    ubi_URLFacebook = "https://dev.api.ubicity.com.br/es/legado/redes-sociais/facebook?name=" + nome;
    $.get(ubi_URLFacebook, function (data, status) {

        for (i = 0; i <= data.data.length - 1; i++) {

            var destino = "resultado-facebook" + i;
            var destinoHash = "#" + destino;

            $("#facebook").append(
                $("#box-facebook").clone(false).prop('id', destino).after("#box-facebook")
            );

            //Pega o link do perfil
            perfilFacebook = data.data[i].link;

            $(destinoHash).find('#perfil-facebook').prop('href', perfilFacebook);
            $(destinoHash).css("display", '');

            //Limpa os campos html de nome
            $(destinoHash).find('#facebook-username').html("");


            $(destinoHash).find('#facebook-selecionado').prop('id', "facebook-selecionado" + i);
            $(destinoHash).find('#facebook-selecionado' + i).prop('value', perfilFacebook);
            $(destinoHash).find('#facebook-selecionado' + i).prop('name', data.data[i].picture.data.url);
            $(destinoHash).find('#facebook-username').append(data.data[i].name);
            $(destinoHash).find('#facebook-user-photo').prop('src', data.data[i].picture.data.url);

        }

        $(document).on('click', 'facebookCheck', function () {
            if ($(this).is(':checked')) {
                $('.facebookCheck').on('change', function () {
                    $('.facebookCheck').not(this).prop('checked', false);
                });
                usuarioFacebookSelecionado = document.getElementById(this.id).value;
                usuarioFacebookSelecionadoFoto = document.getElementById(this.id).name;
            } else {
                usuarioFacebookSelecionado = "";
                usuarioFacebookSelecionadoFoto = "";
            }
        });


    }).fail(function () {
        document.getElementById("nao-encontrado-f").style.display = '';
        document.getElementById("pesquisando").style.display = 'none'; //oculta
        document.getElementById("redes-sociais").style.display = ''; //mostra
    });


    ubi_URLGooglePlus = "https://dev.api.ubicity.com.br/es/legado/redes-sociais/google-plus?name=" + nome;
    $.get(ubi_URLGooglePlus, function (data, status) {
        NProgress.done();
        document.getElementById("pesquisando").style.display = 'none'; //oculta
        document.getElementById("redes-sociais").style.display = ''; //mostra
        cidadesAnteriores = [];
        if (data.status == "error") {

            // alert("moio");
            document.getElementById("nao-encontrado-g").style.display = '';
            document.getElementById("resultadoGoogle0").style.display = '';
        }
        for (i = 0; i <= data.data.length - 1; i++) {

            var destinoGoogle = "resultadoGoogle" + i;
            var destinoHashGoogle = "#" + destinoGoogle;


            $("#google-plus").append(
                $("#box-google-plus").clone(false).prop('id', destinoGoogle).after("#box-google-plus")
            );
            $(destinoHashGoogle).css("display", '');

            $(destinoHashGoogle).find('#google-plus-username').html("");
            $(destinoHashGoogle).find('#google-plus-localizacao').html("");

            $(destinoHashGoogle).find('#google-selecionado').prop('id', "google-selecionado" + i);

            $(destinoHashGoogle).find('#google-selecionado' + i).prop('linkGplus', data.data[i].url);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('fotoGplus', data.data[i].image.url);
            if (data.data[i].hasOwnProperty('placesLived')) {
                $(destinoHashGoogle).find('#google-selecionado' + i).prop('localGplus', data.data[i].placesLived[0].value);
                for (var k = 0; k <= data.data[i].placesLived.length - 1; k++) {
                    console.log(data.data[i].placesLived[k].value);
                    cidadesAnteriores[k] = data.data[i].placesLived[k].value;
                }
            }

            for (var j = 0; j <= cidadesAnteriores.length - 1; j++) {
                if (j == cidadesAnteriores.length - 1) {
                    cidadesAnterioresATTR += cidadesAnteriores[j];
                } else {
                    cidadesAnterioresATTR += cidadesAnteriores[j] + ", ";

                }
            }

            $(destinoHashGoogle).find('#google-selecionado' + i).attr('cidadesAnteriores', cidadesAnterioresATTR);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('profissaoGplus', data.data[i].occupation);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('biografiaGplus', data.data[i].aboutMe);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('estadoCivilGplus', data.data[i].relationshipStatus);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('pensamentoGplus', data.data[i].tagline);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('nomeGplus', data.data[i].displayName);

            $(destinoHashGoogle).find('#google-plus-username').append(data.data[i].displayName);
            $(destinoHashGoogle).find('#google-plus-photo').prop('src', data.data[i].image.url.replace("sz=50", "sz=400"));
            $(destinoHashGoogle).find('#perfil-google').prop('href', data.data[i].url);

        }

        $(document).on('click', '.googleCheck', function () {
            if ($(this).is(':checked')) {
                $('.googleCheck').on('change', function () {
                    $('.googleCheck').not(this).prop('checked', false);
                });
                usuarioGoogleSelecionado = document.getElementById(this.id).linkGplus;
                usuarioGoogleSelecionadoFoto = document.getElementById(this.id).fotoGplus;
                usuarioGoogleSelecionadoLocalizacao = document.getElementById(this.id).localGplus;
                foto = document.getElementById(this.id).fotoGplus;
                profissao = document.getElementById(this.id).profissaoGplus;
                biografia = document.getElementById(this.id).biografiaGplus;
                estadoCivil = document.getElementById(this.id).estadoCivilGplus;
                pensamento = document.getElementById(this.id).pensamentoGplus;
                cidade = document.getElementById(this.id).localGplus;
                nome = document.getElementById(this.id).nomeGplus;
                cidades = document.getElementById(this.id).cidadesAnteriores;
                console.log(JSON.stringify(cidadesAnterioresATTR));
            } else {
                usuarioGoogleSelecionado = "";
                usuarioGoogleSelecionadoFoto = "";
                usuarioGoogleSelecionadoLocalizacao = "";
            }
        });

    });

    //API que retorna os perfils do Twitter
    ubi_URLTwitter = "https://dev.api.ubicity.com.br/es/legado/redes-sociais/twitter?name=" + nome;
    $.get(ubi_URLTwitter, function (data, status) {
        for (i = 0; i <= data.data.length - 1; i++) {

            var destino = "resultadoTwitter" + i;
            var destinoHash = "#" + destino;

            $("#twitter").append(
                $("#box-twitter").clone(false).prop('id', destino).after("#box-twitter")
            );

            usuarioTwitterSelecionado = "http://www.twitter.com.br/" + data.data[i].screen_name;
            $(destinoHash).find('#twitter-perfil').prop('href', usuarioTwitterSelecionado);
            $(destinoHash).css("display", '');

            $(destinoHash).find('#twitter-username').html("");
            $(destinoHash).find('#twitter-localizacao').html("");
            $(destinoHash).find('#twitter-selecionado').prop('id', "twitter-selecionado" + i);

            $(destinoHash).find('#twitter-selecionado' + i).prop('value', usuarioTwitterSelecionado);
            $(destinoHash).find('#twitter-selecionado' + i).prop('name', data.data[i].profile_image_url);
            $(destinoHash).find('#twitter-selecionado' + i).prop('nomeTwitter', data.data[i].name);
            $(destinoHash).find('#twitter-selectionado' + i).prop('biografiaTwitter', data.data[i].description);
            $(destinoHash).find('#twitter-username').append(data.data[i].name);
            $(destinoHash).find('#twitter-localizacao').append(data.data[i].location);
            $(destinoHash).find('#twitterUserPhoto').prop('src', data.data[i].profile_image_url.replace("_normal", "_400x400"));

        }

        $(document).on('click', '.twitterCheck', function () {
            if ($(this).is(':checked')) {
                $('.twitterCheck').on('change', function () {
                    $('.twitterCheck').not(this).prop('checked', false);
                });
                usuarioTwitterSelecionado = document.getElementById(this.id).value;
                usuarioTwitterSelecionadoFoto = document.getElementById(this.id).name;
                biografiaSecundaria = document.getElementById(this.id).biografiaTwitter;
                console.log(biografiaSecundaria);

                // nome = document.getElementById(this.id).nomeTwitter;
            } else {
                usuarioTwitterSelecionado = "";
                usuarioTwitterSelecionadoFoto = "";
            }
        });
    }).fail(function () {
        document.getElementById("nao-encontrado-t").style.display = '';
    });


} // fim da funcao pesquisa nome


function pesquisa2() {
    // limpar();
    document.getElementById("seguimento-pesquisa").style.display = 'none'; //oculta
    document.getElementById("filtros").style.display = 'none';
    document.getElementById("navbar-filtros").style.display = '';
    document.getElementById("interface").style.marginTop = "-6%";
    document.getElementById("nao-encontrado-g").style.display = 'none';
    document.getElementById("nao-encontrado-f").style.display = 'none';
    document.getElementById("nao-encontrado-t").style.display = 'none';

    // limpa campos texto que começam com ubi_
    x = $("[id^=ubi_]");
    for (var i = 0; i < x.length; i++) {
        // console.log("limpando elementos:" + i);
        //$(x[i]).css("display","none");
        $(x[i]).html("");

    }
    // limpa as imagens  que começam com ubi_foto
    xFoto = $("[id^=ubi_foto]");
    for (var i = 0; i < xFoto.length; i++) {
        // console.log("limpando fotos dos elementos:" + i);

        //  $(x[i]).html("");
        $(xFoto[i]).prop("src", "");
    }

    k = $("[id^=resultado]");
    for (var i = 0; i < k.length; i++) {
        $(k[i]).remove();
    }


    // pega o primeiro nome e imagem do usuario
    atualizaNome = true;
    atualizaImagem = true;
    cpf = document.getElementById("cpf2").value;
    telefone = document.getElementById("telefone2").value;
    email = document.getElementById("email2").value;
    nome = document.getElementById("nome2").value;
    if ((typeof nome != "undefined") && (nome.length == 0)) {
        // este é um nome de um usuário padrão para testar o sistema quando nenhum nome for digitado
        nome = "AARON JONATHAN EDWARDS";
        console.log("pegou nome padrao :/");
    }
    NProgress.start();
    $("#msg-status").html("Procurando redes sociais");


    document.getElementById("redes-sociais").style.display = 'none';
    document.getElementById("pesquisando").style.display = ''; //mostra

    $("#nomeParaPesquisar").html(nome);

    function salve() {
        setTimeout(function () {
            $("#msg-status").html("Procurando trabalho e rendimento");
        }, 800);

    }

    setTimeout(function () {
        NProgress.set(0.4);
        $("#msg-status").html("Procurando escolaridade");
        salve();
    }, 800);


    // uaitie


    // enriquecimento inicia aqui

    // API responsável pelas informações profissionais do potencial cliente
    urlTransparencia = "https://dev.api.ubicity.com.br/es/legado/transparencia/scraping?name=" + nome;
    $.get(urlTransparencia, function (data, status) {
        // verifica status de hhtp
        // verifica status do retorno
        console.log(data);
        console.log(data.qt);
        console.log(data.results[0].name);
        NProgress.done();

        document.getElementById("pesquisando").style.display = 'none'; //oculta
        document.getElementById("redes-sociais").style.display = ''; //mostra

        for (var i = 0; i <= data.qt - 1; i++) {

            var destinoTransparencia = "resultado-transparencia" + i;
            var destinoHashTransparencia = "#" + destinoTransparencia;

            $("#transparencia").append(
                $("#box-transparencia").clone(false).prop("id", destinoTransparencia).after("#box-transparencia")
            );

            $(destinoHashTransparencia).css("display", '');

            $(destinoHashTransparencia).find('#transparencia-username').html("");
            $(destinoHashTransparencia).find('#transparencia-cargo').html("");
            $(destinoHashTransparencia).find('#transparencia-empresa').html("");

            $(destinoHashTransparencia).find('#transparencia-selecionado').prop('id', "transparencia-selecionado" + i);


            $(destinoHashTransparencia).find('#transparencia-username').append(data.results[i].name);
            $(destinoHashTransparencia).find('#transparencia-cargo').append(data.results[i].basic_data);
            $(destinoHashTransparencia).find('#transparencia-empresa').append(data.results[i].entity);

            $(destinoHashTransparencia).find('#transparencia-selecionado' + i).attr('name', data.results[i].name);
            $(destinoHashTransparencia).find('#transparencia-selecionado' + i).prop('value', data.results[i].basic_data);
            $(destinoHashTransparencia).find('#transparencia-selecionado' + i).prop('transparenciaEmp', data.results[i].entity);


        }

        $(document).on('click', '.transparenciaCheck', function () {
            if ($(this).is(':checked')) {
                $('.transparenciaCheck').on('change', function () {
                    $('.transparenciaCheck').not(this).prop('checked', false);
                });
                nomeFuncionario = document.getElementById(this.id).name;
                cargo = document.getElementById(this.id).value;
                nomeEmpresa = document.getElementById(this.id).transparenciaEmp;

                console.log(nomeFuncionario, cargo);
                console.log(nomeEmpresa);

            } else {
                nomeFuncionario = null;
                cargo = null;
                nomeEmpresa = null;
            }
        });

    }).fail(function () {
        alert(moio);
        document.getElementById("nao-encontrado-s").style.display = '';
        NProgress.done();
        document.getElementById("redes-sociais").style.display = ''; //mostra
        document.getElementById("pesquisando").style.display = 'none'; //
    });

    // GET para API de escolaridade

    urlEscolaridadeDesc = "https://dev.api.ubicity.com.br/es/legado/escolaridade?name=" + nome;

    $.get(urlEscolaridadeDesc, function (data, status) {
        descricao = "";
        descricao = data.result.description;
    });


// https://dev.api.ubicity.com.br/es/legado/escolaridade?name=Fabio+Mesquita+Buiati (edited)
    urlEscolaridade = "https://dev.api.ubicity.com.br/es/escolaridade?nome=" + nome;
    $.get(urlEscolaridade, function (data, status) {
        idiomas = [];
        instituicao = [];
        for (var i = 0; i < data.educacao.idiomas.length; i++) {
            idiomas[i] = data.educacao.idiomas[i].idioma;
        }

        for (var k = 0; k < data.educacao.academico.length; k++) {
            academico[k] = data.educacao.academico[k].instituicao;
            titulo[k] = data.educacao.academico[k].titulo;
        }

        console.log(academico);
        console.log(titulo);

    });


    //API que retorna os perfils do facebook
    ubi_URLFacebook = "https://dev.api.ubicity.com.br/es/legado/redes-sociais/facebook?name=" + nome;
    $.get(ubi_URLFacebook, function (data, status) {
        NProgress.done();

        document.getElementById("pesquisando").style.display = 'none'; //oculta
        document.getElementById("redes-sociais").style.display = ''; //mostra

        for (i = 0; i <= data.data.length - 1; i++) {

            var destino = "resultado-facebook" + i;
            var destinoHash = "#" + destino;

            $("#facebook").append(
                $("#box-facebook").clone(false).prop('id', destino).after("#box-facebook")
            );

            //Pega o link do perfil
            perfilFacebook = data.data[i].link;

            $(destinoHash).find('#perfil-facebook').prop('href', perfilFacebook);
            $(destinoHash).css("display", '');

            //Limpa os campos html de nome
            $(destinoHash).find('#facebook-username').html("");


            $(destinoHash).find('#facebook-selecionado').prop('id', "facebook-selecionado" + i);
            $(destinoHash).find('#facebook-selecionado' + i).prop('value', perfilFacebook);
            $(destinoHash).find('#facebook-selecionado' + i).prop('name', data.data[i].picture.data.url);
            $(destinoHash).find('#facebook-username').append(data.data[i].name);
            $(destinoHash).find('#facebook-user-photo').prop('src', data.data[i].picture.data.url);

        }

        $(document).on('click', '.facebookCheck', function () {
            if ($(this).is(':checked')) {
                $('.facebookCheck').on('change', function () {
                    $('.facebookCheck').not(this).prop('checked', false);
                });
                usuarioFacebookSelecionado = document.getElementById(this.id).value;
                usuarioFacebookSelecionadoFoto = document.getElementById(this.id).name;
            } else {
                usuarioFacebookSelecionado = "";
                usuarioFacebookSelecionadoFoto = "";
            }
        });


    }).fail(function () {
        document.getElementById("nao-encontrado-f").style.display = '';
        NProgress.done();
        document.getElementById("redes-sociais").style.display = ''; //mostra
        document.getElementById("pesquisando").style.display = 'none'; //
    });
    ubi_URLGooglePlus = "https://dev.api.ubicity.com.br/es/legado/redes-sociais/google-plus?name=" + nome;
    $.get(ubi_URLGooglePlus, function (data, status) {
        cidadesAnteriores = [];
        if (data.status == "error") {
            document.getElementById("nao-encontrado-g").style.display = '';
            document.getElementById("resultadoGoogle0").style.display = '';

        }
        for (i = 0; i <= data.data.length - 1; i++) {

            var destinoGoogle = "resultadoGoogle" + i;
            var destinoHashGoogle = "#" + destinoGoogle;


            $("#google-plus").append(
                $("#box-google-plus").clone(false).prop('id', destinoGoogle).after("#box-google-plus")
            );
            $(destinoHashGoogle).css("display", '');

            $(destinoHashGoogle).find('#google-plus-username').html("");
            $(destinoHashGoogle).find('#google-plus-localizacao').html("");

            $(destinoHashGoogle).find('#google-selecionado').prop('id', "google-selecionado" + i);

            $(destinoHashGoogle).find('#google-selecionado' + i).prop('linkGplus', data.data[i].url);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('fotoGplus', data.data[i].image.url);
            if (data.data[i].hasOwnProperty('placesLived')) {
                $(destinoHashGoogle).find('#google-selecionado' + i).prop('localGplus', data.data[i].placesLived[0].value);
                for (var k = 0; k <= data.data[i].placesLived.length - 1; k++) {
                    console.log(data.data[i].placesLived[k].value);
                    cidadesAnteriores[k] = data.data[i].placesLived[k].value;
                }
            }

            for (var j = 0; j <= cidadesAnteriores.length - 1; j++) {
                if (j == cidadesAnteriores.length - 1) {
                    cidadesAnterioresATTR += cidadesAnteriores[j];
                } else {
                    cidadesAnterioresATTR += cidadesAnteriores[j] + ", ";

                }
            }

            $(destinoHashGoogle).find('#google-selecionado' + i).attr('cidadesAnteriores', cidadesAnterioresATTR);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('profissaoGplus', data.data[i].occupation);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('biografiaGplus', data.data[i].aboutMe);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('estadoCivilGplus', data.data[i].relationshipStatus);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('pensamentoGplus', data.data[i].tagline);
            $(destinoHashGoogle).find('#google-selecionado' + i).prop('nomeGplus', data.data[i].displayName);

            $(destinoHashGoogle).find('#google-plus-username').append(data.data[i].displayName);
            $(destinoHashGoogle).find('#google-plus-photo').prop('src', data.data[i].image.url.replace("sz=50", "sz=400"));
            $(destinoHashGoogle).find('#perfil-google').prop('href', data.data[i].url);

        }

        $(document).on('click', '.googleCheck', function () {
            if ($(this).is(':checked')) {
                $('.googleCheck').on('change', function () {
                    $('.googleCheck').not(this).prop('checked', false);
                });
                usuarioGoogleSelecionado = document.getElementById(this.id).linkGplus;
                usuarioGoogleSelecionadoFoto = document.getElementById(this.id).fotoGplus;
                usuarioGoogleSelecionadoLocalizacao = document.getElementById(this.id).localGplus;
                foto = document.getElementById(this.id).fotoGplus;
                profissao = document.getElementById(this.id).profissaoGplus;
                biografia = document.getElementById(this.id).biografiaGplus;
                estadoCivil = document.getElementById(this.id).estadoCivilGplus;
                pensamento = document.getElementById(this.id).pensamentoGplus;
                cidade = document.getElementById(this.id).localGplus;
                nome = document.getElementById(this.id).nomeGplus;
                cidades = document.getElementById(this.id).cidadesAnteriores;
                console.log(JSON.stringify(cidadesAnterioresATTR));
            } else {
                usuarioGoogleSelecionado = "";
                usuarioGoogleSelecionadoFoto = "";
                usuarioGoogleSelecionadoLocalizacao = "";
            }
        });

    });


    //API que retorna os perfils do Twitter
    ubi_URLTwitter = "https://dev.api.ubicity.com.br/es/legado/redes-sociais/twitter?name=" + nome;
    $.get(ubi_URLTwitter, function (data, status) {
        NProgress.done();
        document.getElementById("redes-sociais").style.display = ''; //mostra
        document.getElementById("pesquisando").style.display = 'none'; //mostra

        for (i = 0; i <= data.data.length - 1; i++) {

            var destino = "resultadoTwitter" + i;
            var destinoHash = "#" + destino;

            $("#twitter").append(
                $("#box-twitter").clone(false).prop('id', destino).after("#box-twitter")
            );

            usuarioTwitterSelecionado = "http://www.twitter.com.br/" + data.data[i].screen_name;
            $(destinoHash).find('#twitter-perfil').prop('href', usuarioTwitterSelecionado);
            $(destinoHash).css("display", '');

            $(destinoHash).find('#twitter-username').html("");
            $(destinoHash).find('#twitter-localizacao').html("");
            $(destinoHash).find('#twitter-selecionado').prop('id', "twitter-selecionado" + i);

            $(destinoHash).find('#twitter-selecionado' + i).prop('value', usuarioTwitterSelecionado);
            $(destinoHash).find('#twitter-selecionado' + i).prop('name', data.data[i].profile_image_url);
            $(destinoHash).find('#twitter-selecionado' + i).prop('nomeTwitter', data.data[i].name);
            $(destinoHash).find('#twitter-username').append(data.data[i].name);
            $(destinoHash).find('#twitter-localizacao').append(data.data[i].location);
            $(destinoHash).find('#twitterUserPhoto').prop('src', data.data[i].profile_image_url.replace("_normal", "_400x400"));

        }

        $(document).on('click', '.twitterCheck', function () {
            if ($(this).is(':checked')) {
                $('.twitterCheck').on('change', function () {
                    $('.twitterCheck').not(this).prop('checked', false);
                });
                usuarioTwitterSelecionado = document.getElementById(this.id).value;
                usuarioTwitterSelecionadoFoto = document.getElementById(this.id).name;
                // nome = document.getElementById(this.id).nomeTwitter;
            } else {
                usuarioTwitterSelecionado = "";
                usuarioTwitterSelecionadoFoto = "";
            }
        });
    }).fail(function () {
        document.getElementById("nao-encontrado-t").style.display = '';
        NProgress.done();
        document.getElementById("redes-sociais").style.display = ''; //mostra
        document.getElementById("pesquisando").style.display = 'none'; //mostra

    });

} // fim da funcao pesquisa nome


function voltar() {
    limpar();
    document.getElementById("seguimento-pesquisa").style.display = 'none';
    document.getElementById("redes-sociais").style.display = '';
    document.getElementById("interface").style.marginTop = "-6%";

}

function resetGeral() {
    $('#ubi_URLFacebook').html("");
    $('#ubi_URLTwitter').html("");
    $('#ubi_URLGooglePlus').html("");
    $('#ubi_fotoUsuario').prop('src', "");
    $('#ubi_cpf').html("");
    $('#ubi_telefone').html("");
    $('#ubi_email').html("");
    $('#ubi_username').html("");
    $('#ubi_biografia').html("");
    $('#ubi_biografia').html("");
    $('#ubi_cidade').html("");
    $('#ubi_profissao').html("");
    $('#ubi_estadoCivil').html("");
    $('#ubi_pensamento').html("");

// informações pessoais
    foto = null;
    nome = null;
    cpf = null;
    email = null;
    telefone = null;
    cidade = null;
    profissao = null;
    biografia = null;
    biografiaSecundaria = null;
    idiomas = null;
    cidadesAnteriores = [];
    escolaridade = null;
    instituicao = [];
    academico = [];
    titulo = [];
    escolaridadeResumo = null;
    cidades = [];
    cidadesAnterioresATTR = null;

// informações profissional
    nomeEmpresa = null;
    cargo = null;
    salarioLiquido = null;
    tempoCargo = null;
    endereco = null;

// redes sociais
    facebook = null;
    twitter = null
    googlePlus = null;
    linkedin = null;
    youtube = null;

//outras informacões
    estadoCivil = null;
    pensamento = null;
}

function getIdioma() {
    var printIdioma = "";
    for (var i = 0; i <= idiomas.length - 1; i++) {
        if (i == idiomas.length - 1) {
            printIdioma += idiomas[i];
        } else {
            printIdioma += idiomas[i] + ", ";
        }

    }
    return printIdioma;
}

function getEscolaridade() {
    var printEscolaridade = "";
    for (var i = 0; i <= titulo.length - 1; i++) {
        if (i == titulo.length - 1) {
            printEscolaridade += titulo[i] + " | " + academico[i]
        } else {
            printEscolaridade += titulo[i] + " | " + academico[i] + '<br/><br/> ';
        }
    }
    return printEscolaridade;
}


function gerarResultado() {

    document.getElementById("redes-sociais").style.display = 'none';
    document.getElementById("seguimento-pesquisa").style.display = '';
    document.getElementById("interface").style.marginTop = "-6%";

    $('#ubi_URLFacebook').html(perfilFacebook);
    $('#ubi_URLTwitter').html(usuarioTwitterSelecionado);
    $('#ubi_URLGooglePlus').html(usuarioGoogleSelecionado);
    $('#ubi_URLFacebook').prop('href', perfilFacebook);
    $('#ubi_URLTwitter').prop('href', usuarioTwitterSelecionado);
    $('#ubi_URLGooglePlus').prop('href', usuarioGoogleSelecionado);
    if (foto)
    $('#ubi_fotoUsuario').prop('src', foto.replace('sz=50', 'sz=200'));
    $('#ubi_cpf').html(cpf);
    $('#ubi_telefone').html(telefone);
    $('#ubi_email').html(email);
    $('#ubi_username').html(nome);
    if (biografia == null) {
        $('#ubi_biografia').html(biografiaSecundaria);
    } else {
        $('#ubi_biografia').html(biografia);
    }
    $('#ubi_cidade').html(cidade);
    $('#ubi_profissao').html(profissao);
    $('#ubi_estadoCivil').html(estadoCivil);
    $('#ubi_pensamento').html(pensamento);
    $('#idiomas').html(getIdioma());
    console.log(nomeEmpresa);
    console.log(cargo);
    $('#nome-empresa').html(nomeEmpresa);
    $('#cargo').html(cargo);
    $('#escolaridadeResumo').html(escolaridadeResumo);
    $('#escolaridade').html(getEscolaridade());
    $('#cidades-anteriores').html(cidadesAnterioresATTR);
    // $('#escolaridade-instituicao').html(getEscolaridade2());
    // console.log(getEscolaridade());
    // console.log(getEscolaridade2());

}
