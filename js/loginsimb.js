/* simb.js created by UbiCity
Tamer americo

*/

var debug = false;

function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    else {
      console.log("cookie nao expira.");
      expires = "; expires=Fri, 31 Dec 9999 23:59:59 GMT" ;
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

/* funcao para ler um cookie
fonte: https://stackoverflow.com/questions/10730362/get-cookie-by-name
*/
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}


/* limpa um determinado cookie*/
function eraseCookie(name) {
    createCookie(name, "", -1);
}




/* verifica se token é valido*/
function tokenValido() {

    $.ajax({
        url: "https://api.ubicity.com.br/ub/verifica",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
            debug = false; // a variavel global nao esta entrando aqui :/
            if (debug) console.log("o token do usuario é válido");
            if (debug) console.log(res);
            return true;
        },
        error: function (xhr, status, error) {
            debug = false; // a variavel global nao esta entrando aqui :/

            if (debug) console.log("token não existe ou é invalido ou expirou:");
            if (debug) console.log(xhr.status);
            if (debug) console.log(status);
            if (debug) console.log(xhr.responseText);

            eraseCookie('token');
            eraseCookie('username');
            eraseCookie('useremail');
            eraseCookie('uuid');


            window.location.href = 'index.html';

            return false;
        },
    });

}


/* faz a inicializacao do aplicativo*/


if (debug) console.log(">>> Inicializando validacao de login do dashboardSIMB ubicity.com.br 2018");
tokenValido(); // boolean
