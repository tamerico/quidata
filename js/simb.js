/* simb.js created by UbiCity
Tamer americo
LG

codigo legado apenas para consulta
*/

var debug = true;
var testeInternoSimbUbiplaces = false;


function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    else {
      console.log("cookie nao expira.");
      expires = "; expires=Fri, 31 Dec 9999 23:59:59 GMT" ;
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

/* funcao para ler um cookie
fonte: https://stackoverflow.com/questions/10730362/get-cookie-by-name
*/
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}


/* limpa um determinado cookie*/
function eraseCookie(name) {
    createCookie(name, "", -1);
}

/*  obs: como teste entao o usuário nao precisa estar logado. */
function submitTeste() {
    if (debug) console.log("realizando pesquisa submitTeste()");
    setTimeout(function () {
        var userToken = readCookie('token');
        if (!userToken || (userToken == '')) {
            $("#cadastroModal").modal('show');
        } else if (consumoPlano && (consumoPlano == franquiaPlano)) {
            $('#contratarModal').modal('show');
        } else //if ($('#name-teste').next().text() == '')
        {
            //bla
            window.location.search = 'email='
                + $('#email-teste').val();
        }
    }, 500);
    return false;
}

function submitTeste2() {
    setTimeout(function () {
        if (debug) console.log("chamou a funcao submitTeste2()");
        var userToken = readCookie('token');
        if (!userToken || (userToken == '')) {
            $("#cadastroModal").modal('show');
        } else if (consumoPlano && (consumoPlano == franquiaPlano)) {
            $('#contratarModal').modal('show');
        } else //if ($('#name-teste2').next().text() == '')
        {
            window.location.search = 'email='
                + $('#email-teste2').val();

        }
    }, 500);
    return false;
}

function submitTesteLogado() {
    if (debug) console.log("iniciando procedimento para pesquisar email... submitTesteLogado()");
    setTimeout(function () {
        if (consumoPlano == franquiaPlano) {
            $('#mensagemSemConsultas').modal();
        } else //if ($('#nome-teste-logado').next().text() == '')
        {

            // window.location faz a mudança da url no browser visualmente.
            destino = 'email=' + $('#email-teste-logado').val();
            console.log("testeInternoSimbUbiplaces:" + testeInternoSimbUbiplaces);

            if (testeInternoSimbUbiplaces) {
                console.log("entrou no if do testeInternoSimbUbiplaces");
                if ($('#nome-teste-logado').val() != '')
                    destino += '&nome=' + $('#nome-teste-logado').val();
                if ($('#telefone-teste-logado').val() != '')
                    destino += '&telefone=' + $('#telefone-teste-logado').val();
                if ($('#cpf-teste-logado').val() != '')
                    destino += '&cpf=' + $('#cpf-teste-logado').val();
            }
            if (debug) console.log("destino>" + destino);
            window.location.search = destino;

        }
    }, 500);
    return false;
}

/* funcao que envia formulario de contato atraves de endpoint do portal imobiliario*/
function contato() {

    var nome = $("input#contato-name").val();
    var email = $("input#contato-email").val();
    var assunto = $("input#contato-subject").val();
    var msg = $("textarea#contato-mensagem").val();

    var mensagem
        = "SIMB - CONTATO "
        + "Assunto: " + assunto + "\n"
        + "Msg: " + msg;

    var dados = {
        "email": email,
        "nome": nome,
        "mensagem": mensagem
    };

    if (debug) console.log(dados);

    $.ajax({
        url: "https://api.boju.com.br/us/contato/institucional",
        type: "POST",
        data: JSON.stringify(dados),
        cache: false,
        success: function (response) {
            $('#contato-success').html("<div class='alert alert-success'>");
            $('#contato-success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#contato-success > .alert-success')
                .append("<strong>Obrigado por entrar em contato. Retornaremos em até 12 horas.</strong>");
            $('#contato-success > .alert-success')
                .append('</div>');
        },
        error: function () {
            $('#contato-success').html("<div class='alert alert-danger'>");
            $('#contato-success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#contato-success > .alert-danger').append("<strong>Sorry, it seems that my mail server is not responding. Please try again later!");
            $('#contato-success > .alert-danger').append('</div>');
        },
    });

    return false;

}

function showLandingLogado() {

    window.location.href = 'dashboard.html';


}

function showLandingPage() {
    $('#landing').css('display', '');
    $('#landing-logado').css('display', 'none');
    $('#enriquecimento').css('display', 'none');
    $('.header-landing').css('display', '');
    $('.header-landing-logado').css('display', 'none');
    $('.header-enriquecimento-logado').css('display', 'none');
    $('.header-enriquecimento').css('display', 'none');
    $('#nextprev').css('display', 'none');
}

function showEnriquecimento() {
    $('#enriquecimento').css('display', '');
    $('#landing-logado').css('display', 'none');
    $('#landing').css('display', 'none');
    $('.header-enriquecimento-logado').css('display', 'none');
    $('.header-enriquecimento').css('display', '');
    $('.header-landing-logado').css('display', 'none');
    $('.header-landing').css('display', 'none');
    $('#nextprev').css('display', '');
}

function showEnriquecimentoLogado() {
    $('#enriquecimento').css('display', '');
    $('#landing-logado').css('display', 'none');
    $('#landing').css('display', 'none');
    $('.header-enriquecimento-logado').css('display', '');
    $('.header-enriquecimento').css('display', 'none');
    $('.header-landing-logado').css('display', 'none');
    $('.header-landing').css('display', 'none');

    $('.username-enriquecimento').html(readCookie('username'));
}


function contrate() {

    var nome = $(".modal input#nome").val();
    var email = $(".modal input#email").val();
    var telefone = $("input#telefone").val();
    var plano = $("select#planos option:selected").val();
    var observacoes = $("textarea#observacoes").val();

    var mensagem = "SIMB - QUERO CONTRATAR "
        + "Telefone: " + telefone + "\n"
        + "Plano: " + plano + "\n"
        + "Observações: " + observacoes + "\n";

    var dados = {
        "email": email,
        "nome": nome,
        "mensagem": mensagem
    };

    $.ajax({
        url: "https://api.boju.com.br/us/contato/institucional",
        type: "POST",
        data: JSON.stringify(dados),
        cache: false,
        success: function (response) {
            $('#success').html("<div class='alert alert-success'>");
            $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#success > .alert-success')
                .append("<strong>Obrigado por entrar em contato. Retornaremos em até 12 horas.</strong>");
            $('#success > .alert-success')
                .append('</div>');
            //clear all fields
            //$('#contactForm').trigger("reset");
        },
        error: function () {
            $('#success').html("<div class='alert alert-danger'>");
            $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#success > .alert-danger').append("<strong>Sorry, it seems that my mail server is not responding. Please try again later!");
            $('#success > .alert-danger').append('</div>');
        },
    });

    return false;

}

/* inicializa plataforma de pagamento*/
function initializeStripe() {

    var plano = ''
    var handler = StripeCheckout.configure({
        key: 'pk_live_OAFNKrnNa9c3INokPoKWPRgG',
        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
        locale: 'auto',  // aqui muda a linguagem do modal do stripe
        token: function (token) {
            token.stripeToken = token.id;
            token.plano = plano;
            token.avulso = qtd;
            $.ajax({
                url: "https://api.ubicity.com.br/ub/pagamento",
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader
                    ("Authorization", "Bearer " + readCookie('token'));
                },
                data: token,
                cache: false,
                success: function (charge) {
                    if (debug) console.log(charge);
                    $('#contratarSuccess').css('display', 'block');
                    setTimeout(function () {
                        showLandingLogado();
                        $("#contratarModal").modal('hide');
                        $('.modal-backdrop').remove();
                        $('#contratarSuccess').html('');
                    }, 1500);
                    getSaldo(function (worked) {
                    });
                    $("#contratarModal").modal('hide');
                    $('.modal-backdrop').remove();
                },
                error: function () {
                    showLandingLogado();
                    $('#contratarFail').html('');
                    $("#contratarModal").modal('hide');
                    $('.modal-backdrop').remove();
                }
            });
        }
    });

    document.getElementById('iniciante-button').addEventListener('click', function (e) {
        plano = 'Iniciante';
        handler.open({
            name: 'UbiCity Tecnologia da Informação Ltda.',
            description: 'Plano Iniciante - 50 pesquisas',
            currency: 'brl',
            amount: planos[1].valor   // ex:19900  = 199,00
        });
        e.preventDefault();
    });

    document.getElementById('empresarial-button').addEventListener('click', function (e) {
        plano = 'Empresarial';
        handler.open({
            name: 'UbiCity Tecnologia da Informação Ltda.',
            description: 'Plano Empresarial - 100 pesquisas',
            currency: 'brl',
            amount: planos[2].valor
        });
        e.preventDefault();
    });

    document.getElementById('avulso-button').addEventListener('click', function (e) {
        plano = 'Avulso';
        handler.open({
            name: 'UbiCity Tecnologia da Informação Ltda.',
            description: 'Avulso - ' + nomePlano,
            currency: 'brl',
            amount: price * 100 /*multiplico por 100 para inserir os centavos no stripe*/
        });
        e.preventDefault();
    });

    window.addEventListener('popstate', function () {
        handler.close();
    });

}

$("#quantidade").bind('keyup mouseup', changeQuantidade);

var qtd = 1;
$('#quantidade').val('1');
var price = 0;
var precoPlanoAvulso = 0;
var consumoPlano = null;
var nomePlano = '';
var planos = [];

/* muda os valores para a quantidade de avulso desejada, de acordo com o plano corrente*/
function changeQuantidade() {

    qtd = Number($('#quantidade').val());

    if (qtd) {

        price = qtd * precoPlanoAvulso;
        if (debug) console.log("preço puro:" + price)
        //$('#preco-plano').html('Por R$' + precoPlanoAvulso.toFixed(2).toString().replace('.',',') + ' cada (plano ' + (nomePlano ? nomePlano : 'Grátis') + ')');
        $('#total').html('Total: R$' + price.toFixed(2).toString().replace('.', ','));

    } else {

        //$('#preco-plano').html('Por R$' + precoPlanoAvulso.toFixed(2).toString().replace('.',',') + ' cada (plano ' + (nomePlano ? nomePlano : 'Grátis') + ')');
        $('#total').html('Total: R$' + precoPlanoAvulso.toFixed(2).toString().replace('.', ','));

    }
}

var cadastroButtonEnabled = true;

function cadastro() {

    if (cadastroButtonEnabled) {

        $('#cadastroButton').css('background-color', '#cccccc');
        cadastroButtonEnabled = false;

        var nome = $(".modal input#cadastro-nome").val();
        var email = $(".modal input#cadastro-email").val();
        var senha = $("input#cadastro-senha").val();
        var senhaConfirmacao = $("input#cadastro-senha-confirmacao").val();

        var dados = {
            "email": email,
            "nome": nome,
            "senha": senha
        };



            $.ajax({
                url: "https://api.ubicity.com.br/ub/auth/cadastro",
                type: "POST",
                data: dados,
                cache: false,
                success: function (response) {

                    createCookie('token', response.token);
                    createCookie('username', response.nome);
                    createCookie('useremail', response.email);

                    if (debug) console.log(response);

                    $('#cadastro_success').html("<div class='alert alert-success'>");
                    $('#cadastro_success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#cadastro_success > .alert-success')
                        .append("<strong>Cadastro realizado com sucesso!</strong>");
                    $('#cadastro_success > .alert-success')
                        .append('</div>');

                    setTimeout(function () {
                      // redirecionar para dashboard
                        showLandingLogado();
                        $("#cadastroModal").modal('hide');
                        $('.modal-backdrop').remove();
                        $('#cadastro_success').html('');
                        $('#cadastroButton').css('background-color', '#117a8b');
                        cadastroButtonEnabled = true;
                    }, 1500);

                },
                error: function () {

                    txt = 'Erro (email já cadastrado)';

                    $('#cadastro_success').html("<div class='alert alert-danger'>");
                    $('#cadastro_success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");

                    $('#cadastro_success > .alert-danger').append("<strong>" + txt);
                    $('#cadastro_success > .alert-danger').append('</div>');

                    setTimeout(function () {
                        $('#cadastro_success').html('');
                    }, 3500);

                    $('#cadastroButton').css('background-color', '#117a8b');
                    cadastroButtonEnabled = true;

                },
            });



    }

    return false;

}

var loginButtonEnabled = true;

/* efetua o login do usuario*/
function login() {

    if (loginButtonEnabled) {

        $('#loginButton').css('background-color', '#cccccc');
        loginButtonEnabled = false;

        var email = $(".modal input#login-email").val();
        var senha = $("input#login-senha").val();

        var dados = {
            "email": email,
            "senha": senha
        };
        log ("pegou dados para fazer login" + dados);

        $.ajax({
            url: "https://api.ubicity.com.br/ub/auth/login",
            type: "POST",
            data: dados,
            cache: false,
            success: function (response) {

                eraseCookie('token');
                eraseCookie('username');
                eraseCookie('useremail');

                createCookie('token', response.token);
                createCookie('username', response.usuario);
                createCookie('useremail', response.email);

                // Show message for success login
                $('#login_message .success').slideDown(200);

                setTimeout(function () {
                    window.location.href = 'dashboard.html';
                    showLandingLogado();
                    $("#loginModal").modal('hide');
                    // Hide success message
                    $('#login_message .success').slideUp(200);
                    $('.modal-backdrop').remove();
                    loginButtonEnabled = true;
                }, 1500);

            },
            error: function (e) {
                // Show alert message for error login
                $('#login_message .error').slideDown(200);

                setTimeout(function () {
                    // Hide error message
                    $('#login_message .error').slideUp(200);
                }, 3500);

                $('#loginButton').css('background-color', '#117a8b');
                loginButtonEnabled = true;

            },
        });

    }

    return false;

}

/* realiza a saida da plataforma de forma elegante, apagando o cookie*/
function sairLogin() {

    eraseCookie('token');
    eraseCookie('username');
    eraseCookie('useremail');
    qtd = 0;
    price = 0;
    precoPlanoAvulso = 0;
    consumoPlano = null;
    nomePlano = '';




    showLandingPage();
    window.location.href = 'index.html'
}

function monthDay(txt) {

    var x = txt.slice(0, -6).slice(5).split('-');
    return x[1] + '/' + x[0];

}


function enriquecerNovamente() {

    var userToken = readCookie('token');

    if (userToken && userToken != '') {
        showLandingLogado()
    } else {
        showLandingPage();
    }

    window.history.pushState('', '', '/');

}

function metricasSaldo(obj) {
    $('#pesquisas').text(obj.consumo_plano);
    $('#leads_enriqecidos').text(obj.consumo_plano);

    var ctx = document.getElementById("metrica_consumo");
    if (ctx) {
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["21/jan", "22/jan", "23/jan", "24/jan", "30/jan"],
                datasets: [
                    {
                        label: 'Consumo',
                        data: [10, 35, 20, 40, 5],
                        backgroundColor: [
                            'rgba(255, 99, 132, .2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)'
                        ]
                    },
                    {
                        label: 'Leads Enriquecidos',
                        data: [20, 10, 42, 24, 15],
                        backgroundColor: [
                            'rgba(255, 188, 52, .2)'
                        ],
                        borderColor: [
                            'rgb(255, 188, 52)'
                        ]
                    }
                ]
            }
        });
    }
}

/* obtem o saldo e mostra na pagina do usuario*/
function getSaldo(callback) {
    $.ajax({
        url: "https://api.ubicity.com.br/ub/saldo?email=" + readCookie('useremail'),
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
            $('.saldo').html('Consultas realizadas: ' + Number(res.consumo_plano) + ' de ' + Number(res.franquia_plano));
            if (res.plano) {
                $('.plano').html(res.plano + ' (até ' + monthDay(res.data_fim_plano) + ')')
            }
            consumoPlano = Number(res.consumo_plano);
            franquiaPlano = Number(res.franquia_plano);
            if (consumoPlano == franquiaPlano) {
                $('#mensagemSemConsultas').modal('toggle');
            }

            metricasSaldo(res);

            nomePlano = res.plano;
            if (nomePlano == 'Grátis') {
                $('.updateNow .dadoPlano').text('Plano '+ nomePlano +', válido até '+ monthDay(res.data_fim_plano));
                $('.updateNow').fadeIn(300);
            }
            precoPlanoAvulso = calcularPrecoAvulso(res.plano);
            price = precoPlanoAvulso;
            $('#plano-nome').html('Seu plano atual é o ' + (nomePlano ? nomePlano : 'plano grátis') + ' e cada contato avulso custa R$' + precoPlanoAvulso.toFixed(2).toString().replace('.', ','));
            //$('#preco-plano').html('Por R$' + precoPlanoAvulso.toFixed(2).toString().replace('.',',') + ' cada (plano ' + (nomePlano ? nomePlano : 'Grátis') + ')');
            $('#total').html('Total: R$' + precoPlanoAvulso.toFixed(2).toString().replace('.', ','));

            callback(true);
        },
        error: function (responseDoErro) {
            callback(false);
        },
    });

}


/* de alguma forma esta funcao é chamada quando o saldo expirou ou atingiu o limite.*/
function getSaldoEnriquecimento(callback) {
    $.ajax({
        url: "https://api.ubicity.com.br/ub/saldo?email=" + readCookie('useremail'),
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
            $('.saldo-enriquecimento').html('Consultas realizadas: ' + Number(res.consumo_plano) + ' de ' + Number(res.franquia_plano));
            if (res.plano) {
                $('.plano-enriquecimento').html(res.plano + ' (até ' + monthDay(res.data_fim_plano) + ')')
            }
            consumoPlano = Number(res.consumo_plano);
            franquiaPlano = Number(res.franquia_plano);
            if (consumoPlano == franquiaPlano) {
                $('#mensagemSemConsultas').css('display', 'block');
            } else {
                $('#mensagemSemConsultas').css('display', 'none');
            }
            nomePlano = res.plano;
            precoPlanoAvulso = calcularPrecoAvulso(res.plano);
            price = precoPlanoAvulso;
            $('#plano-nome').html('Seu plano atual é o: ' + (nomePlano ? nomePlano : 'plano grátis'));
            //$('#preco-plano').html('Por R$' + precoPlanoAvulso.toFixed(2).toString().replace('.',',') + ' cada (plano ' + (nomePlano ? nomePlano : 'Grátis') + ')');
            callback(true);
        },
        error: function (responseDoErro) {
            callback(false);
        },
    });

}

/* verifica se token é valido*/
function tokenValido() {
    $.ajax({
        url: "https://api.ubicity.com.br/ub/verifica",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
            if (debug) console.log("o token do usuario é válido");
            if (debug) console.log(res);
            return true;
        },
        error: function (xhr, status, error) {
            var landing = $('#landing').length;

            if (debug) console.log("usuario tem nome no token, tem token mas é invalido ou expirou:");
            if (debug) console.log(xhr.status);
            if (debug) console.log(status);
            if (debug) console.log(xhr.responseText);

            // que merda faz isso aqui :
            !landing ? window.location.href = 'index.html' : console.log("nao entendi: landing:"+landing);

            eraseCookie('token');
            eraseCookie('username');
            eraseCookie('useremail');

            return false;
        },
    });

}


function calcularPrecoAvulso(nomePlano) {

    if (nomePlano == 'Grátis') {

        return (planos[0].valor_avulso) / 100;

    } else if (nomePlano == 'Iniciante') {

        return (planos[1].valor_avulso) / 100;

    } else if (nomePlano == 'Empresarial') {

        return (planos[2].valor_avulso) / 100;

    }

}


function obterInformacaoDosPlanos() {

    $.ajax({
        url: "https://api.ubicity.com.br/ub/planos",
        type: "GET",
        cache: false,
        success: function (res) {

            planos = res.planos;
            var gratis = res.planos[0];
            var iniciante = res.planos[1];
            var empresarial = res.planos[2];


            // configuracao do plano gratis no decorrer do site
            // 3 pesquisas gratis para provar nossa plataforma
            $('#plano_gratis_frase_pesquisar').text(gratis.franquia);
            $('#plano_gratis_frase_teste1').text(gratis.franquia);
            $('#plano_gratis_frase_teste2').text(gratis.franquia);


            // configuracao dos planos de preços na section pricing

            $('.gratis-preco').html('<sup>R$</sup>' + gratis.valor);
            $('.gratis-quantidade').html('<i class="ion-android-checkmark-circle"></i>' + gratis.franquia + ' enriquecimentos');
            $('.gratis-preco-avulso').html('<i class="ion-android-checkmark-circle"></i> Contato adicional: R$' + (gratis.valor_avulso / 100));

            $('.iniciante-preco').html('<sup>R$</sup>' + ((iniciante.valor) / 100));
            $('.iniciante-quantidade').html('<i class="ion-android-checkmark-circle"></i>' + iniciante.franquia + ' enriquecimentos');
            $('.iniciante-preco-avulso').html('<i class="ion-android-checkmark-circle"></i> Contato adicional: R$' + (iniciante.valor_avulso / 100));

            $('.empresarial-preco').html('<sup>R$</sup>' + ((empresarial.valor) / 100));
            $('.empresarial-quantidade').html('<i class="ion-android-checkmark-circle"></i>' + empresarial.franquia + ' enriquecimentos');
            $('.empresarial-preco-avulso').html('<i class="ion-android-checkmark-circle"></i> Contato adicional: R$' + (empresarial.valor_avulso / 100));

        },
        error: function (responseDoErro) {
        },
    });

}

/* faz a inicializacao do aplicativo*/
$(document).ready(function () {

    if (debug) console.log(">>> Inicializando SIMB ubicity.com.br 2018");
    // verifica token
    tokenValido();


    var url_string = window.location.href
    console.log("url_string:"+ url_string);

    var url = new URL(url_string);

    //var n = url.searchParams.get("name");
    var paramEmail = url.searchParams.get("email");

    var emailSimbUbiplaces = readCookie('useremail');


    if ((paramEmail) ||
        (  (emailSimbUbiplaces == "tamer@ubicity.com.br") ||
            (emailSimbUbiplaces == "edicarlosconsultor@gmail.com") || // edi carlos
            (emailSimbUbiplaces == "joaorobsonmartins@gmail.com") || // edi carlos
            (emailSimbUbiplaces == "fabio@ubicity.com.br") )

    ) {
        //if (debug) console.log("dentro do read com paramEmail true");
        var userToken = readCookie('token');
        if (userToken && userToken != '') {

            getSaldoEnriquecimento(function (auth) {
                if (auth)
                    showEnriquecimentoLogado();
                else
                    showEnriquecimento();
            });
        } else {
            showEnriquecimento();
        }


    } else {
        if (debug) console.log("dentro do read com paramEmail false");

        var userToken = readCookie('token');
        if (userToken && userToken != '') {
            showLandingLogado()
        } else {
            showLandingPage();
        }

    }

    $('#search_people').on('submit', function (e) {
        e.preventDefault();
        submitTesteLogado();
    });

    $('#log_out').on('click', function () {
        sairLogin();
    });

    initializeStripe();
    obterInformacaoDosPlanos();

});
