/* landingsimb.js created by UbiCity
by Tamer americo
5 de junho de 2018

*/

var debug = true;
var testeInternoSimbUbiplaces = false;




function verifica_iframe(){
        $("custom-select").change(function(){
          alert("The text has been changed.");
        });
}

/* funcao que envia formulario de contato atraves de endpoint do portal imobiliario*/
function contato() {

    var nome = $("input#contato-name").val();
    var email = $("input#contato-email").val();
    var telefone = $("input#contato-telefone").val();
    var assunto = $("input#contato-subject").val();
    var ramo = $("#contato-ramo option:selected").val();
    var mensagem = $("textarea#contato-mensagem").val();

    if (ramo == 'Outros'){
      ramo = $("input#contato-ramo2").val();
    }


    var dados = {
        "fonte": "Landing Quidata",
        "email": email,
        "telefone": telefone,
        "nome": nome,
        "assunto": assunto,
        "ramo": ramo,
        "mensagem": mensagem
    };

    $.ajax({
        url: "https://api.boju.com.br/us/contato/institucional/quidata",
        type: "POST",
        data: JSON.stringify(dados),
        cache: false,
        success: function (response) {
            console.log(response)
          window.location.href = 'obrigado.html'
        },
        error: function (response) {
          console.log(response);
        },
    });

    return false;

}

/*
 envia email de interesse em contratar o simb
 Funcao nao esta mais em uso.
*/
function contrate() {

    var nome = $(".modal input#nome").val();
    var email = $(".modal input#email").val();
    var telefone = $("input#telefone").val();
    var plano = $("select#planos option:selected").val();
    var observacoes = $("textarea#observacoes").val();

    var mensagem = "SIMB - QUERO CONTRATAR "
        + "Telefone: " + telefone + "\n"
        + "Plano: " + plano + "\n"
        + "Observações: " + observacoes + "\n";

    var dados = {
        "email": email,
        "nome": nome,
        "mensagem": mensagem
    };

    $.ajax({
        url: "https://api.boju.com.br/us/contato/institucional/quidata",
        type: "POST",
        data: JSON.stringify(dados),
        cache: false,
        success: function (response) {
            $('#success').html("<div class='alert alert-success'>");
            $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#success > .alert-success')
                .append("<strong>Obrigado por entrar em contato. Retornaremos em até 12 horas.</strong>");
            $('#success > .alert-success')
                .append('</div>');
            //clear all fields
            //$('#contactForm').trigger("reset");
        },
        error: function () {
            $('#success').html("<div class='alert alert-danger'>");
            $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                .append("</button>");
            $('#success > .alert-danger').append("<strong>Sorry, it seems that my mail server is not responding. Please try again later!");
            $('#success > .alert-danger').append('</div>');
        },
    });

    return false;

}




/*  obs: nao permitimos testes sem cadastro. */
function submitTeste() {

    setTimeout(function () {

            $("#cadastroModal").modal('show');

    }, 500);
    return false;
}


function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    else {
      console.log("cookie nao expira.");
      expires = "; expires=Fri, 31 Dec 9999 23:59:59 GMT" ;
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

/* limpa um determinado cookie*/
function eraseCookie(name) {
    createCookie(name, "", -1);
}




var qtd = 1;
$('#quantidade').val('1');
var price = 0;
var precoPlanoAvulso = 0;
var consumoPlano = null;
var nomePlano = '';
var planos = [];



/*
  Realiza o cadastro do usuario
*/
var cadastroButtonEnabled = true;
function cadastro() {

    if (cadastroButtonEnabled) {

        $('#cadastroButton').css('background-color', '#cccccc');
        cadastroButtonEnabled = false;

        var nome = $(".modal input#cadastro-nome").val();
        var email = $(".modal input#cadastro-email").val();
        var senha = $("input#cadastro-senha").val();


        var dados = {
            "email": email,
            "nome": nome,
            "senha": senha
        };



            $.ajax({
                url: "https://api.ubicity.com.br/ub/auth/cadastro",
                type: "POST",
                data: dados,
                cache: false,
                success: function (response) {

                    createCookie('token', response.token);
                    createCookie('username', response.nome);
                    createCookie('useremail', response.email);

                    if (debug) console.log(response);

                    $('#cadastro_success').html("<div class='alert alert-success'>");
                    $('#cadastro_success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#cadastro_success > .alert-success')
                        .append("<strong>Cadastro realizado com sucesso!</strong>");
                    $('#cadastro_success > .alert-success')
                        .append('</div>');

                    setTimeout(function () {
                      // redirecionar para dashboard
                        window.location.href = 'dashboard.html';
                    }, 1500);

                },
                error: function () {

                    txt = 'Erro (Email já cadastrado)';


                    $('#cadastro_error > .alert-danger').append("<strong>" + txt);
                    $('#cadastro_error > .alert-danger').append('</div>');

                    setTimeout(function () {
                        $('#cadastro_success').html('');
                    }, 3500);

                    $('#cadastroButton').css('background-color', '#117a8b');
                    cadastroButtonEnabled = true;

                },
            });



    }

    return false;

}

/*

 efetua o login do usuario

 A pagina da landing page é a página que contem o formulario de login para o dashboard do simb

 */
var loginButtonEnabled = true;
function login() {
    if (loginButtonEnabled) {
        $('#loginButton').css('background-color', '#cccccc');
        loginButtonEnabled = false;

        var email = $(".modal input#login-email").val();
        var senha = $("input#login-senha").val();

        var dados = {
            "email": email,
            "senha": senha
        };

        $.ajax({
            url: "https://api.ubicity.com.br/ub/auth/login",
            type: "POST",
            data: dados,
            cache: false,
            success: function (response) {
              console.log(response);
                eraseCookie('token');
                eraseCookie('username');
                eraseCookie('useremail');
                eraseCookie('uuid');

                createCookie('token', response.token);
                createCookie('username', response.usuario);
                createCookie('useremail', response.email);
                createCookie('uuid', response.uuid);


                // Hide message error quidata
                $('#login-error').css('display', 'none');

                // Mensagem de sucesso no login
                $('#cadastro-success').html("<div class='alert alert-success'>");
                $('#cadastro-success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                    .append("</button>");
                $('#cadastro-success > .alert-success')
                    .append("<strong>Redirecionando para o Dashboard...</strong>");
                $('#cadastro-success > .alert-success')
                    .append('</div>');

                $('#loginButton').css('background-color', '#117a8b');
                loginButtonEnabled = true;

                setTimeout(function () {
                    window.location.href = 'dashboard.html';
                }, 1500);

            },

            error: function () {
                $('#login-error').html("<div style='display: block' class='alert alert-danger'>");
                $('#login-error > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                    .append("</button>");
                $('#login-error > .alert-danger').append("<strong>Credenciais inválidas");
                $('#login-error > .alert-danger').append('</div>');


                $('#loginButton').css('background-color', '#117a8b');
                loginButtonEnabled = true;

            },
        });

    }

    return false;

}


function obterInformacaoDosPlanos() {

    $.ajax({
        url: "https://api.ubicity.com.br/ub/planos",
        type: "GET",
        cache: false,
        success: function (res) {

            planos = res.planos;
            var gratis = res.planos[0];  // o nome das variaveis vai continuar como Grátis  pois vou fazer um teste visual com o nome Experimente durante a semana do ConectaImob
            var iniciante = res.planos[1];
            var empresarial = res.planos[2];


            // configuracao do plano gratis no decorrer do site
            // 3 pesquisas gratis para provar nossa plataforma
            $('#plano_gratis_frase_pesquisar').text(gratis.franquia);
            $('#plano_gratis_frase_teste1').text(gratis.franquia);
            $('#plano_gratis_frase_teste2').text(gratis.franquia);


            // configuracao dos planos de preços na section pricing

            $('.gratis-preco').html('<sup>R$</sup>' + ((gratis.valor) / 100 ));
            $('.gratis-quantidade').html('<i class="ion-android-checkmark-circle"></i>' + gratis.franquia + ' enriquecimento(s)');
            $('.gratis-preco-avulso').html('<i class="ion-android-checkmark-circle"></i> Contato adicional: R$' + (gratis.valor_avulso / 100));

            $('.iniciante-preco').html('<sup>R$</sup>' + ((iniciante.valor) / 100));
            $('.iniciante-quantidade').html('<i class="ion-android-checkmark-circle"></i>' + iniciante.franquia + ' enriquecimentos');
            $('.iniciante-preco-avulso').html('<i class="ion-android-checkmark-circle"></i> Contato adicional: R$' + (iniciante.valor_avulso / 100));

            $('.empresarial-preco').html('<sup>R$</sup>' + ((empresarial.valor) / 100));
            $('.empresarial-quantidade').html('<i class="ion-android-checkmark-circle"></i>' + empresarial.franquia + ' enriquecimentos');
            $('.empresarial-preco-avulso').html('<i class="ion-android-checkmark-circle"></i> Contato adicional: R$' + (empresarial.valor_avulso / 100));

        },
        error: function (responseDoErro) {
          console.log("algo deu errado. " + responseDoErro);
        }
    });

}







/*


 faz a inicializacao da landing pageo aplicativo*/
$(document).ready(function () {

    if (debug) console.log(">>> Inicializando landingSIMB ubicity.com.br 2018");

    obterInformacaoDosPlanos();


});
