$(function() {

    $("input,select").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {},
        submitSuccess: function($form, event) {

            event.preventDefault();
            var nome        = $(".modal input#nome").val();
            var email       = $(".modal input#email").val();
            var telefone    = $("input#telefone").val();
            var imobiliaria = $("input#imobiliaria").val();
            var integrador  = "nao selecionou";
            integrador = $('input[type=radio][name=integrador]:checked').val();
            var integrador_outro = $("input#integrador_outro").val();
            var url_integrador = $("input#url_integrador").val();
            //var phone = $("input#phone").val();
            var numAnuncios = $("select#numAnuncios option:selected").val();

            // se ususario selecionou outro entao verifica se ele informou qual usa
            if (integrador=="outro")
                if (integrador_outro != null)
                    integrador= integrador_outro;


	    var mensagem  = " telefone:"+ telefone +","+
                "  imobiliaria:"+ imobiliaria + ","+
                "  integrador:"+ integrador + ","+
                "  url_integrador:"+ url_integrador + ","+
                "  numAnuncios:"+ numAnuncios ;
	    
	    var dados= {
		"email": email,
		"nome": nome,
		"mensagem": mensagem
	    };

	    console.log(dados);

	    dados = JSON.stringify(dados);
	    console.log(dados)

            $.ajax({
                url: "https://api.boju.com.br/us/contato/institucional",
                type: "POST",
		data: dados,
                cache: false,
                success: function(response) {
		    console.log("mail: " + response);
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>A mensagem foi enviada. </strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    //$('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry, it seems that my mail server is not responding. Please try again later!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    //$('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
