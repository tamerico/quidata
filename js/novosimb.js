/* novosimb.js created by UbiCity
by Tamer americo
*/

var debug = false; // isto nao esta mais funcionando

function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    else {
      expires = "; expires=Fri, 31 Dec 9999 23:59:59 GMT" ;
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}



/* funcao para ler um cookie
fonte: https://stackoverflow.com/questions/10730362/get-cookie-by-name
*/
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}


/* limpa um determinado cookie*/
function eraseCookie(name) {
    createCookie(name, "", -1);
}


/* realiza a saida da plataforma de forma elegante, apagando o cookie*/
function sairLogin() {

    eraseCookie('token');
    eraseCookie('username');
    eraseCookie('useremail');
    eraseCookie('uuid');
    eraseCookie('verificado');
    qtd = 0;
    price = 0;
    precoPlanoAvulso = 0;
    consumoPlano = null;
    nomePlano = '';
    window.location.href = 'index.html'
}

function monthDay(txt) {

    var x = txt.slice(0, -6).slice(5).split('-');
    return x[1] + '/' + x[0];

}


// função que vai ativar o iframe de acordo com o usuário

function getIframeByUuid() {
    var uuid = readCookie('uuid');
    
    var IFRAME_URL_DASHBOARD = "http://quidata.com.br:3001/public/dashboard/"+uuid;
    if (uuid == 'null' || uuid == 'undefined'){
        $('#error404').text('DASHBOARD NÃO ENCONTRADO');

    }
    else{
        IFRAME_URL_DASHBOARD = IFRAME_URL_DASHBOARD.replace('http://www.quidata.com.br/api/public', 'http://www.quidata.com.br/public');
        console.log('IFRAME URL')
        console.log(IFRAME_URL_DASHBOARD)
        $('#dashboard-iframe').attr('src', IFRAME_URL_DASHBOARD);
    }
    

}

function getIframeUrl() {
    $.ajax({
        url: "https://api.ubicity.com.br/ub/get-embed",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
            var IFRAME_URL =  res['url'];
            IFRAME_URL = IFRAME_URL.replace('http://192.168.155.20', 'http://www.quidata.com.br');
            console.log(IFRAME_URL);
            $('#dashboard-iframe2').attr('src', IFRAME_URL);
        },
        error: function (responseDoErro) {
            $('#tabela-error').text('Algum erro ocorreu ao gerar a tabela completa');
       
        },
    });

    return false;
}


// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  var _MS_PER_DAY = 1000 * 60 * 60 * 24;

  // Discard the time and time-zone information.
  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}


function UFporDDD(ddd){

  estadoPorDdd =  {
    "11": "SP",
    "12": "SP",
    "13": "SP",
    "14": "SP",
    "15": "SP",
    "16": "SP",
    "17": "SP",
    "18": "SP",
    "19": "SP",
    "21": "RJ",
    "22": "RJ",
    "24": "RJ",
    "27": "ES",
    "28": "ES",
    "31": "MG",
    "32": "MG",
    "33": "MG",
    "34": "MG",
    "35": "MG",
    "37": "MG",
    "38": "MG",
    "41": "PR",
    "42": "PR",
    "43": "PR",
    "44": "PR",
    "45": "PR",
    "46": "PR",
    "47": "SC",
    "48": "SC",
    "49": "SC",
    "51": "RS",
    "53": "RS",
    "54": "RS",
    "55": "RS",
    "61": "DF",
    "62": "GO",
    "63": "TO",
    "64": "GO",
    "65": "MT",
    "66": "MT",
    "67": "MS",
    "68": "AC",
    "69": "RO",
    "71": "BA",
    "73": "BA",
    "74": "BA",
    "75": "BA",
    "77": "BA",
    "79": "SE",
    "81": "PE",
    "82": "AL",
    "83": "PB",
    "84": "RN",
    "85": "CE",
    "86": "PI",
    "87": "PE",
    "88": "CE",
    "89": "PI",
    "91": "PA",
    "92": "AM",
    "93": "PA",
    "94": "PA",
    "95": "RR",
    "96": "AP",
    "97": "AM",
    "98": "MA",
    "99": "MA"
  };

  return estadoPorDdd[ddd];

}

/*
  Funcao criada pelo Eliseu
*/
function metricasSaldo(obj) {


    var ctx = document.getElementById("metrica_consumo");
    if (ctx) {
        var myLineChart = new Chart(ctx,{
            type: 'line',
            data: {
                labels: ["21/jan", "22/jan", "23/jan", "24/jan", "30/jan"],
                datasets: [
                    {
                        label: 'Consumo',
                        data: [10, 35, 30, 40, 35],
                        backgroundColor: [
                            'rgba(255, 99, 132, .2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)'
                        ]
                    } 
                ]
            }
        });
    }
}


/* obtem o saldo e mostra na pagina do usuario*/
function getSaldo(tela) {
    $.ajax({
        url: "https://api.ubicity.com.br/ub/saldo?email=" + readCookie('useremail'),
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
            //metricasSaldo(res); // para desenhar o grafico de uso TODO

            consumo_plano = 0;

            if (res.consumo_plano)
              consumo_plano = Number(res.consumo_plano);
            if (res.franquia_plano)
              franquia_plano = Number(res.franquia_plano);

            // verifica se consumo da franquia foi atingigdo
            if (res.consumo_atingido)
              consumo_atingido = res.consumo_atingido;
            else
              consumo_atingido = false;

              // verifica se prazo de uso expirou
              if (res.prazo_atingido)
              {
                  prazo_atingido = res.prazo_atingido;
                  data_fim_plano = res.data_fim_plano;
              }
              else
                prazo_atingido = false;


            if (tela == "tela_consumo"){

              $('#saldo').html('Consultas realizadas desde: ' + monthDay(res.data_inicio_plano) + ': ' + consumo_plano + ' de ' + franquia_plano);
              if ((res.plano)&&(res.data_fim_plano)) {
                $('#plano').html('Plano ' +res.plano +' com início em: '+ monthDay(res.data_inicio_plano) + ' (válido até ' + monthDay(res.data_fim_plano) + ')')
              }

            }


            if (consumo_atingido) {
              //  alert ("Voce atingiu o limite da tua franquia.");
                sweetAlert({
                       title: ":(",
                         text: ("Você atingiu o limite da tua franquia. Você consumiu " + consumo_plano + " de " + franquia_plano) ,
                         //imageUrl: 'thumbs-up.jpg'  falta definir uma imagem e local
                         type: "warning",
                         allowOutsideClick: true
                     });

            }else if (prazo_atingido){

              sweetAlert({
                     title: ":(",
                       text: ("Você atingiu a data limite para utilizar tua franquia: "+monthDay(data_fim_plano)+ ". Insira mais crédito e restaure o uso da ferramenta. Você consumiu " + consumo_plano + " de " + franquia_plano) ,
                       //imageUrl: 'thumbs-up.jpg'  falta definir uma imagem e local
                       type: "warning",
                       allowOutsideClick: true
                   });

            }

            if (tela == "tela_consumo"){
              $('#pesquisas').text(consumo_plano);
              $('#franquia_plano').text(franquia_plano);
            }

            nomePlano = res.plano;

            if (nomePlano == 'Grátis') {
                textoPlano = 'Plano '+ nomePlano +' com início em '+ monthDay(res.data_inicio_plano) + ' e válido até '+ monthDay(res.data_fim_plano)

                if (tela == "tela_consumo"){
                  $('.updateNow .dadoPlano').text(textoPlano);
                  $('.updateNow').fadeIn(300);
                }


            }

            precoPlanoAvulso = calcularPrecoAvulso(res.plano);
            price = precoPlanoAvulso;

            if (tela == "tela_consumo"){
              $('#plano_nome').html('Seu plano atual é o ' + (nomePlano ? nomePlano : 'plano grátis') + ' e cada contato avulso custa R$' + precoPlanoAvulso.toFixed(2).toString().replace('.', ','));
              //$('#preco-plano').html('Por R$' + precoPlanoAvulso.toFixed(2).toString().replace('.',',') + ' cada (plano ' + (nomePlano ? nomePlano : 'Grátis') + ')');
              $('#total').html('Total: R$' + precoPlanoAvulso.toFixed(2).toString().replace('.', ','));
            }else
            if (tela == "tela_planos"){

              var a = new Date(res.data_inicio_plano),
                  b = new Date(res.data_fim_plano),
                  difference = dateDiffInDays(a, b);

              textoPlano = 'Plano <strong>'+ nomePlano +'</strong> com início em '+ monthDay(res.data_inicio_plano) + ' e válido até '+ monthDay(res.data_fim_plano) + '. Restam ' + difference + ' dia(s)';
              $('#plano_atual').html(textoPlano);



            }

            //callback(true);
        },
        error: function (responseDoErro) {
            //callback(false);
        },
    });

}


/* de alguma forma esta funcao é chamada quando o saldo expirou ou atingiu o limite.*/
function getSaldoEnriquecimento(callback) {
    $.ajax({
        url: "https://api.ubicity.com.br/ub/saldo?email=" + readCookie('useremail'),
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
            $('.saldo-enriquecimento').html('Consultas realizadas: ' + Number(res.consumo_plano) + ' de ' + Number(res.franquia_plano));
            if (res.plano) {
                $('.plano-enriquecimento').html(res.plano + ' (até ' + monthDay(res.data_fim_plano) + ')')
            }
            consumoPlano = Number(res.consumo_plano);
            franquiaPlano = Number(res.franquia_plano);
            if (consumoPlano == franquiaPlano) {
                $('#mensagemSemConsultas').css('display', 'block');
            } else {
                $('#mensagemSemConsultas').css('display', 'none');
            }
            nomePlano = res.plano;
            precoPlanoAvulso = calcularPrecoAvulso(res.plano);
            price = precoPlanoAvulso;
            $('#plano-nome').html('Seu plano atual é o: ' + (nomePlano ? nomePlano : 'plano grátis'));
            //$('#preco-plano').html('Por R$' + precoPlanoAvulso.toFixed(2).toString().replace('.',',') + ' cada (plano ' + (nomePlano ? nomePlano : 'Grátis') + ')');
            callback(true);
        },
        error: function (responseDoErro) {
            callback(false);
        },
    });

}

/* exibe um alerta de regerar token.*/
function gerarChave(atualizar)
{

  alert (1);

  urlParam = "";
   have = false;
  if (atualizar)
  {
    if (atualizar == true)
    {
      urlParam = "?force_update=true"
      chaveAnterior = $('#token_api_key').val();
      $('#token_api_key').val(""); // apaga o conteudo da chave
    }

  }


    $.ajax({
        url: "https://api.ubicity.com.br/ub/token"+urlParam,
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
          if (res.status)
          {
            if (res.status=='ok')
            {
              switch (res.info) {
                case "1st token generated":
                      console.log("token 1st ok . token gerado");
                      console.log(res);
                      console.log (res.token)
                      $('#token_api_key').val(res.token) ;
                      $('#botao_atualizar_token').css("display",'');
                      $('#botao_copiar_token').css("display",'');
                      $('#botao_gerar_token').css("display",'none');

                  break;

                case "token renewed":
                    console.log("token renovado ok . token gerado");
                    console.log(res);
                    console.log (res.token)
                    $('#token_api_key').val(res.token) ;
                    $('#botao_atualizar_token').css("display",'');
                    $('#botao_copiar_token').css("display",'');
                    $('#botao_gerar_token').css("display",'none');

                  break;
              }
            }else{
              console.log("nao foi possivel gerar chave ");
              $('#token_api_key').val(chaveAnterior) ;


            }
          }
        },
        error: function (responseDoErro) {
          alert ("erro na obtencao do token de api");
        },
    });
  return false;
}

/* exibe um alerta de regerar token.*/
function atualizarChave()
{


  console.log("entrou e fez atualziar ");

        /* https://stackoverflow.com/questions/46034634/sweet-alert-confirmation-before-delete*/

        swal({
        title: "Você tem certeza?",
        text: "A chave anterior será substituída por uma nova chave.",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Não, ainda não!",
        confirmButtonColor: "#009efb",
        confirmButtonText: "Sim, quero alterar a chave",
        closeOnConfirm: true,
        closeOnCancel: true,
        allowOutsideClick: true
      },
      function(isConfirm){
        if (isConfirm) {
          console.log(" aqui vamos chamar o force_update");
          gerarChave(true);

        }
      });

  return false;

}

/**/
function copiarChave()
{


  console.log("entrou e fez copiar ");
    /* Get the text field */
   var copyText = $("#token_api_key");

   /* Select the text field */
   copyText.select();

   /* Copy the text inside the text field */
   document.execCommand("copy");

   /* Alert the copied text */
   //console.log("Copied the text: " + copyText.val);
  return false;

}

/* esta funcao mostra o token de API caso exista*/
function getToken(callback) {
    $.ajax({
        url: "https://api.ubicity.com.br/ub/token",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
          if (res.status)
          {
            console.log ("token avaliando status");
            if (res.status=='ok')
            {
              console.log("token ok");
              //console.log (res.token)
              $('#token_api_key').val(res.token) ;
              $('#botao_atualizar_token').css("display",'');
              $('#botao_copiar_token').css("display",'');

            }
            else{
              console.log("token vazio. nao existe token");
              $('#botao_gerar_token').css("display",'');

            }
          }

        },
        error: function (responseDoErro) {
          alert ("erro na obtencao do token de api");
        },
    });

}


function verificaCpf(cpf, searched_key){


   if (cpf == undefined )
   {
    cpf = "";
   }
   else 
   {
     if ((searched_key!=null) && (searched_key=='cpf') || (searched_key=='email') ){
      
      cpf = "<span style='color:blue;font-weight:bold'>"+cpf+"</span>";
     }
   }
  return cpf;
}

function verificaTelefone(telefone, searched_key){
  if(telefone == undefined )
  {
    telefone = ""
  }
  else 
  {
   if (searched_key!=null && searched_key=='telefone')
      telefone = "<span style='color:blue;font-weight:bold'>"+telefone+"</span>";
  } 
  return telefone; 
}

function verificaEmail(email, emails, searched_key){
    if (!email && searched_key == 'email')
    {
       if (emails && searched_key == 'email'){
          email =  "<span style='color:blue;font-weight:bold'>"+emails[0]+"</span>";
          return email
       }
      
    }
    else if (email && searched_key == 'email'){
        if(Array.isArray(email) && email.length > 1){
            email = email[0];
            email =  "<span style='color:blue;font-weight:bold'>"+email+"</span>";
            return email
        } 
        else{
            console.log(email)
            email = email
            email =  "<span style='color:blue;font-weight:bold'>"+email+"</span>";
            return email
        } 
    }
    else if(email){
        if(Array.isArray(email) && email.length > 1 && searched_key != 'email'){
            email = email[0];
            return email
        }
        else{
            return email
        } 
    }
    else if (emails && searched_key != 'email'){
        email = emails[0]
        return email
    }
    else return " "
   
}

function criaTabela(totalUsuariosListagem){

var tamanhoTotalRegistros = totalUsuariosListagem;
var visualiza5 = Math.round (tamanhoTotalRegistros/5);
var visualiza4 = Math.round (tamanhoTotalRegistros/4);
var visualiza3 = Math.round (tamanhoTotalRegistros/3);
var visualiza2 = Math.round (tamanhoTotalRegistros/2);

$('#carteiraUsuarios').DataTable(
{

"order": [[ 0, "desc" ]] ,

"language": {
 // ou pegar todos os dados de "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
     "sEmptyTable": "Nenhum registro encontrado",
     "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
     "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
     "sInfoFiltered": "(Filtrados de _MAX_ registros)",
     "sInfoPostFix": "",
     "sInfoThousands": ".",
     "sLengthMenu": "_MENU_ resultados por página",
     "sLoadingRecords": "Carregando...",
     "sProcessing": "Processando...",
     "sZeroRecords": "Nenhum registro encontrado",
     "sSearch": "Pesquisar ",
      "searchPlaceholder": "no seu histórico...",
     "oPaginate": {
         "sNext": "Próximo",
         "sPrevious": "Anterior",
         "sFirst": "Primeiro",
         "sLast": "Último"
     },
     "oAria": {
         "sSortAscending": ": Ordenar colunas de forma ascendente",
         "sSortDescending": ": Ordenar colunas de forma descendente"
     }
}, //fonte https://datatables.net/plug-ins/i18n/Portuguese-Brasil
"aLengthMenu": [
                   [visualiza5, visualiza4, visualiza3, visualiza2, tamanhoTotalRegistros], [visualiza5, visualiza4, visualiza3, visualiza2, "Todos"]
               ],
"pageLength": tamanhoTotalRegistros //ou "All"
});
}

function DownloadPdf() {
    const filename  = 'Card.pdf';

    html2canvas(document.querySelector('#resultadoPesquisa')).then(canvas => {
        let pdf = new jsPDF('p', 'mm', 'a4');
        pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 293);
        pdf.save(filename);
        pdf.autoPrint();
        window.open(pdf.output('bloburl'), '_blank');
    
    });
}

function imprimir(){
    html2canvas(document.querySelector('#resultadoPesquisa')).then(canvas => {
        let pdf = new jsPDF('p', 'mm', 'a4');
        pdf.addImage(canvas.toDataURL('image/png'), 'PNG', 0, 0, 211, 293);
        pdf.autoPrint();
        window.open(pdf.output('bloburl'), '_blank');
    
    });
}




function replace_lead_score(result){

    if(result.res){
        result = result.res;
    }
    $('#progress_lead_Score').text("");

    if(result.eco){
        console.log('Entrou na função lead_score')

            var temp = 'Lead Score: <b>'+result.eco.lead_score+'</b> | Temperatura do lead: <b> '+result.eco.temperatura_lead+' </b> '
            console.log(temp)
            $('#progress_lead_Score').prepend(temp);                    
    }
    if(result.lead_score){
        $('#progress_lead_Score').text("");
        console.log(result.lead_score[0])
        var temp = 'Lead Score: <b>'+result.lead_score[0]+'</b> | Temperatura do lead: <b> '+result.lead_score[1]+' </b> '
        console.log(temp)
        $('#progress_lead_Score').prepend(temp);    
    }
    
}

function replace_email(result){

    if (result.res){
        result = result.res;
    }
    $('#pesquisa_email').text("");

    if (result.email){
        $("#pesquisa_email").prepend('<h6>'+result.email+'</h6>');

        if (result.emails){
            
            for(var i = 0; i<result.emails.length; i++){
                if (result.emails[i] == result.email){
                    continue    
                }
                $("#pesquisa_email").prepend('<h6>'+result.emails[i]+'</h6>');

            }
        }
    }
        if(result.emails && !result.email){
            for(var i = 0; i<result.emails.length; i++){         
            $("#pesquisa_email").prepend('<h6>'+result.emails[i]+'</h6>');

            }
    }

}


function replace_telefones(result){
    contador = 0;
    var elements = $();
    
   
    if (result.res){
        result = result.res;
    }
    if(Array.isArray(result.telefones)){
   
        if (result.telefones){
            for(var i = 0; i<result.telefones.length; i++){
                elements = elements.add('<small>'+result.telefones[i]+'</small></br>');
                
            }
            $('#pesquisa_telefones').text("");
            $('#pesquisa_telefones').append(elements);
        }
    }
    else if(!Array.isArray(result.telefones) && result.telefones){
   
        array_telefones = result.telefones.split(',')
        for(var i = 0; i<array_telefones.length; i++){
            elements = elements.add('<small>'+array_telefones[i]+'</small></br>');
            
        }
        $('#pesquisa_telefones').text("");
        $('#pesquisa_telefones').append(elements);
    }
    
}


function replace_cidade_estado(result){

    let cidade_estado = " ";
    let cidade = " ";
    let estado =  " ";
    if (result.res){
        result = result.res;
    }

    cidade_array = []
    
    $('#pesquisa_cidades').text("");
    if(result.cidades && result.cidades != undefined){
        for(var i = 0; i<result.cidades.length; i++){
            cidade_array.push(result.cidades[i].cidade)
        }
        cidade_array = [...new Set(cidade_array)]

        for(var i = 0; i<cidade_array.length; i++){
            $("#pesquisa_cidades").prepend('<h6>'+cidade_array[i]+'</h6>');
        }
    }

    if (result.cidades){
        if(result.cidades[0].cidade && result.cidades[0].estado){
            cidade_estado = result.cidades[0].cidade + " | " + result.cidades[0].estado;

            return cidade_estado
        }
        else if(result.cidades[0].cidade){

            cidade = result.cidades[0].cidade;
            return cidade
        }
        else if(result.cidades[0].estado){

            estado = result.cidades[0].estado; 
            return estado
        }

    }
    if (result.cidade){
        cidade = result.cidade
        return cidade;
    }



    if(result.enderecos){
        if(result.enderecos[0].city && result.enderecos[0].state){

            cidade_estado = result.enderecos[0].city + " | " + result.enderecos[0].state;
            return cidade_estado
        }
        else if (result.enderecos[0].city){
            cidade = result.enderecos[0].city;
            return cidade
        }
        else if(result.enderecos[0].state){
            estado = result.enderecos[0].state;
            return estado
        }
    }
    
    else{
        return "***"
    }
}

function replace_idiomas(result){
    if (result.res){
        result = result.res;
    }
    contador = 0;
    var elements = $();
    if(Array.isArray(result.idiomas)){
        if (result.idiomas){
            for(var i = 0; i<result.idiomas.length; i++){
                elements = elements.add('<small>'+result.idiomas[i]+'</small></br>');
                
            }
            $('#pesquisa_idiomas').text("");
            $('#pesquisa_idiomas').append(elements);
        }
    }
    else if(!Array.isArray(result.idiomas) && result.idiomas){
        array_idiomas = result.idiomas.split(',')
        for(var i = 0; i<array_idiomas.length; i++){
            elements = elements.add('<small>'+array_idiomas[i]+'</small></br>');
            
        }
        $('#pesquisa_idiomas').text("");
        $('#pesquisa_idiomas').append(elements);
    }
}

function adicionaOuRemove(id, array) {
    let index = array.findIndex(val => val.Valor == id);
    if(index < 0) {
        array.push({Param: 0, Valor: id});
    } else {
        array.splice(index, 1);
    }
  }
  

function replace_classe_social(result){
    if(result.res){
        result = result.res
    }

    $("#pesquisa_classe_social").text(" ")

    if(result.classe_social){
        $("#pesquisa_classe_social").text(result.classe_social)
    }
}

function replace_pessoas_relacionadas(result){
    if (result.res){
        result = result.res
    }
    $("#pesquisa_pessoa_relacionada").text(" ")
    $("#pesquisa_pessoa_relacionada2").text(" ")

    /* limpa a quantidade de pessoas */
    $('#quantidadePessoas').text("");

        if(result.pessoas_relacionadas){

        /*Coloca a quantidade de pessoas */
        $('#quantidadePessoas').text(' ('+result.pessoas_relacionadas.length+')')

        if(result.pessoas_relacionadas.length>3){
            $('#vermaispessoas').css('display', 'block')
        }
        for(var i = 0; i<result.pessoas_relacionadas.length; i++){
            var cpf = String(result.pessoas_relacionadas[i]['cpf'])            
            if(i < 4){                
                pessoas_relacionadas = '<li>' + result.pessoas_relacionadas[i]['nome'] + " | Relação: " + result.pessoas_relacionadas[i]['parentesco'] + " | cpf: " + '<a id="someId"'+contador+' onclick="pesquisa('+cpf+')" href="#">'  + cpf  +  '</a></li><br>'
                $("#pesquisa_pessoa_relacionada").prepend(pessoas_relacionadas)                
            }
            else{
                pessoas_relacionadas = '<li>' + result.pessoas_relacionadas[i]['nome'] + " | Relação: " + result.pessoas_relacionadas[i]['parentesco'] + " | cpf: " + '<a id="someId"'+contador+' onclick="pesquisa('+cpf+')" href="#">'  + cpf  +  '</a></li><br>'
                $("#pesquisa_pessoa_relacionada2").prepend(pessoas_relacionadas)

            }
        }
    }
    


}

function replace_endereco(result){
    let endereco = "***"

    if (result.res){
        result = result.res;
    }
    $("#pesquisa_endereco").text(" ")
    $("#pesquisa_endereco2").text(" ")

    var array = []
    /*limpa a quandtidade do lead anterior antes de colocar */
    $('#quantidadeEnderecos').text("");
    if(result.enderecos_completos && result.enderecos_completos.length>0){
        var lookup = {}
        var array = []
        
        for(var ob, i = 0; ob = result.enderecos_completos[i++];){
            var cep2 = ob.cep;
            var obj = ob
            var subStringCep = cep2.substring(0, 5);
        
            if (!(subStringCep in lookup) && !(cep2.endsWith('000')) || cep2 === " " ) {
                lookup[subStringCep] = 1;
                array.push(obj);
              }
        }
    }

    array = array.reverse()    
    
    if (array.length>3){
        $('#vermaisenderecos').css('display', 'block')
    }

    for(var i = 0; i<array.length; i++){

        if(i<4){
            if(array[i]['enderecos'] && array[i]['cep'] != " "){
                endereco = '<li>' + array[i]['enderecos']  + ", cep: " + array[i]['cep'] + '</li><br>'
                $("#pesquisa_endereco").prepend(endereco)
            }

            if(array[i]['endereco'] && array[i]['cep'] != " "){
                endereco2 = '<li>' + array[i]['endereco']  + ", cep: " + array[i]['cep'] + '</li><br>'
                $("#pesquisa_endereco").prepend(endereco2)
            }

            if(array[i]['endereco'] && array[i]['cep'] == " ") {
             
                endereco2 = '<li>' + array[i]['endereco']+'</li><br>'                
                $("#pesquisa_endereco").prepend(endereco2)
            }
        }
        
        else{
            if(array[i]['enderecos'] && array[i]['cep'] != " "){
                endereco = '<li>' + array[i]['enderecos']  + ", cep: " + array[i]['cep'] + '</li><br>'
                $("#pesquisa_endereco2").prepend(endereco)
            }

            if(array[i]['endereco'] && array[i]['cep'] != " "){
                endereco2 = '<li>' + array[i]['endereco']  + ", cep: " + array[i]['cep'] + '</li><br>'
                $("#pesquisa_endereco2").prepend(endereco2)
            }
            if(array[i]['endereco'] && array[i]['cep'] == " ") {
             
                endereco2 = '<li>' + array[i]['endereco']+'</li><br>'                
                $("#pesquisa_endereco2").prepend(endereco2)
            }
        }
    }

        /*limpa a quandtidade do lead anterior antes de colocar */
        $('#quantidadeEnderecos').text("");
        /*Coloca a quantidade de enderecos */
        $('#quantidadeEnderecos').text(' ('+array.length+')');


}

function replace_faixa_renda(result){
    let faixa_renda =  "***";
    if (result.res){
        result = result.res;
    }

    if (result.faixa_renda){
        faixa_renda = result.faixa_renda;
        return faixa_renda;
    }
    else {
        return faixa_renda
    }
}

function replace_empresas_vinculadas(result){
    let empresas_vinculadas = "***";
    if (result.res){
        result = result.res;
    }
    if(result.dados_cnpj){
        empresas_vinculadas = result.dados_cnpj.nome_empresa;
        return empresas_vinculadas
    }
    else{
        return empresas_vinculadas
    }
}

function replace_data_nascimento(result){
    if (result.res){
        result = result.res;
    }
    if (result.data_nascimento){
        return result.data_nascimento
    }
    else if(result['data-nascimento']){
        return result['data-nascimento']
    }
}

function replace_sexo(result){
    let sexo = "***"
    if (result.res){
        result = result.res;
    }
    sexo = result.sexo
    return sexo;
    
}

function replace_imagem(result){
    var imagem = "https://i.ibb.co/Fs9Yhk7/anonimo.png"
    if (result.res){
        result = result.res;
    }
    $('#pesquisa_images').text('')
    if (result.foto){
        imagem = result.foto;
        
        $('#pesquisa_images').attr("src", imagem);
    }
    
    else if(result.images){
        
        
        for(var i = 0; i<result.images.length; i++){
            imagem = result.images[i]
            imagem_tag = '<img src="'+imagem+'" class="img-circle" width="100" height="100"/>'
            $("#pesquisa_images").prepend(imagem_tag)
            if(i==2){
                break;
            }
            
        }
    }
    else{
        imagem_tag = '<img src="'+imagem+'" class="img-circle" width="100" height="100"/>'
        $("#pesquisa_images").prepend(imagem_tag)

    }
}

function replace_escolaridade(result){
    if (result.res){
        result = result.res
    }
    var escolaridade = "***";
    if (result.escolaridade){
        escolaridade = result.escolaridade[0].instituicao;
        escolaridade_descricao = result.escolaridade[0].descricao;
        escolaridade = 'Instituição: ' + escolaridade + ' | ' + escolaridade_descricao;

        return escolaridade;
    }
}

function replace_redes_sociais(result){

    if(result.res){
        result = res.result;
    }

    redes_sociais_url = [];
    redes_sociais_nome = [];
    const twitter ='<a id="twitter" href="#" target="_blank"><img style="width: 2%; height:2%;" src="https://cdn2.iconfinder.com/data/icons/metro-uinvert-dock/256/Twitter_NEW.png" src="teste"></a>'
    const facebook ='<a id="facebook" href="#" target="_blank"><img style="width: 2%; height:2%;" src="https://cdn3.iconfinder.com/data/icons/free-social-icons/67/facebook_square-128.png" src="teste"></a>'
    const googlePlus ='<a id="googleplus" href="#" target="_blank"><img style="width: 2%; height:2%;" src="https://freeiconshop.com/wp-content/uploads/edd/google-plus-flat.png" src="teste"></a>'
    const linkedin = '<a id="linkedin" href="#" target="_blank"><img style="width: 2%; height:2%;" src="https://cdn3.iconfinder.com/data/icons/capsocial-round/500/linkedin-128.png" src="teste"></a>'
    const pinterest = '<a id="pinterest" href="#" target="_blank"><img style="width: 2%; height:2%;" src="https://icons-for-free.com/free-icons/png/256/186277.png" src="teste"></a>'
    const instagram = '<a id="instagram" href="#" target="_blank"><img style="width: 2%; height:2%;" src="https://image.flaticon.com/icons/png/128/174/174855.png" src="teste"></a>'
    const flickr = '<a id="flickr" href="#" target="_blank"><img style="width: 2%; height:2%;" src="https://www.freeiconspng.com/uploads/flickr-icon-26.png" src="teste"></a>'
    

    $('#pesquisa_redes_sociais').text("");

    if(result.dominio){
        
        for(var i = 0; i<=result.dominio.social_networks.facebook.length; i++){
            if (result.dominio.social_networks.facebook[i]){
                $('#pesquisa_redes_sociais').prepend(facebook)
                $('#facebook').attr("href", result.dominio.social_networks.facebook[i])
            }
   
        }
        for(var i = 0; i<=result.dominio.social_networks.twitter.length; i++){
            if(result.dominio.social_networks.twitter[i]){
                $('#pesquisa_redes_sociais').prepend(twitter)
                $('#twitter').attr("href", result.dominio.social_networks.twitter[i])
              }
        }
        for(var i = 0; i<=result.dominio.social_networks.linkedin.length; i++){
            if(result.dominio.social_networks.linkedin[i]){
                $('#pesquisa_redes_sociais').prepend(linkedin)
                $('#linkedin').attr("href", result.dominio.social_networks.linkedin[i])
            }
       }
    }


    if(result.redes_sociais){
        for(var i = 0; i < result.redes_sociais.length; i++){
            redes_sociais_url.push(result.redes_sociais[i].url);
            redes_sociais_nome.push(result.redes_sociais[i].nome);
        }

    

    
        
        for(var i = 0; i<redes_sociais_nome.length; i++){
            if(redes_sociais_nome[i] == 'Twitter'){
                $('#pesquisa_redes_sociais').prepend(twitter)
                $('#twitter').attr("href", redes_sociais_url[i])            
            }
            if(redes_sociais_nome[i] == 'Google+'){
                $('#pesquisa_redes_sociais').prepend(googlePlus)
                $('#googleplus').attr("href", redes_sociais_url[i])
            }
            if(redes_sociais_nome[i] == 'Instagram'){
                $('#pesquisa_redes_sociais').prepend(instagram)
                $('#instagram').attr("href", redes_sociais_url[i])
            }
            if(redes_sociais_nome[i] == 'Facebook'){
                $('#pesquisa_redes_sociais').prepend(facebook)
                $('#facebook').attr("href", redes_sociais_url[i])
            }
            if(redes_sociais_nome[i] == 'Pinterest'){
                $('#pesquisa_redes_sociais').prepend(pinterest)
                $('#pinterest').attr("href", redes_sociais_url[i])
            }
            if(redes_sociais_nome[i] == 'Flickr'){
                $('#pesquisa_redes_sociais').prepend(flickr)
                $('#flickr').attr("href", redes_sociais_url[i])
            }
            if(redes_sociais_nome[i] == 'Pinterest'){
                $('#pesquisa_redes_sociais').prepend(pinterest)
                $('#pinterest').attr("href", redes_sociais_url[i])
            }        
    
        }
    }
    
}

function replace_profissao(result){

    if(result.res){
        result = res.result;
    }


    if(result.profissional){
        $("#pesquisa_profissao").text('');
        $("#pesquisa_profissao").prepend('<h6>Descrição: '+ result.profissional[0].descricao+ '</h6>');
        $("#pesquisa_profissao").prepend('<h6>Instituição: '+ result.profissional[0].instituicao+ '</h6>');
        
    }

    else if(result.profissao){
        $("#pesquisa_profissao").text(result.profissao);
    }
}


function replace_renda(result){
    if(result.res){
        result = res.result;
    }

    if(result.renda){
        $('#pesquisa_renda').text('');
        $('#pesquisa_renda').prepend('<h6>'+result.renda+'</h6>')
    }
}

function replace_servidor_publico(result){
    if(result.res){
        result = res.result;
    }

    if(result.remuneracao){
        $('#pesquisa_remuneracao_empresa').text('');
        $('#pesquisa_remuneracao_cargo').text('');
        $('#pesquisa_remuneracao_remuneracao').text('');
        $('#pesquisa_remuneracao_descricao').text('');
        $('#servidor_publico').css('display', 'block');


        if(result.remuneracao.cargo){
            $('#pesquisa_remuneracao_cargo').text(result.remuneracao.cargo);
        }

        if(result.remuneracao.empresa){
            $('#pesquisa_remuneracao_empresa').text(result.remuneracao.empresa);
        }

        if(result.remuneracao.remuneracao){
            $('#pesquisa_remuneracao_remuneracao').text(result.remuneracao.remuneracao);
        }


        if(result.remuneracao.tempo_no_cargo){
            $('#pesquisa_remuneracao_descricao').text(result.remuneracao.tempo_no_cargo);
        }            
        
    }
    else if(!result.remuneracao) {
        $('#servidor_publico').css('display', 'none');
    }
}


function replace_card(res){
    if (res.result.res){
        res.result = res.result.res
    }
    replace_lead_score(res.result);
    replace_servidor_publico(res.result)
    replace_classe_social(res.result);
    replace_email(res.result);
    replace_telefones(res.result);
    var cidade = replace_cidade_estado(res.result);
    replace_idiomas(res.result);
    replace_endereco(res.result);
    replace_pessoas_relacionadas(res.result)
    var faixa_renda = replace_faixa_renda(res.result)
    var empresas = replace_empresas_vinculadas(res.result);        
    var data_nascimento = replace_data_nascimento(res.result);
    var sexo = replace_sexo(res.result);
    replace_imagem(res.result);
    var escolaridade =replace_escolaridade(res.result);
    replace_redes_sociais(res.result);
    replace_profissao(res.result);
    replace_renda(res.result);
    var possuiEmpresa = res.result.possui_empresa; if (res.result.possui_empresa) res.result.possui_empresa; else 'Não'
    $("#pesquisa_possui_empresa").text(possuiEmpresa);


    if (res.result.cnpj){
        $("#pesqusia_cnpj").text('cnpj: '+ res.result.cnpj);
    }
    //nome - tem que ter em todos
    $("#pesquisa_nome").text(res.result.nome);
    // Primeira faixa -> Email | Telefone | Cidade | Profissão    $("#pesquisa_cidade").text(cidade);
    
    // Segunda faixa -> cpf | data nascimento | idade
    $("#pesquisa_cpf").text(res.result.cpf);
    $("#pesquisa_data_nascimento").text(data_nascimento);
    $("#pesquisa_idade").text(res.result.idade);
    $("#pesquisa_cidade").text(cidade);
    // Terceira faixa -> Estado Civil | Sexo | Endereço
    $("#pesquisa_estadocivil").text(res.result.estado_civil);
    $("#pesquisa_sexo").text(sexo);
    // $("#pesquisa_endereco").text(endereco);
    // Quarta faixa -> Renda presumida | Faixa de risco | score
    $("#pesquisa_faixa_renda").text(faixa_renda);
    $("#pesquisa_risco").text(res.result.risco);
    $("#pesquisa_score").text(res.result.score);
    // Quinta faixa -> Empresa | Tem Empresa | Participação
    $("#pesquisa_empresas").text(empresas);
    $("#pesquisa_participacao_societaria").text(res.result.participacao_societaria);
    // Sexta faixa -> Nome da mae | escolaridade  | Trabalho | Idioma
    $("#pesquisa_mae").text(res.result.mae);
    $("#pesquisa_nivel_escolaridade").text(res.result.nivel_escolaridade);
    $("#pesquisa_escolaridade").text(escolaridade);
    $("#pesquisa_idioma").text(res.result.idioma);

}
/*
 Obter historico de consumo do cliente através do endpoint abaixo
*/
function pesquisa_cache(){

  var table = document.getElementsByTagName("table")[0];
  var tbody = table.getElementsByTagName("tbody")[0];
  var pic = document.getElementById('pic1');

  //Aqui eu consigo pegar o elemento <span> da linha atual
  tbody.onclick = function (e) {
    e = e || window.event;
    var data = [];
    var target = e.srcElement || e.target;
    while (target && target.nodeName !== "TR") {
        target = target.parentNode;
    }
    if (target) {
        var cells = target.getElementsByTagName("span");

        for (var i = 0; i < cells.length; i++) {
            data.push(cells[i].innerHTML);
            if (data.length > 0 ){
                break;
            }
        }
    }
    
   // Jquery god - clean all resuts and set text *** 
   $( "[id^='pesquisa_']" ).text( "***" ); 
   $.ajax({
        url: "https://api.ubicity.com.br/ub/perfil?data="+data,
        type: "GET",
        crossDomain: true,
         beforeSend: function (xhr) {
         xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {

        // replace all
        replace_card(res)

        },
        error: function (error) {
          sweetAlert({
               title: ":(",
               text: "Erro ao pesquisar o perfil novamente!",
               type: "info",
               allowOutsideClick: true
          });
  
        },

  });

  return false

  };

}

function coletaDadosTodosOsLeads () {
    
}

function coletaDadosHistoricoUsuario(){
    $.ajax({
        url: "https://api.ubicity.com.br/ub/historico",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,

      	success: function (response) {
                  // atualiza campo especifico
                  document.getElementById("resultadoDummy").style.display='none';       
                  var totalUsuariosListagem=0;
                  console.log('data: ', response['data'])
                for (usuario in response['data']) {

                    totalUsuariosListagem++;
                    var data = response['data'][usuario]["timestamp"];                    
                    var telefones = response['data'][usuario]["telefones"];
                    var emails = response['data'][usuario]["emails"];
                    var temperatura = ' '
                    
                    if (response['data'][usuario]["lead_score_oficial"]){
                        if(response['data'][usuario]["lead_score_oficial"].length >=1){
                            temperatura = response['data'][usuario]["lead_score_oficial"][0][1];
                            if (temperatura == 'Frio'){
                                temperatura = '<h6 style="color: blue">'+temperatura+'</h6>';
                            }
                            if (temperatura == 'Morno'){
                                temperatura = '<h6 style="color: orange">'+temperatura+' </h6>';
                            }
                            if (temperatura == 'Quente'){
                                temperatura = '<h6 style="color: red">'+temperatura+' </h6>';
                            }
    
                        }
                       

                    }
                    else {
                        temperatura = temperatura
                    }               

                    var searched_key = response['data'][usuario]["searched_key"];
                    var cpf = response['data'][usuario]["cpf"];
                    var telefone = response['data'][usuario]["telefone"];
                    var email = response['data'][usuario]["email"];
                    if (temperatura == undefined){
                        temperatura = '';
                    }
                    cpf = verificaCpf(cpf,searched_key);
                    telefone = verificaTelefone(telefone, searched_key);
                    email = verificaEmail(email, emails, searched_key);

                    if (email == undefined){
                        email = " "
                    }
            
            
                    $('#resultadosUsuarios_listagem tr:last').after(
                    '<tr id="linha_tabela'+usuario+'">    \
                     <td>'+data+'</td>  \
                     <td>'+cpf+'</td>       \
                     <td>'+email+'</td>       \
                     <td>'+temperatura+'</td>       \
                     <td><a class="btn btn-info" onclick="pesquisa_cache()" data-toggle="modal" data-target="#perfilModal" href="#">Ver Perfil</a></td></tr>');
                  }

                       $('#resultadoDummy').remove();
                       criaTabela(totalUsuariosListagem);
  
                       //format date
                       $.fn.dataTable.moment('D-MM-Y HH:mm');

      	},
      	error: function (response) {

        }
    });
} 

// Funçao que irá retornar todos as planilhas xlxs do usuário
function getPlanilhas() {
var teste = 0;
 $.ajax({
     url: "https://api.ubicity.com.br/ub/listaplanilhas",
     type: "GET",
     beforeSend: function (xhr) {
         xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'), "Content-Transfer-Encoding",  "multipart/form");
     },
     cache: false,
     success: function (res) {
        document.getElementById("resultadoDummy").style.display='none';
        var totalArquivos=0;
        res.result.reverse();
        
        //monta tabela dos arquivos
        for (arquivo in res.result) {
            
            var detail = "";
            totalArquivos++;

            // colunas da tabela
            var filename = String(res.result[arquivo]["filename"]);
            var id_modal = filename.split('.').join("");
            id_modal = id_modal.split(':').join("");
            id_modal = id_modal.split('_').join("");

            var id_progress_bar = id_modal.split('-').join("");
            var status = res.result[arquivo]["status"];
            var timestamp =  res.result[arquivo]["timestamp"];
  
            var media = (status.total_of_enriched_rows / status.number_rows) * 100;
            
            
            if (Number(status.ok) == 1){
                novo_status = "<span class=\"label label-info\">Ok</span>"
            }
            else {
                novo_status = "<span class=\"label label-danger\">Erro</span>";

            }
            // Build modals
            $('#buildModal').after(
                    '<div class="modal fade" id="'+id_modal+'" tabindex="-1" role="dialog" aria-labelledby="'+id_modal+'"> \
                        <div class="modal-dialog" role="document"> \
                            <div class="modal-content"> \
                                <div class="modal-header text-center"> \
                                    <h3 class="modal-title text-center" id="excelDetail">Detalhes enriquecimento</h3> \
                                        <img src="./img/logo-original.png" alt=""> \
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> \
                                        </button> \
                                </div> \
                                <div id="statusDetail" class="modal-body"> \
                                <p><b>Nome do arquivo:</b> '+filename+'</p> \
                                <p><b>Total de linhas:</b> '+status.number_rows+'</p> \
                                <p><b>Data do arquivo:</b> '+timestamp+'</p> \
                                <p><b>Status:</b> '+novo_status+'</p> \
                                <p><b>Resultado:</b> '+status.detail+'</p> \
                                <p><b>Total de linhas em branco:</b> '+status.total_of_blank_results+'</p> \
                                <p><b>Total de erros:</b> '+status.total_of_errors+'</p> \
                                <p><b>Total de linhas enriquecidas:</b> '+status.total_of_enriched_rows+'</p> \
                                <p><b>Assertividade:</b> '+media.toFixed(2)+'% <\p> \
                                <div class="progress"> \
                                <div id="'+id_progress_bar+'" class="progress-bar" role="progressbar" style="width: '+media.toFixed(0)+'%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div> \
                                </div>\
                                </div> \
                            </div> \
                        </div> \
                    </div>'
            );
                  
        if (status.detail === 'Enriquecimento finalizado') 
        {
        var button = '';
          
        }
        else 
        {
         var button = 'disabled' ;
        }

            // Build table
            $('#resultadoListagemFile tr:last').after(
                '<tr id="resultadoDummy"'+arquivo+'>"    \
                         <td>'+timestamp+'</td> \
                         <td><b><a data-toggle="modal" data-target="#'+id_modal+'" href="#">'+filename+'</a></b></td>  \
                         <td>'+novo_status+'</td>>  \
                         <td>'+status.detail+'</td> \
                         <td><button type=\"button\" class=\"btn btn-rounded btn-success\"><a style="color:white;" class="'+button+' " \
                                href="https://api.ubicity.com.br/ub/downloadexcel?filename='+filename+'&user_email='+readCookie('useremail')+'" \
                                target="_blank" >Download</a></button></td> \
                    </tr>');

        }
        $('#totalArquivos').after(
            '<h4>Total de arquivos: '+totalArquivos+' </h4>'
        );

        return false;        
     },
     error: function (xhr, status, error) {
        return false;
     },
 });

}



/* verifica se token é valido*/
function tokenValido() {
    $.ajax({
        url: "https://api.ubicity.com.br/ub/verifica",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (res) {
            return true;
        },
        error: function (xhr, status, error) {

            eraseCookie('token');
            eraseCookie('username');
            eraseCookie('useremail');
            eraseCookie('verificado');
            eraseCookie('uuid');

            sweetAlert({
                   title: ":(",
                   text: "Sessão esgotada, faça login novamente",
                   type: "info",
                   allowOutsideClick: true
                 });
            
            setTimeout(function () {
              window.location.href = "index.html";
            }, 1500);
  
            return false;
        },
    });

}


// acho que essa funcao precisa ser chamada com um callback function
function calcularPrecoAvulso(nomePlano) {

    if (nomePlano == 'Grátis') {

        return (planos[0].valor_avulso) / 100;

    } else if (nomePlano == 'Iniciante') {

        return (planos[1].valor_avulso) / 100;

    } else if (nomePlano == 'Empresarial') {

        return (planos[2].valor_avulso) / 100;

    }

}


/* exibe uma amostra do que deve ser uma planilha*/
function templatePlanilha()
{
  sweetAlert({
         title: "",
           text: "<img src='../img/exemploPlanilha.png' style='height:100px;'>",
           html: true,
           //type: "info",
           allowOutsideClick: true

       });


}



function enviarArquivo(){


     var filedfield = document.getElementById('arquivoExcel');
     var formData = new FormData();
     formData.append('filedfield', $('#arquivoExcel')[0].files[0]);

     var ext = $('#arquivoExcel').val().split('.').pop().toLowerCase();
     if($.inArray(ext, ['xlsx']) == -1) {
       alert('invalid extension!');
       return false;
     }

         switch(ext) {
             case 'xlsx':

                 $.ajax({
                     url: "https://api.ubicity.com.br/ub/file",
                     type: "POST",
                     beforeSend: function (xhr) {
                         xhr.setRequestHeader
                         ("Authorization", "Bearer " + readCookie('token'));
                     },
                     data: formData, //estava como token, // aqui tem que ser o formData
                     cache: false,
                     contentType: false,                    
                     processData: false,

                     success: function (res) {
                           sweetAlert({
                                  title: ":)",
                                  text: "Seu arquivo foi enviado para processamento",
                                  type: "info",
                                  allowOutsideClick: true
                          });

                     setTimeout(function () {
                        window.location.href = 'upload_planilha.html';
                    }, 1500);
                            // getPlanilhas();
                     },
                     error: function (res) {

                       sweetAlert({
                              title: ":(",
                              text: "Erro ao enriquecer o arquivo. Verifique o formato das colunas",
                              type: "info",
                              allowOutsideClick: true
                      });
                     }
                });

                 break;
             default:
                 sweetAlert({
                        title: ":(",
                          text: "O formato válido é .xls ou .xlsx.  Outros formatos não são suportados ainda",
                          type: "info",
                          allowOutsideClick: true
                      });
                 this.value='';
         }

     return false;
   }


function verificaValorDaPesquisa(emailUsuario, cpfUsuario, telefoneUsuario, paramPesquisa){


    if (emailUsuario != '' && cpfUsuario == '' && telefoneUsuario == '')
    {   
        paramPesquisa = 'e-mail';
        pesquisouPorEmail = true;
        api_pesquisa = 'https://api.ubicity.com.br/ub/pesquisa?large=true&email=' + emailUsuario;
    }
    else if (emailUsuario == '' && cpfUsuario != '' && telefoneUsuario == '') 
    {
        paramPesquisa = 'cpf';
        pesquisouPorCPF = true;
        api_pesquisa = 'https://api.ubicity.com.br/ub/pesquisa?large=true&cpf=' + cpfUsuario;   
    }
    else if (emailUsuario == '' && cpfUsuario == '' && telefoneUsuario != '')
    {
        paramPesquisa = 'telefone';
        pesquisouPorTelefone = true;
        api_pesquisa = 'https://api.ubicity.com.br/ub/pesquisa?large=true&telefone=' + telefoneUsuario;      
    }

    else if(paramPesquisa != ''){
        pesquisouPorCPF = true;
        if(paramPesquisa.length === 10){
            api_pesquisa = 'https://api.ubicity.com.br/ub/pesquisa?large=true&cpf=' + "0" + String(paramPesquisa)    
        }
        else{
            api_pesquisa = 'https://api.ubicity.com.br/ub/pesquisa?large=true&cpf=' + String(paramPesquisa)
        }
        paramPesquisa = 'cpf'
    }
    else 
    {
        api_pesquisa = 'http://https://api.ubicity.com.br/ub/pesquisa?noparameters';  
    }
    return api_pesquisa
}



// Realiza pesquisa passando o (paramPesquisa) - Email, CPF, Telefone
function pesquisa(paramPesquisa) {

    tokenValido();
    var pesquisou = "";

    pesquisouPorEmail = false;
    pesquisouPorCPF = false;
    pesquisouPorTelefone = false;

    emailUsuario = $("#param_email").val();
    cpfUsuario = $("#param_cpf").val();
    telefoneUsuario = $("#param_telefone").val();
    
    /* Limpa valor do leadscore ao pesquisar outro lead */
    $("#progress_lead_Score").text(" ")

 
   //Verifica valor da pesquisa (cpf, email, telefone)
    verificaValorDaPesquisa(emailUsuario, cpfUsuario, telefoneUsuario, String(paramPesquisa));


    // Mostra carregamento
   if ($('#loader_pesquisando').css('display') == 'none')
   {
      $('#loader_pesquisando').css('display','block');
   }

    // Apaga o conteudo de todos os IDs que comecam com pesquisa_
    $( "[id^='pesquisa_']" ).text( "***" );

    	$.ajax({
            url: api_pesquisa,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader
                ("Authorization", "Bearer " + readCookie('token'));
            },
            cache: false,
            success: function(res) {
                console.log(res)
                $('#btn_imprimir').css('visibility', 'visible')
                $('#btn_imprimir2').css('visibility', 'visible')

                $('#search_people').trigger("reset");
                if ( $('#loader_pesquisando').css('display') == 'block' )
                    $('#loader_pesquisando').css('display','none');

                    if  ($('#resultadoPesquisa').css('visibility') == 'hidden' )
                    {
                        $('#resultadoPesquisa').css('visibility','visible');
                    }                                        
                    replace_card({'result':{res}})                    
                
                },

                error: function() 
                {
                    $('#search_people').trigger("reset");
                    console.log("erro na chamada pesquisa");
                    return false;

                }

      }).fail(function() 
      {

          console.log("erro 500 na chamada pesquisa.");
          $('#search_people').trigger("reset");


        if( $('#loader_pesquisando').css('display') == 'block' )
               $('#loader_pesquisando').css('display','none');
        sweetAlert({
                 title: "x(",
                   text: "Não podemos processar este pedido devido à um erro interno do sistema. Você pode tentar realizar outra pesquisa. Vamos providenciar o conserto para que tenha o resultado desejado" ,
                   type: "info",
                   allowOutsideClick: true
               });
          return false;

      });//ajax

      return false;
}

/* inicializa plataforma de pagamento*/
function initializeStripe() {

    var plano = ''
    var handler = StripeCheckout.configure({
        key: 'pk_live_OAFNKrnNa9c3INokPoKWPRgG',
        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
        locale: 'auto',  // aqui muda a linguagem do modal do stripe
        token: function (token) {
            token.stripeToken = token.id;
            token.plano = plano;
            token.avulso = qtd;
            $.ajax({
                url: "https://api.ubicity.com.br/ub/pagamento",
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader
                    ("Authorization", "Bearer " + readCookie('token'));
                },
                data: token,
                cache: false,
                success: function (charge) {
                    if (debug) console.log(charge);
                    $('#contratarSuccess').css('display', 'block');
                    setTimeout(function () {
                        // TODO verificar se faz sentido as 3 linhas abaixo.
                        $("#contratarModal").modal('hide');
                        $('.modal-backdrop').remove();
                        $('#contratarSuccess').html('');
                    }, 1500);
                    // TODO avaliar :   getSaldo(function (worked) {                     });
                    $("#contratarModal").modal('hide');
                    $('.modal-backdrop').remove();
                },
                error: function () {
                    showLandingLogado();
                    $('#contratarFail').html('');
                    $("#contratarModal").modal('hide');
                    $('.modal-backdrop').remove();
                }
            });
        }
    });


    document.getElementById('experimente-button').addEventListener('click', function (e) {
        plano = 'Experimente';
        handler.open({
            name: 'UbiCity Tecnologia da Informação Ltda.',
            description: 'Plano para experimentar - 1 pesquisa',
            currency: 'brl',
            amount: planos[0].valor   // ex:19900  = 199,00   // O VALOR AQUI É MERAMENTE Informação PARA O USUARIO. O VALOR É OBTIDO NO BACKEND POR MEDIDAS DE SEGURANCA
        });
        e.preventDefault();
    });

    document.getElementById('iniciante-button').addEventListener('click', function (e) {
        plano = 'Iniciante';
        handler.open({
            name: 'UbiCity Tecnologia da Informação Ltda.',
            description: 'Plano Iniciante - 50 pesquisas',
            currency: 'brl',
            amount: planos[1].valor   // ex:19900  = 199,00   // O VALOR AQUI É MERAMENTE Informação PARA O USUARIO. O VALOR É OBTIDO NO BACKEND POR MEDIDAS DE SEGURANCA
        });
        e.preventDefault();
    });

    document.getElementById('empresarial-button').addEventListener('click', function (e) {
        plano = 'Empresarial';
        handler.open({
            name: 'UbiCity Tecnologia da Informação Ltda.',
            description: 'Plano Empresarial - 100 pesquisas',
            currency: 'brl',
            amount: planos[2].valor
        });
        e.preventDefault();
    });

/*  FALTA implementar novamente no novo layout pois nao possui esse botao, que foi criado pelo LG para incrementar o total com setinhas
    document.getElementById('avulso-button').addEventListener('click', function (e) {
        plano = 'Avulso';
        handler.open({
            name: 'UbiCity Tecnologia da Informação Ltda.',
            description: 'Avulso - ' + nomePlano,
            currency: 'brl',
            amount: price * 100 // multiplico por 100 para inserir os centavos no stripe
        });
        e.preventDefault();
    });
*/


    window.addEventListener('popstate', function () {
        handler.close();
    });

}

$("#quantidade").bind('keyup mouseup', changeQuantidade);

var qtd = 1;
$('#quantidade').val('1');
var price = 0;
var precoPlanoAvulso = 0;
var consumoPlano = null;
var nomePlano = '';
var planos = [];

/* muda os valores para a quantidade de avulso desejada, de acordo com o plano corrente*/
function changeQuantidade() {

    qtd = Number($('#quantidade').val());

    if (qtd) {

        price = qtd * precoPlanoAvulso;
        if (debug) console.log("preço puro:" + price)
        //$('#preco-plano').html('Por R$' + precoPlanoAvulso.toFixed(2).toString().replace('.',',') + ' cada (plano ' + (nomePlano ? nomePlano : 'Grátis') + ')');
        $('#total').html('Total: R$' + price.toFixpesquisa_remuneracao_UF)
    } else {

        //$('#preco-plano').html('Por R$' + precoPlanoAvulso.toFixed(2).toString().replace('.',',') + ' cada (plano ' + (nomePlano ? nomePlano : 'Grátis') + ')');
        $('#total').html('Total: R$' + precoPlanoAvulso.toFixed(2).toString().replace('.', ','));

    }
}


function verificaMetabase(){
    

    $.ajax({
        url: "https://api.ubicity.com.br/ub/metabase",
        type: "GET",        
        beforeSend: function (xhr) {
            xhr.setRequestHeader
            ("Authorization", "Bearer " + readCookie('token'));
        },
        cache: false,
        success: function (response) {
            createCookie('verificado', 'true');
            console.log(response)            
        },
        error: function (error) {
          console.log(error)
        }
    });

    return false
}


function calcularPrecoAvulso(nomePlano) {

  if (planos){

    if (nomePlano == 'Grátis') {

        return (planos[0].valor_avulso) / 100;

    } else if (nomePlano == 'Iniciante') {

        return (planos[1].valor_avulso) / 100;

    } else if (nomePlano == 'Empresarial') {

        return (planos[2].valor_avulso) / 100;

    }

  }
}


function obterInformacaoDosPlanos() {

    $.ajax({
        url: "https://api.ubicity.com.br/ub/planos",
        type: "GET",
        cache: false,
        success: function (res) {

            planos = res.planos;
            var gratis = res.planos[0];
            var iniciante = res.planos[1];
            var empresarial = res.planos[2];

        },
        error: function (responseDoErro) {
        },
    });

}



// mostra o email do usuario
function boasVindas(){
  console.log("boasvindas xpto");
  $('#bemvindo').text(readCookie('useremail'));

}

/*


 faz a inicializacao da landing page*/
$(document).ready(function () {

    //obterInformacaoDosPlanos();
    //initializeStripe();

});



function verificaUsuarioMostraBotaoCadastro(){

    var email = readCookie('useremail');

    var verifica = email.includes("ubicity");
    $('#mostraCadastro').css('display', 'none');

    if(verifica){
        $('#mostraCadastro').css('display', 'block');
    }

}    


function cadastro(){

    var nome = $(".modal input#cadastro-nome").val();
    var email = $(".modal input#cadastro-email").val();
    var senha = $("input#cadastro-senha").val();
    var franquia = $("input#cadastro-franquia").val();
    var dados = {
        "email": email,
        "nome": nome,
        "senha": senha,
        "franquia": franquia
    };


    $.post('https://api.ubicity.com.br/ub/auth/cadastro', { email: dados['email'], nome : dados['nome'], senha:dados['senha'], franquia:dados['franquia']}, 
    function(returnedData){
        

        console.log(returnedData)

        if (returnedData['info'] == 'user created'){
            var info = "Usuário criado"
        }
        else {
            var info = returnedData['info'];
        }

        sweetAlert({
            title: ":)",
                text: info,                
                type: "info",
                allowOutsideClick: true
        });

        }).fail(function(error){
        console.log(error.responseText)
        var erro = error.responseText;
        erro = JSON.parse(erro)
        console.log(erro)
        console.log(erro["status"])
        sweetAlert({
            title: ":(",
                text: erro['message'] ,
                //imageUrl: 'thumbs-up.jpg'  falta definir uma imagem e local
                type: "warning",
                allowOutsideClick: true
            });
        });
    
    return false;

    
}


function filter_metabase(){
    
    

    $.ajax({
    
        url: "192.168.155.20:3001/api/table/252/query_metadata",    
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-Metabase-Session", 'cdcd22d6-a84c-455d-ba1c-ddbd83d293db');
            xhr.setRequestHeader("Access-Control-Allow-Origin", 'http://192.168.155.20:3001/api/table/252/query_metadata?_=1552441328263');
            xhr.setRequestHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        },
        cache: false,
        success: function (res) {
        
        if(res.fields){
           for(var i =0; i<res.fields.length; i++){
            $('#filter_metabase').prepend(res.fields[i].display_name)
           }
        }

        },
        error: function (responseDoErro) {
        },
    });

    return false;
}
