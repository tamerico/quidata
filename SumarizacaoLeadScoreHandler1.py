# api para obter dados de sumarizacao vinda do ubiplaces. a chave é o cookie do usuario e startada do email
# esta api obtem dados de LEAD SCORE
# TODO verificar handlers que gravam dados no mongo (cache) pois este está bem mvp

import json
import logging

import tornado.web
import datetime
from tornado.gen import coroutine
#from .tor_handler import TorHandler
from .utility_handler import UtilityHandler
from decorators.authenticated import authenticated_imob

@authenticated_imob
class SumarizacaoLeadScoreHandler(tornado.web.RequestHandler): #

    @coroutine
    def __learquivo(self):
        logger = logging.getLogger('imobiliaria-services.sumarizacaoLeadScore')
        try:
            x_file =  open('data_leadscore.json')
            #x_file =  open('/home/ubicity/json/data_leadscore.json') ISTO NAO GERA ERRO MAS TAMBEM NAO LE O ARQUIVO NO APPSERVEr!
            content =  x_file.read()
            baseSumarizacaoClientes =  json.loads(content)
            #print (baseSumarizacaoClientes)
            return baseSumarizacaoClientes
        except Exception as e:
            print('err leitura do arquivo json')
            print("erro : " + str(e))
            logger.debug(" erro lendo arquivo json" + str(e))
            return



    # retorna o conteudo de um determinado usuario atraves do email
    @coroutine
    def __search(self,base, email):
        logger = logging.getLogger('imobiliaria-services.sumarizacaoLeadScore')
        if base is None:
            logger.debug(" erro no __search base vazia.")
            return {"status":"error", "description": "base vazia!"}
        else:
            #logger.debug("  base:")
            #logger.debug(base)
            logger.debug(" base json ok. procurando email no base...")
            for p in base:
                if 'email' in p:
                    if p['email'] == email:
                        return p
                #else:
                    #logger.debug(p)


    @coroutine
    # grava os dados do sumarizacao no mongodb
    def __save_search(self, search):
        print ("fazendo a gravacao da sumarizacao no mongodb")
        search['timestamp'] = datetime.datetime.now()
        yield self.application.mongodb["sumarizacao_lead_score"].insert_one(search)
        return

    # responde a requisicao http get
    @coroutine
    def get(self):
        logger = logging.getLogger('imobiliaria-services.sumarizacaoLeadScore')

        print ("sumarizacaoLeadScore iniciando get")
        utility_handler = UtilityHandler(self)
        # esta sumarizacao é iniciada quando um cliente deseja ser contactado e para isso o email dele deve ser valido
        email = self.get_argument("email", default=None)
        if not utility_handler.valid_arguments(email):
            utility_handler.status400('email')
            return

        logger.debug(" buscando dados de sumarizacao no cache")


        result = yield utility_handler.get_cache('sumarizacao_lead_score', {"pesquisa": email})
        # result = False
        # Se há resultado no cache de pesquisa, retorna ele.
        if result:
            logger.debug(" dados de sumarizacao no cache no mongodb ok")
            #print (result)
            # apago algumas informacoes caso necessario
            # informo que veio do cache
            result['cache'] = True
            del result['_id']
            del result['timestamp']
            self.set_status(200)
            self.set_header("Content-Type", 'application/json; charset="utf-8"')
            self.write(result)
            return

        # Se não há então fazer a sumarizacao do usuario agora. isso pode levar minutos
        else:
            logger.debug(" realizando sumarizacaoLeadScore. pesquisando por dados novos para gravar no mongo")


            baseLeads = yield self.__learquivo()
            #print (baseLeads)
            if baseLeads is None:
                result = None
            else:
                logger.debug(baseLeads)
                result = yield self.__search(baseLeads, email)


            if result:

                self.set_header("Content-Type", 'application/json; charset="utf-8"')

                # se quiser imprimir de forma amigavel usar o pprint:   pprint(vars(result))
                self.set_status(200)
                #self.write( {"email":result.email.username}) isto funciona!
                # raw_json é um parametro que contem todo o json do pipl

                                #adiciono duas keys para fazer a busca via chache
                resultadoJson = result
                resultadoJson["pesquisa"] = email # considerando que a pesquisa tem como chave o e-mail mas pode ser melhorado isso aqui para pesquisar por nome tambem
                resultadoJson['cache'] = False # neste primeiro momento vai como cache false para mostrar que a informação veio direto da api do pipl
                self.write( resultadoJson)
                self.__save_search(resultadoJson) # prende a requisicao até gravar no banco

            else:
                response = {
                    'status': 'erro',
                    'message': 'Não foi possível encontrar resultados para esse email.'
                }

                self.set_status(404)
                self.write(response)

            return
